
## Logger

Logging library

### Internal usage

```
var logger = logging.logger();                        // create logger instance                                                              
logger.AddFileLogger(                                 // log file stream
    "logfile",                                        // log stream name
    "/var/log/foo.log",                               // log file
    logging.FATAL|logging.CRITICAL|logging.ERROR      // log levels to process (FATAL, CRITICAL and ERROR)
);
logger.AddStderrLogger("warn_log", logging.WARNING);  // stderr log stream with only WARNING level
logger.AddStdoutLogger("info_log", logging.INFO);     // stdout log stream with only INFO level

logger.Log("Hello, world", logging.FATAL);
logger.Log("Hello, world! But with log level INFO");
logger.Info("Also info");
logger.Error("Oopsie Woopsie!!", new Exception("A wittle fucko boingo"));

logger.Dispose();                                     // Destroys logger
```

#### List of sopported log levels
```
logging.DEBUG
logging.INFO
logging.WARNING
logging.ERROR
logging.CRITICAL
logging.FATAL
logging.ALL      // Any level
```

### External usage

```cs
using qsldotnet.lib

...
Logger logger = new Logger();
logger.AddFileLogger(                                 // log file stream
    "logfile",                                        // log stream name
    "/var/log/foo.log",                               // log file
    LogLevel.FATAL|LogLevel.CRITICAL|LogLevel.ERROR   // log levels to process (FATAL, CRITICAL and ERROR)
);
logger.AddStderrLogger("warn_log", LogLevel.WARNING); // stderr log stream with only WARNING level
logger.AddStdoutLogger("info_log", LogLevel.INFO);    // stdout log stream with only INFO level
logger.Log("Hello, world", LogLevel.FATAL);
logger.Log("Hello, world! But with log level INFO");
logger.Info("Also info");
logger.Error("Oopsie Woopsie!!", new Exception("A wittle fucko boingo"));

logger.Dispose();                                     // Destroys logger
...

```
