# qsl.net

**Script language interpreter/compiler with .Net integration**

Language has access control for types that you can use in scripts

Package also provides some useful library

### Can be used as standalone binary or as reference library
## Standalone Usage

```
usage: qsl.net.45.exe [-h] [--help] [-t type] [--type type] [-m module] [--module module] [-b] [--build] [-e eval] [--eval eval] [--forbid-io] [--forbid-net] [--forbid-http] [--forbid-threading] [--forbid-assemblies] [-c] [--case-sensitive] [-i] [--stdin] [file [file ...]]

Subcommands:

Positional arguments:
	file	Execute file

Optional arguments:
	-h, --help	Show this help
	-t type, --type type	Allowed type
	-m module, --module module	Preload module
	-b, --build	Compile source before run
	-e eval, --eval eval	Evaluate string
	--forbid-io Do not load 'system' function
	--forbid-net Do not load net module
	--forbid-threading Forbid using threads
	--forbid-assemblies Forbid using assembly() function
	-c, --case-sensitive	Case senstive symbols
	-i, --stdin	Interactive mode
```

By default qsl runs as interactive program, evaluating every line

```
$ qsldotnet.exe
>2+2
4
>print("hello");
hello
```

## Reference as library

```cs
using qsldotnet;

class Program
{
    public static void Main(string[] args)
    {
        QEngine scriptEngine = new QEngine(QSecurityFlags.StrictTypes | QSecurityFlags.ForbidSystem);
        scriptEngine.Run("print('Hello, world!')");
    }
}
```


