﻿using System.Reflection;
using System;
using System.Collections.Generic;
using qsldotnet.tests;

namespace qsl.net.tests
{
    class Program
    {
        static int Main(string[] args)
        {
            List<TestBook> testBooks = new List<TestBook>();
            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (typeof(TestBook).IsAssignableFrom(type))
                {
                    ConstructorInfo ctor = type.GetConstructor(new Type[] { });
                    object tb = type.GetConstructor(new Type[] { }).Invoke(new object[0]);
                    testBooks.Add(tb as TestBook);
                }
            }
            int errors = 0;
            foreach (TestBook tb in testBooks)
                errors += tb.RunTests();
            return errors > 0 ? 1 : 0;
        }
    }
}
