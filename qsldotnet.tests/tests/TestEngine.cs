﻿using System;
using System.Collections.Generic;
using qsldotnet.tests;
using qsldotnet;

namespace qsl.net.tests
{
    public class TestEngine : TestBook
    {

        public void TestBubbleSort()
        {
            int[] arr = new int[] { 1, 67, 2, 4, 1, 2, 4, 7, 8 };
            QEngine engine = new QEngine();
            int[] arrCopy = new int[arr.Length];
            arr.CopyTo(arrCopy, 0);
            engine.Run(TestCommon.BubbleSort, new Dictionary<string, object> { { "input", arr } });
            Array.Sort(arrCopy);
            for (int i = 0; i < arr.Length; i++)
            {
                AssertValue(arrCopy[i], arr[i]);
            }
        }
    }
}
