﻿using System;
namespace qsl.net.tests
{
    public static class TestCommon
    {
        public const string BubbleSort = @"
var bubble = function(arr) {
    var len = arr.length;
    
    foreach (i in range(len)) {
        foreach(j in range(len - i - 1)) {
            if (arr[j] > arr[j + 1]) {

                var temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    return arr;
};

bubble(input);
";

    }
}

