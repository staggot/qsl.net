﻿using System;
using System.IO;
using qsldotnet.lib;

namespace qsldotnet.cmd
{
    internal static class Program
    {
        static void Run(CmdArgs args)
        {
            string[] types = args["type"] as string[];
            string[] modules = args["module"] as string[];
            string[] files = args["file"] as string[];
            bool build = (bool)args["build"];
            QEngine engine = new QEngine(
                    ((bool)args["case-sensitive"] ? QSecurityFlags.CaseSensitive : 0) |
                    ((bool)args["forbid-system"] ? QSecurityFlags.ForbidSystem : 0) |
                    ((bool)args["forbid-io"] ? QSecurityFlags.ForbidIO : 0) |
                    ((bool)args["forbid-threading"] ? QSecurityFlags.ForbidTheading : 0) |
                    ((bool)args["forbid-net"] ? QSecurityFlags.ForbidNet : 0) |
                    ((bool)args["forbid-assemblies"] ? QSecurityFlags.ForbidAssembly : 0) |
                    ((types.Length > 0) ? QSecurityFlags.StrictTypes : 0)
                    );

            foreach (string type in types)
            {
                engine.AllowType(Type.GetType(type));
            }
            foreach (string module in modules)
            {
                string[] modulePair = module.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (modulePair.Length == 2)
                    engine.GlobalModule[modulePair[0]] = engine.LoadModule(engine.CurrentModuleDirectory, modulePair[1]);
                else if (modulePair.Length == 1)
                    engine.LoadModule(engine.CurrentModuleDirectory, modulePair[0]);
            }
            bool interactive = true;
            if (files.Length > 0)
            {
                interactive = false;
                for (int i = 0; i < files.Length; i++)
                {
                    if (build)
                    {
                        using (StreamReader sr = new StreamReader(files[i]))
                        {
                            engine.Compile(sr)(new FunctionArguments());
                        }
                    }
                    else
                    {
                        engine.GlobalModule.CreateContext().RunFile(files[i]);
                    }
                }

            }
            if (args["eval"] is {})
            {
                interactive = false;
                object res;
                if (build)
                {
                    res = engine.Compile((string)args["eval"])(new FunctionArguments());
                }
                else
                {
                    res = engine.GlobalModule.EvalString((string)args["eval"]);
                }
                if (!(res is Unassigned))
                    Console.WriteLine(engine.DumpString(res));
            }
            else if ((bool)args["stdin"])
            {
                interactive = false;
                engine.GlobalModule.CreateContext().RunStream(new StreamReader(Console.OpenStandardInput(1)));
            }
            while (interactive)
            {
                try
                {
                    string line = "";
                    while (true)
                    {
                        Console.Write(">");
                        string str = Console.ReadLine();
                        if (str.EndsWith("\\", StringComparison.Ordinal))
                        {
                            str = str.Trim('\\');
                            line += str + "\n";
                        }
                        else
                        {
                            line += str;
                            break;
                        }
                    }
                    object res = engine.GlobalModule.EvalString(line);
                    if (!(res is Unassigned))
                        Console.WriteLine(engine.DumpString(res));
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.ToInflatedString());
                }
            }
        }

        internal static void Main(string[] argv)
        {
            ArgParser rootParser = new ArgParser();
            rootParser.AddArgument(
                "--type",
                't',
                action: ArgAction.StoreAppend,
                type: ArgType.String,
                defaultValue: new string[0],
                help: "Allowed type");
            rootParser.AddArgument(
                "--module",
                'm',
                action: ArgAction.StoreAppend,
                type: ArgType.String,
                defaultValue: new string[0],
                help: "Preload module");
            rootParser.AddArgument(
                "--build",
                'b',
                action: ArgAction.StoreBool,
                defaultValue: false,
                help: "Compile source before run");
            rootParser.AddArgument(
                "file",
                action: ArgAction.StoreAppend,
                type: ArgType.String,
                argumentsNumber: null,
                defaultValue: new string[0],
                help: "Execute file");
            rootParser.AddArgument(
                "--eval",
                'e',
                action: ArgAction.StoreValue,
                type: ArgType.String,
                defaultValue: null,
                help: "Evaluate string");
            rootParser.AddArgument(
                "--forbid-system",
                action: ArgAction.StoreBool,
                help: "Do not load 'system' function");
            rootParser.AddArgument(
                "--forbid-io",
                action: ArgAction.StoreBool,
                help: "Do not load io module");
            rootParser.AddArgument(
                "--forbid-net",
                action: ArgAction.StoreBool,
                help: "Do not load net module");
            rootParser.AddArgument(
                "--forbid-http",
                action: ArgAction.StoreBool,
                help: "Do not load http module");
            rootParser.AddArgument(
                "--forbid-threading",
                action: ArgAction.StoreBool,
                help: "Forbid using threads");
            rootParser.AddArgument(
               "--forbid-assemblies",
               action: ArgAction.StoreBool,
               help: "Forbid using assembly() function");
            rootParser.AddArgument(
                "--case-sensitive",
                'c',
                action: ArgAction.StoreBool,
                help: "Case senstive symbols");
            rootParser.AddArgument(
                "--stdin",
                'i',
                action: ArgAction.StoreBool,
                help: "Interactive mode");
            rootParser.Function = new Action<CmdArgs>(Run);

            CmdArgs args = rootParser.ParseArguments(argv);
            try
            {
                args.Execute();
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToInflatedString());
                Environment.Exit(1);
            }
        }
    }
}