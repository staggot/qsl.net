﻿using System;

namespace qsldotnet.core
{
    internal enum StackWordType : byte
    {
        Value = 0,
        Symbol = 1,
        Type = 2,
        StaticMember = 3,
        InstanceMember = 4,
        ContextMember = 5,
        Array = 6,
        NewSymbol = 7
    }

    public partial struct ExecutionStackWord
    {
        static Func<ExecutionStack, ExecutionStackWord, object>[] getters = new Func<ExecutionStack, ExecutionStackWord, object>[]{
            new Func<ExecutionStack, ExecutionStackWord, object>(ValueGet),
            new Func<ExecutionStack, ExecutionStackWord, object>(SymbolGet),
            new Func<ExecutionStack, ExecutionStackWord, object>(TypeGet),
            new Func<ExecutionStack, ExecutionStackWord, object>(StaticMemberGet),
            new Func<ExecutionStack, ExecutionStackWord, object>(InstanceMemberGet),
            new Func<ExecutionStack, ExecutionStackWord, object>(ContextMemberGet),
            new Func<ExecutionStack, ExecutionStackWord, object>(ArrayGet),
            new Func<ExecutionStack, ExecutionStackWord, object>(NewSymbolGet),
        };
        static Action<ExecutionStack, ExecutionStackWord, object>[] setters = new Action<ExecutionStack, ExecutionStackWord, object>[]{
            new Action<ExecutionStack, ExecutionStackWord, object>(ValueSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(SymbolSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(TypeSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(StaticMemberSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(InstanceMemberSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(ContextMemberSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(ArraySet),
            new Action<ExecutionStack, ExecutionStackWord, object>(NewSymbolSet),
        };
        static Action<ExecutionStack, ExecutionStackWord, object>[] localSetters = new Action<ExecutionStack, ExecutionStackWord, object>[]{
            new Action<ExecutionStack, ExecutionStackWord, object>(ValueSetLocal),
            new Action<ExecutionStack, ExecutionStackWord, object>(SymbolLocalSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(TypeSetLocal),
            new Action<ExecutionStack, ExecutionStackWord, object>(StaticMemberLocalSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(InstanceMemberLocalSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(ContextMemberLocalSet),
            new Action<ExecutionStack, ExecutionStackWord, object>(ArraySetLocal),
            new Action<ExecutionStack, ExecutionStackWord, object>(NewSymbolLocalSet),
        };
        static Action<ExecutionStack, ExecutionStackWord, FunctionArguments>[] executors = new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>[]{
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(ValueExecute),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(SymbolExecute),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(TypeExecute),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(StaticMemberExecute),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(InstanceMemberExecute),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(ContextMemberExecute),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(ArrayExecute),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(NewSymbolExecute),
        };
        static Action<ExecutionStack, ExecutionStackWord, FunctionArguments>[] contructors = new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>[]{
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(ValueContsructor),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(SymbolConstructor),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(TypeConstructor),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(StaticMemberConstructor),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(InstanceMemberConstructor),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(ContextMemberConstructor),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(ArrayConstructor),
            new Action<ExecutionStack, ExecutionStackWord, FunctionArguments>(NewSymbolConstructor),
        };




        internal object Parameter;
        internal object Value;
        internal StackWordType WordType;

        internal ExecutionStackWord(StackWordType type, object value, object parameter = null)
        {
            this.Value = value;
            this.Parameter = parameter;
            this.WordType = type;
        }

        internal object Get(ExecutionStack stack)
        {
            return getters[(int)WordType](stack, this);
        }

        internal void Set(ExecutionStack stack, object value)
        {
            setters[(int)WordType](stack, this, value);
        }

        internal void SetLocal(ExecutionStack stack, object value)
        {
            localSetters[(int)WordType](stack, this, value);
        }

        internal void Execute(ExecutionStack stack, FunctionArguments args)
        {
            executors[(int)WordType](stack, this, args);
        }

        internal void Constructor(ExecutionStack stack, FunctionArguments args)
        {
            contructors[(int)WordType](stack, this, args);
        }

        public override string ToString()
        {
            return string.Format("Word {0}",
                                 this.WordType.ToString()
                                 );
        }
    }
}
