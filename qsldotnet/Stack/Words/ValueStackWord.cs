﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static object ValueGet(ExecutionStack stack, ExecutionStackWord word)
        {
            return word.Value;
        }
        static void ValueSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            throw new ExecutionException(stack.Context, string.Format("Can`t Assign: {0} To: {1}", stack.Engine.DumpString(value), word.ToString()));
        }
        static void ValueSetLocal(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            throw new ExecutionException(stack.Context, string.Format("Can`t Assign: {0} To: {1}", stack.Engine.DumpString(value), word.ToString()));
        }
        static void ValueExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultExecute(stack, word, args);
        }
        static void ValueContsructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultConstructor(stack, word, args);
        }
    }
}