﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static object InstanceMemberGet(ExecutionStack stack, ExecutionStackWord word)
        {
            if (word.Value is null)
                throw new ExecutionException(stack.Context, string.Format("Null Pointer Exception '{0}'", word));
            Type type = word.Value.GetType();
            MemberInfo member = Reflector.FindInstanceMember(type, word.Parameter as string, stack.Engine.IgnoreCase);
            if (member is null)
                throw new ExecutionException(stack.Context, string.Format("Type {0} Has No Member '{1}'", type.FullName, word.Parameter));
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)member).GetValue(word.Value);
                case MemberTypes.Property:
                    return ((PropertyInfo)member).GetValue(word.Value);
                default:
                    throw new ExecutionException(stack.Context, string.Format("[Get] Unsupported Member Type {0}", member.ToString()));
            }
        }

        static void InstanceMemberSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            InstanceMemberLocalSet(stack, word, value);
        }

        static void InstanceMemberLocalSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            if (word.Value is null)
                throw new ExecutionException(stack.Context, string.Format("Null Pointer Exception '{0}'", word));
            Type type = word.Value.GetType();
            MemberInfo member = Reflector.FindInstanceMember(type, word.Parameter as string, stack.Engine.IgnoreCase);
            if (member is null)
                throw new ExecutionException(stack.Context, string.Format("Type {0} Has No Member '{1}'", ((Type)word.Value).FullName, word.Parameter));
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    ((FieldInfo)member).SetValue(word.Value, value);
                    break;
                case MemberTypes.Property:
                    ((PropertyInfo)member).SetValue(word.Value, value);
                    break;
                default:
                    throw new ExecutionException(stack.Context, string.Format("[Set] Unsupported Member Type {0}", member.ToString()));
            }
        }

        static void InstanceMemberExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            if (word.Value is null)
                throw new ExecutionException(stack.Context, string.Format("Null Pointer Exception '{0}'", word));
            Type type = word.Value.GetType();
            MemberInfo member = Reflector.FindInstanceMember(type, word.Parameter as string, stack.Engine.IgnoreCase);
            if (member is null)
                throw new ExecutionException(stack.Context, string.Format("Type {0} Has No Member '{1}'", type.FullName, word.Parameter));
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                case MemberTypes.Property:
                    DefaultExecute(stack, word, args);
                    break;
                case MemberTypes.Method:
                    Reflector.DynamicInvokeMethod(
                        stack,
                        Reflector.FindMethod(type, word.Value, word.Parameter as string, args, stack.Engine.IgnoreCase),
                        word.Value,
                        args);
                    break;
                default:
                    throw new ExecutionException(stack.Context, string.Format("[Execute] Unsupported Member Type {0}", member.ToString()));
            }
        }

        static void InstanceMemberConstructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            throw new ExecutionException(stack.Context, string.Format("Can't Call A Constuctor Of: {0}", word.ToString()));
        }
    }
}