﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static object ArrayGet(ExecutionStack stack, ExecutionStackWord word)
        {
            dynamic dynObj = word.Value;
            object[] parameters = word.Parameter as object[];
            if (parameters.Length == 1)
            {
                return dynObj[(dynamic)parameters[0]];
            }
            if (parameters.Length == 2)
            {
                return dynObj[(dynamic)parameters[0], (dynamic)parameters[1]];
            }
            if (parameters.Length == 3)
            {
                return dynObj[(dynamic)parameters[0], (dynamic)parameters[1], (dynamic)parameters[2]];
            }
            if (parameters.Length == 4)
            {
                return dynObj[(dynamic)parameters[0], (dynamic)parameters[1], (dynamic)parameters[2], (dynamic)parameters[3]];
            }
            if (parameters.Length == 5)
            {
                return dynObj[(dynamic)parameters[0], (dynamic)parameters[1], (dynamic)parameters[2], (dynamic)parameters[3], (dynamic)parameters[4]];
            }
            if (parameters.Length == 6)
            {
                return dynObj[(dynamic)parameters[0], (dynamic)parameters[1], (dynamic)parameters[2], (dynamic)parameters[3], (dynamic)parameters[4], (dynamic)parameters[5]];
            }
            else
                throw new ExecutionException(stack.Context, string.Format("Number Of Indexers Exceeded: {0}", word.ToString()));
        }

        static void ArraySet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            dynamic dynObj = word.Value;
            object[] parameters = word.Parameter as object[];
            if (parameters.Length == 1)
            {
                dynObj[(dynamic)parameters[0]] = (dynamic)value;
            }
            else if (parameters.Length == 2)
            {
                dynObj[(dynamic)parameters[0], (dynamic)parameters[1]] = (dynamic)value;
            }
            else if (parameters.Length == 3)
            {
                dynObj[(dynamic)parameters[0], (dynamic)parameters[1], (dynamic)parameters[2]] = (dynamic)value;
            }
            else if (parameters.Length == 4)
            {
                dynObj[(dynamic)parameters[0], (dynamic)parameters[1], (dynamic)parameters[2], (dynamic)parameters[3]] = (dynamic)value;
            }
            else if (parameters.Length == 5)
            {
                dynObj[(dynamic)parameters[0], (dynamic)parameters[1], (dynamic)parameters[2], (dynamic)parameters[3], (dynamic)parameters[4]] = (dynamic)value;
            }
            else if (parameters.Length == 6)
            {
                dynObj[(dynamic)parameters[0], (dynamic)parameters[1], (dynamic)parameters[2], (dynamic)parameters[3], (dynamic)parameters[4], (dynamic)parameters[5]] = (dynamic)value;
            }
            else
                throw new ExecutionException(stack.Context, string.Format("Number Of Indexers Exceeded: {0}", word.ToString()));
        }

        static void ArraySetLocal(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            ArraySet(stack, word, value);
        }

        static void ArrayExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultExecute(stack, word, args);
        }

        static void ArrayConstructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultConstructor(stack, word, args);
        }
    }
}