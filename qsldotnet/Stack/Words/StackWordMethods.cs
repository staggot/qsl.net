﻿using System;
using System.Reflection;


namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static void DefaultExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            object func = word.Get(stack);
            if (func is QFunction)
            {
                object result = (func as QFunction)(args);
                if (!(result is Unassigned))
                    stack.PushValue(result);
            }
            else if (func is QConstructor)
            {
                stack.PushValue((func as QConstructor)(args));
            }
            else if (func is Delegate)
            {
                Reflector.DynamicInvokeDelegate(stack, func as Delegate, args);
            }
            else
                throw new ExecutionException(stack.Context, string.Format("Can't Execute: {0}", word));
        }

        static void DefaultConstructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            object obj = word.Get(stack);
            if (obj is Type)
            {
                stack.Engine.TestAllowedType(obj as Type);
                Reflector.DynamicInvokeMethod(stack, Reflector.FindConstructor(obj as Type, args, stack.Engine.IgnoreCase), null, args);
            }
            else if (obj is QConstructor)
            {
                stack.PushValue((obj as QConstructor)(args));
            }
            else
            {
                throw new ExecutionException(stack.Context, string.Format("Can't Call A Constuctor Of: {0}", word));
            }
        }
    }
}
