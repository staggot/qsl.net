﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static object SymbolGet(ExecutionStack stack, ExecutionStackWord word)
        {
            return stack.Context.GetVar(word.Parameter as string);
        }

        static void SymbolSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            stack.Context.SetVar(word.Parameter as string, value);
        }

        static void SymbolLocalSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            stack.Context.SetVarLocal(word.Parameter as string, value);
        }

        static void SymbolExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultExecute(stack, word, args);
        }

        static void SymbolConstructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultConstructor(stack, word, args);
        }
    }
}