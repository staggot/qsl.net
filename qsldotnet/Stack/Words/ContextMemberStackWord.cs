﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static object ContextMemberGet(ExecutionStack stack, ExecutionStackWord word)
        {
            QContext context = (QContext)word.Value;
            return context[word.Parameter as string];
        }

        static void ContextMemberSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            QContext context = (QContext)word.Value;
            context[word.Parameter as string] = value;
        }

        static void ContextMemberLocalSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            QContext context = (QContext)word.Value;
            context[word.Parameter as string] = value;
        }

        static void ContextMemberExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultExecute(stack, word, args);
        }

        static void ContextMemberConstructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultConstructor(stack, word, args);
        }
    }
}