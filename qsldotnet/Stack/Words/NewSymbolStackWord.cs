﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static object NewSymbolGet(ExecutionStack stack, ExecutionStackWord word)
        {
            return stack.Context.GetVar(word.Parameter as string);
        }

        static void NewSymbolSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            stack.Context.SetVarLocal(word.Parameter as string, value);
        }

        static void NewSymbolLocalSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            stack.Context.SetVarLocal(word.Parameter as string, value);
        }

        static void NewSymbolExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultExecute(stack, word, args);
        }

        static void NewSymbolConstructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            DefaultConstructor(stack, word, args);
        }
    }
}