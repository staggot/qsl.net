﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static object StaticMemberGet(ExecutionStack stack, ExecutionStackWord word)
        {
            MemberInfo member = Reflector.FindStaticMember((Type)word.Value, word.Parameter as string, stack.Engine.IgnoreCase);
            if (member is null)
                throw new ExecutionException(stack.Context, string.Format("Type {0} Has No Member '{1}'", ((Type)word.Value).FullName, word.Parameter));
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)member).GetValue(null);
                case MemberTypes.Property:
                    return ((PropertyInfo)member).GetValue(null);
                default:
                    throw new ExecutionException(stack.Context, string.Format("[Get] Unsupported Member Type {0}", member.ToString()));
            }

        }

        static void StaticMemberSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            StaticMemberLocalSet(stack, word, value);
        }

        static void StaticMemberLocalSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            MemberInfo member = Reflector.FindStaticMember((Type)word.Value, word.Parameter as string, stack.Engine.IgnoreCase);
            if (member is null)
                throw new ExecutionException(stack.Context, string.Format("Type {0} Has No Member '{1}'", ((Type)word.Value).FullName, word.Parameter));
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                    ((FieldInfo)member).SetValue(null, value);
                    break;
                case MemberTypes.Property:
                    ((PropertyInfo)member).SetValue(null, value);
                    break;
                default:
                    throw new ExecutionException(stack.Context, string.Format("[Set] Unsupported Member Type {0}", member.ToString()));
            }
        }

        static void StaticMemberExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            MemberInfo member = Reflector.FindStaticMember((Type)word.Value, word.Parameter as string, stack.Engine.IgnoreCase);
            if (member is null)
                throw new ExecutionException(stack.Context, string.Format("Type {0} Has No Member '{1}'", ((Type)word.Value).FullName, word.Parameter));
            switch (member.MemberType)
            {
                case MemberTypes.Field:
                case MemberTypes.Property:
                    DefaultExecute(stack, word, args);
                    break;
                case MemberTypes.Method:
                    Reflector.DynamicInvokeMethod(
                        stack,
                        Reflector.FindMethod((Type)word.Value, null, word.Parameter as string, args, stack.Engine.IgnoreCase),
                        null,
                        args);
                    break;
                default:
                    throw new ExecutionException(stack.Context, string.Format("[Execute] Unsupported Member Type {0}", member.ToString()));
            }
        }

        static void StaticMemberConstructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            throw new ExecutionException(stack.Context, string.Format("Can't Call A Constuctor Of: {0}", word.ToString()));
        }
    }
}