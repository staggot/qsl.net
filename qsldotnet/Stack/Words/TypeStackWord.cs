﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public partial struct ExecutionStackWord
    {
        static object TypeGet(ExecutionStack stack, ExecutionStackWord word)
        {
            return word.Value;
        }
        static void TypeSet(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            throw new ExecutionException(stack.Context, string.Format("Can`t Assign: {0} To: {1}", stack.Engine.DumpString(value), word.ToString()));
        }
        static void TypeSetLocal(ExecutionStack stack, ExecutionStackWord word, object value)
        {
            throw new ExecutionException(stack.Context, string.Format("Can`t Assign: {0} To: {1}", stack.Engine.DumpString(value), word.ToString()));
        }
        static void TypeExecute(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            throw new ExecutionException(stack.Context, string.Format("Can`t Execute Type {0}", stack.Engine.DumpString(word.Value), word.ToString()));
        }
        static void TypeConstructor(ExecutionStack stack, ExecutionStackWord word, FunctionArguments args)
        {
            Type type = word.Value as Type;
            if (type is {})
            {

                stack.Engine.TestAllowedType(type);
                ConstructorInfo constructorInfo = Reflector.FindConstructor(type, args, stack.Engine.IgnoreCase);
                Reflector.DynamicInvokeMethod(stack, constructorInfo, null, args);
            }
            else
                throw new ExecutionException(stack.Context, string.Format("Can't Call A Constuctor Of: {0}", word));
        }
    }
}