﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    public class ExecutionStack
    {
        const int InitialSize = 8;

        internal QContext Context { get; private set; }
        internal QModule Module { get; private set; }

        internal QEngine Engine{ get { return Module.Engine; }}

        internal ExecutionStackWord[] stack;
        int index = 0;

        internal int Size { get { return stack.Length; } }
        internal int Count { get { return index; }}


        internal ExecutionStack(QContext context)
        {
            this.Context = context;
            this.Module = context.Module;
            stack = new ExecutionStackWord[InitialSize];
        }

        void Reallocate(int size)
        {
            ExecutionStackWord[] newStack = new ExecutionStackWord[size];
            for (int i = 0; i < size && i < Size; i++)
                newStack[i] = stack[i];
            stack = newStack;
        }

        internal static MethodInfo PopValueMethod = typeof(ExecutionStack).GetMethod(nameof(PopValue), new Type[0]);
        public object PopValue()
        {
            return Pop().Get(this);
        }

        internal static MethodInfo PushValueMethod = typeof(ExecutionStack).GetMethod(nameof(PushValue), new Type[] { typeof(object) });
        public void PushValue(object value)
        {
            ExecutionStackWord word = new ExecutionStackWord(
                StackWordType.Value,
                value
            );
            Push(word);
        }

        internal static MethodInfo PopTrueMethod = typeof(ExecutionStack).GetMethod(nameof(PopTrue), new Type[0]);
        public bool PopTrue()
        {
            object value = PopValue();
            if (value is null)
                return false;
            if (value is bool)
                return (bool)value;
            if (value.IsNumber())
                return (dynamic)value != 0;
            return false;
        }

        internal static MethodInfo PopReturnMethod = typeof(ExecutionStack).GetMethod(nameof(PopReturn), new Type[0]);
        public object PopReturn()
        {
            if (index > 0)
                return PopValue();
            return Unassigned.Value;
        }

        internal static MethodInfo PopMethod = typeof(ExecutionStack).GetMethod(nameof(Pop), new Type[0]);
        public ExecutionStackWord Pop()
        {
            if(index==0)
                throw new ExecutionException(Context, "Stack Exhaust");
            return stack[--index];
        }

        internal ExecutionStackWord Peek()
        {
            if (index == 0)
                throw new ExecutionException(Context, "Stack Exhaust");
            return stack[index - 1];
        }

        internal void Push(ExecutionStackWord word)
        {
            if (index == Size)
                Reallocate(Size * 2);
            stack[index++] = word;
        }

    }
}
