using System.Collections.Generic;

namespace qsldotnet
{
    internal class ArrayArguments
    {
        List<object> values = new List<object>();
        readonly QEngine engine;

        internal ArrayArguments (QEngine engine)
        {
            this.engine = engine;
        }

        internal object this[int i]
        {
            get
            {
                return values[i];
            }
        }

        internal void AddValue(object value)
        {
            values.Add(value);
        }

        internal object GetValue(int index)
        {
            return values[index];
        }

        internal List<object> List
        {
            get { return values; }
        }

        internal object[] Array()
        {
            object[] vals = new object[values.Count];
            for (int i = 0; i < vals.Length; i++)
                vals[i] = values[i];
            return vals;
        }

        internal int Length
        {
            get { return values.Count; }
        }

        public override string ToString ()
        {
            return engine.DumpString (this);
        }
    }
}
