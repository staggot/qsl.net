using System;
using System.Reflection;
namespace qsldotnet
{
    public class Unassigned
    {
        internal Unassigned() { }

        internal static MethodInfo GetUnassignedMethod = typeof(Unassigned).GetProperty(nameof(Value)).GetMethod;
        public static Unassigned Value { get; private set; } = new Unassigned();

        public override string ToString()
        {
            return string.Format("Unassigned");
        }
    }
}
