﻿namespace qsldotnet
{
    public delegate object QFunction(FunctionArguments values);
    internal delegate QContext QConstructor(FunctionArguments values);
}
