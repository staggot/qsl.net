﻿using System;

namespace qsldotnet
{
    internal enum JsonReadState : byte
    {
        WaitValue = 0,
        WaitName = 1,
        WaitColon = 2,
        WaitSeparator = 3
        
    }
}
