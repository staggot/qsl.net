﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace qsldotnet
{
    public abstract class IgnoreCaseMap<T> : IDictionary, IDictionary<string, T>, IEnumerable
    {
        readonly Dictionary<string, T> values = new Dictionary<string, T>();
        readonly Dictionary<string, string> aliasMap = new Dictionary<string, string>();

        protected string GetKeyAlias(string key)
        {
            if (!IsIgnoreCase)
                return key;
            string lower = key.ToLower();
            if (aliasMap.ContainsKey(lower))
                return aliasMap[lower];
            return key;
        }

        public abstract bool IsIgnoreCase { get; }

        public virtual object this[object key]
        {
            get { return this[(string)key]; }
            set { this[(string)key] = (T)value; }
        }
        public virtual T this[string key]
        {
            get
            {
                key = GetKeyAlias(key);
                return values[key];
            }
            set
            {
                key = GetKeyAlias(key);
                if (IsIgnoreCase && !values.ContainsKey(key))
                    aliasMap[key.ToLower()] = key;
                values[key] = value;
            }
        }

        public virtual bool IsFixedSize => false;
        public virtual bool IsReadOnly => false;
        public virtual ICollection Keys => values.Keys;
        public virtual ICollection Values => values.Values;
        public virtual int Count => values.Count;
        public virtual bool IsSynchronized => true;
        public virtual object SyncRoot => values;

        ICollection<string> IDictionary<string, T>.Keys => values.Keys;

        ICollection<T> IDictionary<string, T>.Values => values.Values;

        public virtual void Add(object key, object value)
        {
            this[key] = (T)value;
        }
        public virtual void Add(string key, T value)
        {
            this[key] = value;
        }
        public virtual void Add(KeyValuePair<string, T> item)
        {
            this[item.Key] = item.Value;
        }
        public virtual void Clear()
        {
            values.Clear();
            aliasMap.Clear();
        }
        public virtual bool Contains(object key)
        {
            return ContainsKey((string)key);
        }
        public virtual bool Contains(KeyValuePair<string, T> item)
        {
            return ContainsKey(item.Key);
        }
        public virtual bool ContainsKey(string key)
        {
            key = GetKeyAlias(key);
            return values.ContainsKey(key);
        }
        public virtual void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }
        public virtual void CopyTo(KeyValuePair<string, T>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }
        public virtual IDictionaryEnumerator GetEnumerator() => values.GetEnumerator();
        public virtual void Remove(object key)
        {
            Remove((string)key);
        }
        public virtual bool Remove(string key)
        {
            key = GetKeyAlias(key);
            if (IsIgnoreCase)
            {
                string lowerKey = key.ToLower();
                if (aliasMap.ContainsKey(lowerKey))
                {
                    values.Remove(aliasMap[lowerKey]);
                    aliasMap.Remove(lowerKey);
                    return true;
                }
                return false;
            }
            return values.Remove(key);
        }
        public virtual bool Remove(KeyValuePair<string, T> item)
        {
            return Remove(item.Key);
        }

        public virtual bool TryGetValue(string key, out T value)
        {
            key = GetKeyAlias(key);
            return values.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        IEnumerator<KeyValuePair<string, T>> IEnumerable<KeyValuePair<string, T>>.GetEnumerator()
        {
            return values.GetEnumerator();
        }
    }
}
