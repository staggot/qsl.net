﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;

namespace qsldotnet
{
    public class QJson
    {
        Dictionary<Type, Func<object, string>> primitivePrinter = new Dictionary<Type, Func<object, string>>();
        QEngine engine;

        public QJson(QEngine engine = null)
        {
            this.engine = engine;
            primitivePrinter[typeof(string)] = (arg) =>
            {
                string result = "";
                foreach (char ch in ((string)arg))
                    result += BackslashSequence.ToBackslash(ch, false);
                result = "\"" + result + "\"";
                return result;
            };
            primitivePrinter[typeof(bool)] = (arg) => (bool)arg ? "true" : "false";
            primitivePrinter[typeof(char)] = (arg) => "'" + BackslashSequence.ToBackslash((char)arg, true) + "'";
            primitivePrinter[typeof(int)] = (arg) => arg.ToString();
            primitivePrinter[typeof(uint)] = (arg) => arg.ToString();
            primitivePrinter[typeof(short)] = (arg) => arg.ToString();
            primitivePrinter[typeof(ushort)] = (arg) => arg.ToString();
            primitivePrinter[typeof(byte)] = (arg) => arg.ToString();
            primitivePrinter[typeof(sbyte)] = (arg) => arg.ToString();
            primitivePrinter[typeof(long)] = (arg) => arg.ToString();
            primitivePrinter[typeof(ulong)] = (arg) => arg.ToString();
            primitivePrinter[typeof(double)] = (arg) =>
            {
                double number = (double)arg;
                if (double.IsNegativeInfinity(number))
                    return "-Infinity";
                if (double.IsPositiveInfinity(number))
                    return "Infinity";
                if (double.IsNaN(number))
                    return "NaN";
                string result = number.ToString();
                bool addDot = true;
                foreach (char c in result)
                    if (c == '.' || c == 'e' || c == 'E')
                        addDot = false;
                return result + (addDot ? ".0" : "");
            };
            primitivePrinter[typeof(decimal)] = primitivePrinter[typeof(double)];
            primitivePrinter[typeof(float)] = (arg) =>
            {
                float number = (float)arg;
                if (float.IsNegativeInfinity(number))
                    return "-Infinity";
                if (float.IsPositiveInfinity(number))
                    return "Infinity";
                if (float.IsNaN(number))
                    return "NaN";
                string result = number.ToString();
                bool addDot = true;
                foreach (char c in result)
                    if (c == '.' || c == 'e' || c == 'E')
                        addDot = false;
                return result + (addDot ? ".0" : "");
            };
            primitivePrinter[typeof(QFunction)] = (arg) => "null";
            primitivePrinter[typeof(QConstructor)] = (arg) => "null";
        }


        /// <summary>
        /// Adds dump() string serializer for type
        /// </summary>
        /// <param name="type">Type for serialization</param>
        /// <param name="function">Function that returns serialized type instance</param>
        public void SetJsonizer(Type type, Func<object, string> function)
        {
            if (function is null)
                primitivePrinter.Remove(type);
            else
                primitivePrinter[type] = function;
        }

        string PrimitiveToString(object obj)
        {
            if (obj is null)
                return "null";
            if (obj is Unassigned)
                return "unassigned";
            if (obj is Enum)
            {
                Enum value = (Enum)obj;
                return obj.GetType().FullName.Replace('.', '_') + "." + value.ToString();
            }
            if (obj is Type)
            {
                Type value = obj as Type;
                return "typeof(" + (value).FullName.Replace('.', '_') + ")";
            }
            if (primitivePrinter.ContainsKey(obj.GetType()))
                return primitivePrinter[obj.GetType()](obj);

            return null;
        }


        internal void Dump(StreamWriter stream, HashSet<object> printedCollections, object obj)
        {
            if (obj is null || primitivePrinter.ContainsKey(obj.GetType()))
            {
                stream.Write(PrimitiveToString(obj));
            }
            else if (obj is IDictionary)
            {
                if (printedCollections.Contains(obj))
                    throw new ScriptEngineException(engine, "Recursion found");
                printedCollections.Add(obj);
                IDictionary dict = obj as IDictionary;
                stream.Write("{");
                bool first = true;
                foreach (object key in dict.Keys)
                {
                    if (!first)
                        stream.Write(",");
                    first = false;
                    if (key is string)
                    {
                        Dump(stream, printedCollections, key);
                    }
                    else
                        throw new ScriptEngineException(engine, "Can dump dictionaries with only string keys");
                    stream.Write(":");
                    Dump(stream, printedCollections, dict[key]);
                }
                stream.Write("}");
                printedCollections.Remove(obj);
            }
            else if (obj is IEnumerable)
            {
                if (printedCollections.Contains(obj))
                    throw new ScriptEngineException(engine, "Recursion found");
                printedCollections.Add(obj);
                IEnumerable array = obj as IEnumerable;
                stream.Write("[");
                bool first = true;
                foreach (object value in array)
                {
                    if (!first)
                        stream.Write(",");
                    first = false;
                    Dump(stream, printedCollections, value);
                }
                stream.Write("]");
            }
            else if (obj is QArgs)
            {
                stream.Write(PrimitiveToString(null));
            }
            else
            {
                throw new ScriptEngineException(engine, string.Format("Unaccaptable type [{0}]", obj.GetType().Name));
            }
        }


        /// <summary>
        /// Json serialization
        /// </summary>
        /// <param name="stream">Stream to write</param>
        /// <param name="obj">Object to serialize</param>
        /// <returns>String representation of object</returns>
        public void Dump(StreamWriter stream, object obj)
        {
            Dump(stream, new HashSet<object>(), obj);
        }

        /// <summary>
        /// Json serialization
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>String representation of object</returns>
        public string DumpString(object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                StreamWriter stream = new StreamWriter(ms);
                Dump(stream, new HashSet<object>(), obj);
                stream.Flush();
                ms.Position = 0;
                StreamReader reader = new StreamReader(ms);
                string result = reader.ReadToEnd();
                stream.Dispose();
                reader.Dispose();
                return result;
            }
        }

        /// <summary>
        /// Loads Json from stream 
        /// </summary>
        /// <param name="stream">json stream</param>
        /// <returns></returns>
        public object Load(StreamReader stream)
        {
            return Load(new TokenStream(stream));
        }


        /// <summary>
        /// Loads Json from string 
        /// </summary>
        /// <param name="jsonString">json string</param>
        /// <returns></returns>
        public object LoadString(string jsonString)
        {
            return Load(new TokenStream(jsonString));
        }

        object Load(TokenStream stream)
        {
            Stack<object> stack = new Stack<object>();
            string name = null;
            JsonReadState state = JsonReadState.WaitValue;
            object result = null;
            while(true)
            {
                StreamToken token = stream.Next();
                if (token.Type == StreamTokenType.End)
                    break;

                switch (state)
                {
                    case JsonReadState.WaitColon:
                        if (token.Type == StreamTokenType.Colon)
                            state = JsonReadState.WaitValue;
                        else
                            throw new ParserException(token, "Expected ':'");
                        break;
                    case JsonReadState.WaitValue:
                        object value = null;
                        state = JsonReadState.WaitSeparator;
                        switch (token.Type)
                        {
                            case StreamTokenType.SquareBracketOpen:
                                value = new List<object>();
                                state = JsonReadState.WaitValue;
                                break;
                            case StreamTokenType.BlockStart:
                                if (engine is null)
                                    value = new Dictionary<string, object>();
                                else
                                    value = engine.CreateContext();
                                state = JsonReadState.WaitName;
                                break;
                            case StreamTokenType.TrueValue:
                                value = true;
                                break;
                            case StreamTokenType.FalseValue:
                                value = false;
                                break;
                            case StreamTokenType.IntValue:
                                value = int.Parse(token.Text);
                                break;
                            case StreamTokenType.LongValue:
                                value = long.Parse(token.Text);
                                break;
                            case StreamTokenType.FloatValue:
                                value = float.Parse(token.Text.Replace('.', ','));
                                break;
                            case StreamTokenType.DoubleValue:
                                value = double.Parse(token.Text.Replace('.', ','));
                                break;
                            case StreamTokenType.CharValue:
                            case StreamTokenType.StringValue:
                                value = token.Text;
                                break;
                            case StreamTokenType.Null:
                                value = null;
                                break;
                            case StreamTokenType.Unassigned:
                                value = Unassigned.Value;
                                break;
                            case StreamTokenType.SquareBracketClose:
                                if (stack.Count > 0)
                                {
                                    if (stack.Peek() is IList)
                                    {
                                        state = JsonReadState.WaitSeparator;
                                        if (stack.Count == 1)
                                            result = stack.Pop();
                                        else
                                            stack.Pop();
                                        continue;
                                    }
                                }
                                throw new ParserException(token, "Unexpected");
                        }
                        if (stack.Count > 0)
                        {
                            if (stack.Peek() is IDictionary && name is {})
                            {
                                (stack.Peek() as IDictionary)[name] = value;
                            }
                            else if (stack.Peek() is IList)
                            {
                                (stack.Peek() as IList).Add(value);
                            }
                        }
                        if (value is IDictionary || value is IList)
                            stack.Push(value);
                        if (stack.Count == 0)
                            result = value;
                        break;
                    case JsonReadState.WaitName:
                        if (token.Type == StreamTokenType.StringValue)
                        {
                            name = token.Text;
                            state = JsonReadState.WaitColon;
                            break;
                        }
                        if (token.Type == StreamTokenType.BlockEnd)
                        {
                            if (stack.Count > 0)
                            {
                                if (stack.Peek() is IDictionary)
                                {
                                    state = JsonReadState.WaitSeparator;
                                    if (stack.Count == 1)
                                        result = stack.Pop();
                                    else
                                        stack.Pop();
                                    break;
                                }
                            }
                        }
                        throw new ParserException(token, "Expected key");
                    case JsonReadState.WaitSeparator:
                        switch (token.Type)
                        {
                            case StreamTokenType.End:
                                if (stack.Count != 0 || name is {})
                                    throw new ParserException(token, "Unexpected");
                                return result;
                            case StreamTokenType.SquareBracketClose:
                                if (stack.Count > 0)
                                {
                                    if (stack.Peek() is IList)
                                    {
                                        state = JsonReadState.WaitSeparator;
                                        if (stack.Count == 1)
                                            result = stack.Pop();
                                        else
                                            stack.Pop();
                                        break;
                                    }
                                }
                                throw new ParserException(token, "Unexpected");
                            case StreamTokenType.BlockEnd:
                                if (stack.Count > 0)
                                {
                                    if (stack.Peek() is IDictionary)
                                    {
                                        state = JsonReadState.WaitSeparator;
                                        if (stack.Count == 1)
                                            result = stack.Pop();
                                        else
                                            stack.Pop();
                                        break;
                                    }
                                }
                                throw new ParserException(token, "Unexpected");
                            case StreamTokenType.Comma:
                                if (stack.Count > 0)
                                {
                                    if (stack.Peek() is IList)
                                    {
                                        state = JsonReadState.WaitValue;
                                        break;
                                    }
                                    if (stack.Peek() is IDictionary)
                                    {
                                        state = JsonReadState.WaitName;
                                        break;
                                    }
                                }
                                throw new ParserException(token, "Unexpected");
                            default:
                                throw new ParserException(token, "Unexpected");
                        }
                        break;
                }
            }
            return result;
        }
    }
}
