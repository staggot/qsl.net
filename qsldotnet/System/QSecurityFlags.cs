using System;
namespace qsldotnet
{
    [Flags]
    public enum QSecurityFlags : ushort
    {
        None = 0,
        StrictTypes = 1,
        ForbidSystem = 2,
        ForbidAssembly = 4,
        CaseSensitive = 8,
        ForbidIO = 16,
        ForbidTheading = 32,
        ForbidNet = 128,
        ForbidLogging = 256,
    }
}
