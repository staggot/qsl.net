﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace qsldotnet
{
    /// <summary>
    /// Provides propertly placed arguments
    /// </summary>
    public class QArgs
    {
        object[] values;
        Dictionary<string, int> nameMap = new Dictionary<string, int>();
        Dictionary<string, int> lowerNameMap = new Dictionary<string, int>();

        internal QArgs()
        {
            values = new object[0];
            nameMap = new Dictionary<string, int>();
        }

        /// <summary>
        /// Gets argument count
        /// </summary>
        public int Length
        {
            get
            {
                return values.Length;
            }
        }

        /// <summary>
        /// Gets argument by position
        /// </summary>
        /// <param name="name">Argument name</param>
        /// <returns>Argument value</returns>
        public object this[string name]
        {
            get
            {
                name = name.ToLower();
                if (nameMap.ContainsKey(name))
                    return values[nameMap[name]];
                name = name.ToLower();
                if (lowerNameMap.ContainsKey(name))
                    return values[lowerNameMap[name]];
                return Unassigned.Value;
                
            }
        }

        /// <summary>
        /// Gets argument by position
        /// </summary>
        /// <param name="i">Index</param>
        /// <returns>Argument value</returns>
        public object this[int i]
        {
            get
            {
                return values[i];
            }
        }

        internal QArgs(FunctionArguments args) : this(args.values, args.names)
        {
        }

        internal QArgs(List<object> values, List<string> names)
        {
            for (int i = 0; i < names.Count; i++)
            {
                if (names[i] is null)
                    continue;
                nameMap[names[i]] = i;
                lowerNameMap[names[i].ToLower()] = i;
            }
            this.values = values.ToArray();
        }
    }
}
