﻿using System;
using System.Collections.Generic;
using System.Reflection;
using qsldotnet.core;

namespace qsldotnet
{
    internal class QArgumentLocker
    {
        internal static QArgumentLocker Value = new QArgumentLocker();
    }

    /// <summary>
    /// Provides function arguments
    /// </summary>
    public class FunctionArguments
    {
        /// <summary>
        /// 
        /// </summary>
        public FunctionArguments()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values">parameters</param>
        public FunctionArguments(params object[] values)
        {
            for (int i = 0; i < values.Length; i++)
                AddValue(values[i]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine">Your script engine</param>
        /// <param name="values">key, value dict of parameters</param>
        public FunctionArguments(IDictionary<string, object> values)
        {
            if (values is {})
                foreach (var kv in values)
                    Set(kv.Key, kv.Value);
        }

        int currentIndex = 0;
        int namedCount = 0;
        internal Dictionary<string, int> dictionary = new Dictionary<string, int>();
        internal List<object> values = new List<object>();
        internal List<string> names = new List<string>();

        /// <summary>
        /// Returns argument value by name
        /// </summary>
        /// <param name="name">Argument name</param>
        /// <returns></returns>
        public object this[string name]
        {
            get
            {
                return values[dictionary[name]];
            }
        }

        /// <summary>
        /// Returns argument value by number
        /// </summary>
        /// <param name="i">Argument number</param>
        /// <returns></returns>
        public object this[int i]
        {
            get
            {
                return values[i];
            }
        }

        /// <summary>
        /// Returns arguments count
        /// </summary>
        public int Length { get { return currentIndex; } }

        internal object Get(string key)
        {
            return values[dictionary[key]];
        }

        internal void Set(string key, object value)
        {
            if (!dictionary.ContainsKey(key))
            {
                values.Add(value);
                names.Add(key);
                dictionary[key] = currentIndex;
                currentIndex++;
                namedCount++;
            }
            values[dictionary[key]] = value;
        }

        internal void ApplyDict(QContext context)
        {
            foreach (string key in dictionary.Keys)
            {
                context[key] = values[dictionary[key]];
            }
            context["_args"] = new QArgs(this);
        }

        internal QContext CreateContext(QEngine engine)
        {
            QContext context = engine.CreateContext();
            ApplyDict(context);
            return context;
        }

        internal bool ContainsArgument(QEngine engine, string key)
        {
            if (engine.IgnoreCase)
                key = key.ToLower();
            return dictionary.ContainsKey(key);
        }


        /// <summary>
        /// Extracts arguments by given rules
        /// </summary>
        /// <param name="engine">Your script engine instance</param>
        /// <param name="paramNames">Parameters names, that you would like to extract</param>
        /// <param name="defaultValues">Default values to assign if parameter is not set</param>
        /// <param name="context">Your current context, may be null</param>
        /// <returns>Collection with property placed arguments</returns>
        public QArgs ExctractArguments(QEngine engine, string[] paramNames, object[] defaultValues, QContext context = null)
        {
            List<object> args = new List<object>(Math.Max(paramNames.Length, Length));
            List<string> argsName = new List<string>(Math.Max(paramNames.Length, Length));

            int currentParamIndex = 0;
            bool[] usedParameters = new bool[paramNames.Length];
            bool[] usedRealArguments = new bool[Length];

            for (int i = 0; i < paramNames.Length; i++)
            {
                string name = paramNames[i];
                if (engine.IgnoreCase)
                    name = name.ToLower();
                if (dictionary.ContainsKey(name))
                {
                    int realIndex = dictionary[name];
                    args.Add(values[realIndex]);
                    argsName.Add(name);
                    usedParameters[i] = true;
                    usedRealArguments[realIndex] = true;
                    if (!(context is null))
                        context[name] = values[realIndex];

                }
                else
                {
                    args.Add(defaultValues[i]);
                    argsName.Add(paramNames[i]);
                    if (!(context is null))
                        context[name] = defaultValues[i];
                }
            }
            for (int i = 0; i < values.Count; i++)
            {
                if (usedRealArguments[i] || names[i] is {}) continue;
                while (true)
                {
                    if (currentParamIndex >= paramNames.Length || !usedParameters[currentParamIndex])
                        break;
                    currentParamIndex++;
                }
                if (currentParamIndex >= paramNames.Length) break;
                args[currentParamIndex] = values[i];
                argsName[currentParamIndex] = paramNames[currentParamIndex];
                usedParameters[currentParamIndex] = true;
                usedRealArguments[i] = true;
                if (!(context is null))
                    context[paramNames[currentParamIndex]] = values[i];
            }
            for (int i = 0; i < values.Count; i++)
            {
                if (usedRealArguments[i]) continue;
                if (!(names[i] is null) && !(context is null))
                {
                    context[names[i]] = values[i];
                }
                argsName.Add(names[i]);
                args.Add(values[i]);

                usedRealArguments[i] = true;
            }
            return new QArgs(args, argsName);
        }

        internal object[] GetArgumentsFor(MethodBase method, bool ignoreCase)
        {
            ParameterInfo[] paramegers = method.GetParameters();
            int[] mapping = ParametersMapping(paramegers, ignoreCase);
            object[] res = new object[paramegers.Length];
            for (int i = 0; i < paramegers.Length; i++)
            {
                if (mapping[i] >= 0)
                {
                    Type paramType = paramegers[i].ParameterType;
                    res[i] = Reflector.ParamConvert(values[mapping[i]], paramType);
                }
                else
                    res[i] = paramegers[i].DefaultValue;
            }
            return res;
        }

        internal int[] ParametersMapping(ParameterInfo[] parameters, bool ignoreCase)
        {
            int[] result = new int[parameters.Length];
            for (int i = 0; i < result.Length; i++)
                result[i] = -1;
            if (Length > parameters.Length)
                return null;
            bool[] used = new bool[Length];

            int currentAnonIndex = 0;

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo pi = parameters[i];
                string key = ignoreCase ? pi.Name.ToLower() : pi.Name;
                if (dictionary.ContainsKey(key))
                {
                    int index = dictionary[key];
                    used[index] = true;
                    result[i] = index;
                }
                else
                {
                    while (true)
                    {
                        if (currentAnonIndex >= Length)
                        {
                            if (pi.IsOptional)
                            {
                                return result;
                            }
                            else
                                return null;
                        }
                        if (!used[currentAnonIndex])
                            break;
                        currentAnonIndex++;
                    }
                    if (names[currentAnonIndex] is {})
                        return null;
                    used[currentAnonIndex] = true;
                    result[i] = currentAnonIndex;
                }
            }
            for (int i = 0; i < used.Length; i++)
                if (!used[i])
                    return null;
            return result;
        }

        internal int MatchMethod(MethodBase method, bool ignoreCase)
        {
            int result = 0;

            ParameterInfo[] parameters = method.GetParameters();

            int[] mapping = ParametersMapping(parameters, ignoreCase);

            if (mapping is null)
                return -1;

            for (int i = 0; i < parameters.Length && i < Length; i++)
            {
                ParameterInfo pi = parameters[i];

                int index = mapping[i];
                if (index < 0 && pi.IsOptional)
                {
                    result += 1;
                    continue;
                }

                object value = GetValue(index);

                Type paramType = pi.ParameterType;
                if (value is null && !paramType.IsByRef)
                {
                    return -1;
                }

                Type valueType = null;

                if (value is {})
                    valueType = value.GetType();
                if (paramType.IsAssignableFrom(valueType))
                {
                    result += 3;
                    continue;
                }
                if (paramType.IsSubclassOf(typeof(Delegate)) && value is QFunction)
                {
                    result += 2;
                    continue;
                }
                if (paramType.IsNumberType() && !valueType.IsNumberType())
                    return -1;
                if (!paramType.IsNumberType() && valueType.IsNumberType())
                    return -1;
                if (!paramType.IsNumberType() && !valueType.IsNumberType())
                {
                    if (!paramType.IsAssignableFrom(valueType))
                        return -1;
                }
                result += 1;
            }
            return result;
        }

        internal void AddValue(object value)
        {
            values.Add(value);
            names.Add(null);
            currentIndex++;
        }

        internal object GetValue(string key)
        {
            return values[dictionary[key]];
        }

        internal object GetValue(int index)
        {
            return values[index];
        }
    }
}
