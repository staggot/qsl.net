using System.Collections.Generic;
namespace qsldotnet
{
    internal static class BackslashSequence
    {
        static Dictionary<char, char> forwardConvert = new Dictionary<char, char>();
        static Dictionary<char, char> backwardConvert = new Dictionary<char, char>();

        static BackslashSequence()
        {
            AddBackslash('n', '\n');
            AddBackslash('t', '\t');
            AddBackslash('0', '\0');
            AddBackslash('\\', '\\');
            AddBackslash('r', '\r');
            AddBackslash('f', '\f');
            AddBackslash('b', '\b');
        }

        internal static char GetChar(char backslashedChar)
        {
            return forwardConvert[backslashedChar];
        }

        internal static bool ValidInput(char c)
        {
            return forwardConvert.ContainsKey(c);
        }

        internal static string ToBackslash(char c, bool singleQuote)
        {
            if (backwardConvert.ContainsKey(c))
                return "\\" + backwardConvert[c];
            if (singleQuote && c == '\'')
                return "\\'";
            if (!singleQuote && c == '\"')
                return "\\\"";
            if (c >= 0x7f || c < 0x20)
                return string.Format("\\u{0:X4}", (int)c);
            return c.ToString();
        }

        static void AddBackslash(char input, char output)
        {
            forwardConvert[input] = output;
            backwardConvert[output] = input;
        }

    }
}
