﻿using System;
namespace qsldotnet
{
    internal enum TokenState : byte
    {
        None = 0,
        Symbol,
        Number,
        Special,
        Member,
        QuoteMember,
        Quote,
        BackslashQuote,
        BackslashQuoteMember,
        Comment,
        DotSomething,
        HexNumber,
        BinaryNumber,
        FloatNumber,
        SingleNumberEnd,
        LongNumberEnd,
        FloatExponentialNumber,
    }
}
