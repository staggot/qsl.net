using System;

namespace qsldotnet
{

    internal enum StreamTokenType : byte
    {
        End = 0,
        TrueValue,
        FalseValue,
        CharValue,
        StringValue,
        LongValue,
        IntValue,
        FloatValue,
        DoubleValue,
        EvalValue,
        Operator,
        Symbol,
        Member,
        BlockStart,
        BlockEnd,
        SquareBracketOpen,
        SquareBracketClose,
        RoundBracketOpen,
        RoundBracketClose,
        Comma,
        Null,
        Unassigned,
        Global,
        This,
        Continue,
        Break,
        Semicolon,
        QuestionMark,
        Colon,
        New,
        Assign,
        Var,
        Function,
        Class,
        If,
        Else,
        ElseIF,
        While,
        Foreach,
        Using,
        In,
        Return,
        Try,
        Catch,
        Finally,
        Throw
    }

    internal struct StreamToken
    {
        internal string Text;
        internal StreamTokenType Type;
        internal int Line, Position;

        internal StreamToken(string text, StreamTokenType type, int line, int position)
        {
            Text = text;
            Type = type;
            Line = line;
            Position = position;
        }

        internal StreamToken(string text, StreamTokenType type)
        {
            Text = text;
            Type = type;
            Line = 0;
            Position = 0;
        }

        public static  bool operator ==(StreamToken a, StreamToken b)
        {
            return a.Text == b.Text && a.Type == b.Type;
        }

        public static bool operator !=(StreamToken a, StreamToken b)
        {
            return a.Text != b.Text || a.Type != b.Type;
        }

        public override string ToString ()
        {
            return string.Format ("{0} at Line {1} Position {2}", Text, Line + 1, Position);
        }

        public override bool Equals(object obj)
        {
            return obj is StreamToken token &&
                   Text == token.Text &&
                   Type == token.Type;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Text, Type);
        }
    }
}
