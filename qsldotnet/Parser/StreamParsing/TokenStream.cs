using System;
using System.IO;
using System.Linq;

namespace qsldotnet
{

    internal partial class TokenStream
    {
        protected int tokenBufferSize;

        protected char[] tokenBuffer;
        protected char[] tokenBackBuffer;

        protected int currentLine = 0;
        protected int currentPosition = 0;

        protected int tokenBufferIndex = 0;
        protected int tokenBackBufferIndex = 0;

        protected CharReader charReader;

        internal StreamToken PreviousToken { get; private set; }
        internal StreamToken CurrentToken { get; private set; }

        TokenStream(CharReader charReader)
        {
            this.charReader = charReader;
            tokenBufferSize = DefaultTokenBufferSize;
            tokenBuffer = new char[tokenBufferSize];
            tokenBackBuffer = new char[tokenBufferSize];
        }

        internal TokenStream(string text) : this(new StringCharReader(text))
        {
        }

        internal TokenStream(StreamReader stream) : this(new StreamCharReader(stream))
        {
        }

        private void ReallocateTokenBuffer(int size)
        {
            char[] newTokenBuffer = new char[size];
            char[] newTokenBackBuffer = new char[size];

            for (int i = 0; i < tokenBufferSize; i++)
            {
                newTokenBuffer[i] = tokenBuffer[i];
                newTokenBackBuffer[i] = tokenBackBuffer[i];
            }
            tokenBufferSize = size;
            tokenBuffer = newTokenBuffer;
            tokenBackBuffer = newTokenBackBuffer;
        }

        void PushToken(int length)
        {
            for (int i = tokenBufferIndex - 1; i >= length; i--)
                tokenBackBuffer[tokenBackBufferIndex++] = tokenBuffer[i];

            tokenBufferIndex = 0;
        }


        private char ReadChar()
        {
            char c = charReader.ReadChar();
            if (c == '\n')
            {
                currentLine++;
                currentPosition = 0;
            }
            else
            {
                currentPosition++;
            }
            return c;

        }

        string GetTokenValue(int length)
        {
            return new string(tokenBuffer, 0, length);
        }

        string GetTokenValue(int start, int length)
        {
            return new string(tokenBuffer, start, length);
        }

        bool IsSpecial(int length)
        {
            string str = GetTokenValue(length);
            return Specials.Contains(str);
        }


        bool IsValidSplitter(char c)
        {
            if (c == '\0' || char.IsWhiteSpace(c))
                return true;
            return SpecialsMap[0].ContainsKey(c.ToString());
        }


        int IsValidSpecials(int length)
        {
            char[] sublineChars = new char[length];
            for (int i = 0; i < length; i++)
                sublineChars[i] = tokenBuffer[i];
            string subline = new string(sublineChars);
            if (length >= SpecialsMap.Length)
                return 0;
            if (SpecialsMap[length - 1].ContainsKey(subline))
                return SpecialsMap[length - 1][subline];
            return 0;
        }

        internal StreamToken Next()
        {
            PreviousToken = CurrentToken;
            CurrentToken = _Next();
            return CurrentToken;
        }

        protected char NextChar()
        {
            if (tokenBackBufferIndex > 0)
                return tokenBackBuffer[--tokenBackBufferIndex];
            if (charReader.Done)
                return '\0';
            return ReadChar();
        }

        void PushCharToBuffer(char c)
        {
            if (tokenBufferIndex == tokenBufferSize)
                ReallocateTokenBuffer(tokenBufferSize * 2);
            tokenBuffer[tokenBufferIndex] = c;
            tokenBufferIndex++;
        }


        StreamToken _Next()
        {
            StreamToken token = new StreamToken(null, StreamTokenType.End, currentLine, currentPosition);

            bool tokenFound = false;
            bool tokenFail = false;

            bool commentWarning = false;

            TokenState state = TokenState.None;

            char quote = '\0';
            bool blockComment = false;

            while (!tokenFound && !tokenFail)
            {
                char c = NextChar();
                PushCharToBuffer(c);

                switch (state)
                {
                    case TokenState.None:
                        if (c >= '0' && c <= '9')
                        {
                            state = TokenState.Number;
                        }
                        else if (ValidSymbolBegin(c))
                        {
                            state = TokenState.Symbol;
                        }
                        else if (c == '"' || c == '\'' || c == '`')
                        {
                            state = TokenState.Quote;
                            quote = c;
                        }
                        else if (c == '.')
                        {
                            state = TokenState.DotSomething;
                        }
                        else if (IsValidSpecials(1) > 0)
                        {
                            state = TokenState.Special;
                        }
                        else if (char.IsWhiteSpace(c))
                        {
                            tokenBufferIndex--;
                        }
                        else
                        {
                            tokenFail = true;
                        }
                        break;
                    case TokenState.Number:
                        if (c == '.')
                        {
                            state = TokenState.FloatNumber;
                        }
                        else if (c == 'l' || c == 'L')
                        {
                            state = TokenState.LongNumberEnd;
                        }
                        else if (c == 'e' || c == 'E')
                        {
                            state = TokenState.FloatExponentialNumber;
                        }
                        else if (c == 'f' || c == 'F')
                        {
                            state = TokenState.SingleNumberEnd;
                        }
                        else if (tokenBufferIndex == 2 && tokenBuffer[0] == '0' && char.ToLower(tokenBuffer[1]) == 'x')
                        {
                            state = TokenState.HexNumber;
                            PushToken(2);
                        }
                        else if (tokenBufferIndex == 2 && tokenBuffer[0] == '0' && char.ToLower(tokenBuffer[1]) == 'b')
                        {
                            state = TokenState.BinaryNumber;
                            PushToken(2);
                        }
                        else if (c < '0' || c > '9')
                        {
                            if (IsValidSplitter(c))
                            {
                                token = new StreamToken(GetTokenValue(tokenBufferIndex - 1), StreamTokenType.IntValue, currentLine, currentPosition);
                                tokenFound = true;
                                PushToken(tokenBufferIndex - 1);
                            }
                            else
                                throw new ParserException(new StreamToken(GetTokenValue(tokenBufferIndex), StreamTokenType.IntValue, currentLine, currentPosition), "Unexpected symbol");
                        }
                        break;
                    case TokenState.FloatNumber:
                        if (c == 'f' || c == 'F')
                        {
                            state = TokenState.SingleNumberEnd;
                        }
                        else if (c == 'e' || c == 'E')
                        {
                            state = TokenState.FloatExponentialNumber;
                        }
                        else if (c < '0' || c > '9')
                        {
                            if ((ValidSymbolBegin(c) || c == '\'' || c == '"') && tokenBuffer[tokenBufferIndex - 2] == '.')
                            {
                                token = new StreamToken(GetTokenValue(tokenBufferIndex - 2), StreamTokenType.IntValue, currentLine, currentPosition);
                                tokenFound = true;
                                PushToken(tokenBufferIndex - 2);
                            }
                            else if (IsValidSplitter(c))
                            {
                                token = new StreamToken(GetTokenValue(tokenBufferIndex - 1), StreamTokenType.DoubleValue, currentLine, currentPosition);
                                tokenFound = true;
                                PushToken(tokenBufferIndex - 1);
                            }
                            else
                                throw new ParserException(new StreamToken(GetTokenValue(tokenBufferIndex), StreamTokenType.DoubleValue, currentLine, currentPosition), "Unexpected symbol");
                        }
                        break;
                    case TokenState.FloatExponentialNumber:
                        if ((c == '+' || c == '-') && (tokenBuffer[tokenBufferIndex - 2] == 'e' || tokenBuffer[tokenBufferIndex - 2] == 'E'))
                        {
                            //It's okay
                        }
                        else if (c == 'f' || c == 'F')
                        {
                            state = TokenState.SingleNumberEnd;
                        }
                        else if (c < '0' || c > '9')
                        {
                            if (IsValidSplitter(c))
                            {
                                token = new StreamToken(GetTokenValue(tokenBufferIndex - 1), StreamTokenType.DoubleValue, currentLine, currentPosition);
                                tokenFound = true;
                                PushToken(tokenBufferIndex - 1);
                            }
                            else
                                throw new ParserException(new StreamToken(GetTokenValue(tokenBufferIndex), StreamTokenType.DoubleValue, currentLine, currentPosition), "Unexpected symbol");
                        }
                        break;
                    case TokenState.LongNumberEnd:
                        if (IsValidSplitter(c))
                        {
                            token = new StreamToken(GetTokenValue(tokenBufferIndex - 2), StreamTokenType.LongValue, currentLine, currentPosition);
                            tokenFound = true;
                            PushToken(tokenBufferIndex - 1);
                        }
                        else
                            throw new ParserException(new StreamToken(GetTokenValue(tokenBufferIndex), StreamTokenType.LongValue, currentLine, currentPosition), "Unexpected symbol");
                        break;
                    case TokenState.SingleNumberEnd:
                        if (IsValidSplitter(c))
                        {
                            token = new StreamToken(GetTokenValue(tokenBufferIndex - 2), StreamTokenType.FloatValue, currentLine, currentPosition);
                            tokenFound = true;
                            PushToken(tokenBufferIndex - 1);
                        }
                        else
                            throw new ParserException(new StreamToken(GetTokenValue(tokenBufferIndex), StreamTokenType.LongValue, currentLine, currentPosition), "Unexpected symbol");
                        break;
                    case TokenState.Symbol:
                        if (!ValidSymbolMiddle(c))
                        {
                            if (IsValidSplitter(c))
                            {
                                string tokenText = GetTokenValue(tokenBufferIndex - 1);
                                token = new StreamToken(tokenText, SymbolTokenType(tokenText), currentLine, currentPosition);
                                tokenFound = true;
                                PushToken(tokenBufferIndex - 1);
                            }
                            else
                                throw new ParserException(new StreamToken(GetTokenValue(tokenBufferIndex), StreamTokenType.Symbol, currentLine, currentPosition), "Unexpected symbol");
                        }
                        break;
                    case TokenState.Member:
                        if (!ValidSymbolMiddle(c))
                        {
                            if (IsValidSplitter(c))
                            {
                                token = new StreamToken(GetTokenValue(0, tokenBufferIndex - 1), StreamTokenType.Member, currentLine, currentPosition);
                                tokenFound = true;
                                PushToken(tokenBufferIndex - 1);
                            }
                            else
                                throw new ParserException(new StreamToken(GetTokenValue(tokenBufferIndex), StreamTokenType.Member, currentLine, currentPosition), "Unexpected symbol");
                        }
                        break;
                    case TokenState.DotSomething:
                        if (ValidSymbolBegin(c))
                            state = TokenState.Member;
                        else if (c == '"' || c == '\'')
                            state = TokenState.QuoteMember;
                        else
                            throw new ParserException(new StreamToken(GetTokenValue(tokenBufferIndex), StreamTokenType.Member, currentLine, currentPosition), "Unexpected symbol");
                        break;
                    case TokenState.Quote:
                        if (c == '\\')
                        {
                            state = TokenState.BackslashQuote;
                        }
                        else if (c == quote)
                        {
                            token = new StreamToken(GetTokenValue(1, tokenBufferIndex - 2), c == '\'' ? StreamTokenType.CharValue : c == '`' ? StreamTokenType.Symbol : StreamTokenType.StringValue, currentLine, currentPosition);
                            PushToken(tokenBufferIndex);
                            tokenFound = true;
                        }
                        break;
                    case TokenState.QuoteMember:
                        if (c == '\\')
                        {
                            state = TokenState.BackslashQuoteMember;
                        }
                        else if (c == quote)
                        {
                            token = new StreamToken(GetTokenValue(2, tokenBufferIndex - 2), StreamTokenType.Member, currentLine, currentPosition);
                            PushToken(tokenBufferIndex);
                            tokenFound = true;
                        }
                        break;
                    case TokenState.BackslashQuote:
                        if (BackslashSequence.ValidInput(c))
                        {
                            tokenBufferIndex--;
                            tokenBuffer[tokenBufferIndex - 1] = BackslashSequence.GetChar(c);
                        }
                        else if (c == quote)
                        {
                            tokenBufferIndex--;
                            tokenBuffer[tokenBufferIndex - 1] = quote;
                        }
                        else if (c == 'u')
                        {

                            char[] hexValue = new char[4];
                            for (int i = 0; i < 4; i++)
                            {
                                char hChar = ReadChar();
                                if (hChar == '\0')
                                {
                                    tokenFail = true;
                                    break;
                                }
                                hexValue[i] = hChar;
                            }
                            tokenBufferIndex--;
                            tokenBuffer[tokenBufferIndex - 1] = (char)Convert.ToInt32(new string(hexValue), 16);
                        }
                        state = TokenState.Quote;
                        break;
                    case TokenState.BackslashQuoteMember:
                        if (BackslashSequence.ValidInput(c))
                        {
                            tokenBufferIndex--;
                            tokenBuffer[tokenBufferIndex - 1] = BackslashSequence.GetChar(c);
                        }
                        else if (c == quote)
                        {
                            tokenBufferIndex--;
                            tokenBuffer[tokenBufferIndex - 1] = quote;
                        }
                        else if (c == 'u')
                        {

                            char[] hexValue = new char[4];
                            for (int i = 0; i < 4; i++)
                            {
                                char hChar = ReadChar();
                                if (hChar == '\0')
                                {
                                    tokenFail = true;
                                    break;
                                }
                                hexValue[i] = hChar;
                            }
                            tokenBufferIndex--;
                            tokenBuffer[tokenBufferIndex - 1] = (char)Convert.ToInt32(new string(hexValue), 16);
                        }
                        state = TokenState.QuoteMember;
                        break;
                    case TokenState.Special:
                        int validSpecials = IsValidSpecials(tokenBufferIndex);
                        if (commentWarning && c == '/')
                        {
                            string tokenText = GetTokenValue(tokenBufferIndex - 2);

                            PushToken(tokenBufferIndex);
                            if (tokenText.Length > 0)
                            {
                                token = new StreamToken(tokenText, SpecialTokenType(tokenText), currentLine, currentPosition);
                                tokenFound = true;
                            }
                            state = TokenState.Comment;
                            blockComment = false;
                        }
                        else if (commentWarning && c == '*')
                        {
                            string tokenText = GetTokenValue(tokenBufferIndex - 2);

                            PushToken(tokenBufferIndex);
                            if (tokenText.Length > 0)
                            {
                                token = new StreamToken(tokenText, SpecialTokenType(tokenText), currentLine, currentPosition);
                                tokenFound = true;
                            }
                            state = TokenState.Comment;
                            blockComment = true;
                        }
                        else if (validSpecials == 1 && IsSpecial(tokenBufferIndex))
                        {
                            string tokenText = GetTokenValue(tokenBufferIndex);
                            token = new StreamToken(tokenText, SpecialTokenType(tokenText), currentLine, currentPosition);
                            PushToken(tokenBufferIndex);
                            tokenFound = true;
                        }
                        else if (validSpecials == 0)
                        {
                            for (int specialLength = tokenBufferIndex - 1; specialLength > 0; specialLength--)
                            {
                                if (IsSpecial(specialLength))
                                {
                                    string tokenText = GetTokenValue(specialLength);
                                    token = new StreamToken(tokenText, SpecialTokenType(tokenText), currentLine, currentPosition);
                                    PushToken(specialLength);
                                    tokenFound = true;
                                }
                            }
                            if (!tokenFound)
                                tokenFail = true;
                        }
                        break;
                    case TokenState.Comment:
                        PushToken(tokenBufferIndex);
                        bool blockWarning = false;
                        while (true)
                        {
                            if (c == '\0')
                                break;
                            if (blockComment)
                            {
                                if (blockWarning && c == '/')
                                    break;
                                blockWarning = c == '*';
                            }
                            else if (c == '\n')
                                break;
                            currentPosition++;
                            c = NextChar();
                        }
                        state = TokenState.None;
                        break;
                }
                if (c == '/')
                    commentWarning = true;
                else
                    commentWarning = false;
            }
            return token;
        }
    }
}
