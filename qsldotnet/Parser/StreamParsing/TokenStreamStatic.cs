using System.Linq;
using System.Collections.Generic;

namespace qsldotnet
{
    internal partial class TokenStream
    {
        private const int DefaultTokenBufferSize = 64;

        static Dictionary<string, StreamTokenType> tokenMap = new Dictionary<string, StreamTokenType>()
        {
            { "new" , StreamTokenType.New },
            { "var" , StreamTokenType.Var },
            { "function" , StreamTokenType.Function },
            { "class" , StreamTokenType.Class },
            { "if" , StreamTokenType.If },
            { "else" , StreamTokenType.Else },
            { "elseif" , StreamTokenType.ElseIF },
            { "elif" , StreamTokenType.ElseIF },
            { "continue" , StreamTokenType.Continue },
            { "break" , StreamTokenType.Break },
            { "while" , StreamTokenType.While },
            { "in" , StreamTokenType.In },
            { "foreach" , StreamTokenType.Foreach },
            { "using" , StreamTokenType.Using },
            { "this" , StreamTokenType.This },
            { "global" , StreamTokenType.Global },
            { "return" , StreamTokenType.Return },
            { "null" , StreamTokenType.Null },
            { "unassigned" , StreamTokenType.Unassigned },
            { "try" , StreamTokenType.Try },
            { "catch" , StreamTokenType.Catch },
            { "finally" , StreamTokenType.Finally },
            { "true" , StreamTokenType.TrueValue },
            { "false" , StreamTokenType.FalseValue },
            { "is" , StreamTokenType.Operator },
            { "as" , StreamTokenType.Operator },
        };



        static StreamTokenType SymbolTokenType(string text)
        {
            text = text.ToLower();
            if (tokenMap.ContainsKey(text))
                return tokenMap[text];
            return StreamTokenType.Symbol;
        }

        static StreamTokenType SpecialTokenType(string text)
        {
            switch (text)
            {
                case "[":
                    return StreamTokenType.SquareBracketOpen;
                case "]":
                    return StreamTokenType.SquareBracketClose;
                case "{":
                    return StreamTokenType.BlockStart;
                case "}":
                    return StreamTokenType.BlockEnd;
                case "(":
                    return StreamTokenType.RoundBracketOpen;
                case ")":
                    return StreamTokenType.RoundBracketClose;
                case ",":
                    return StreamTokenType.Comma;
                case ";":
                    return StreamTokenType.Semicolon;
                case ":":
                    return StreamTokenType.Colon;
                case "?":
                    return StreamTokenType.QuestionMark;
                case "=":
                    return StreamTokenType.Assign;
                default:
                    return StreamTokenType.Operator;
            }
        }


        static bool ValidSymbolBegin(char c)
        {
            return c == '_' || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
        }

        static bool ValidSymbolMiddle(char c)
        {
            return c == '_' || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9');

        }

        static TokenStream()
        {
            int maxLength = 0;
            foreach(string special in Specials)
                if (special.Length > maxLength)
                    maxLength = special.Length;

            SpecialsMap = new Dictionary<string, int>[maxLength];
            for (int i=0;i<SpecialsMap.Length;i++)
                SpecialsMap[i] = new Dictionary<string, int>();

            foreach (string special in Specials)
                for (int i = 0; i < special.Length; i++)
                {
                    string subline = special.Substring(0, i + 1);
                    if (!SpecialsMap[i].ContainsKey(subline))
                        SpecialsMap[i][subline] = 0;
                    SpecialsMap[i][subline]++;
                }
        }

        static Dictionary<string,int>[] SpecialsMap;

        static HashSet<string> Specials = new HashSet<string>()
        {
            "+",
            "-",
            "*",
            ".",
            "/",
            "%",
            "&",
            "^",
            "<",
            ">",
            "!",
            "|",
            "~",
            "=",
            "**",
            "+=",
            "-=",
            "*=",
            "/=",
            "%=",
            "&=",
            "|=",
            "^=",
            "<<",
            ">>",
            "!=",
            "==",
            ">=",
            "<=",
            "&&",
            "++",
            "--",
            "||",
            "?",
            ":",
            ",",
            "(",
            ")",
            "{",
            "}",
            "[",
            "]",
            ";"
        };
    }
}
