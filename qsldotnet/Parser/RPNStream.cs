using System.IO;
using System.Collections.Generic;

namespace qsldotnet.core
{
    internal class RpnStream
    {
        TokenStream _tokenStream;
        RpnToken lastCommitedToken;
        internal Stack<RpnToken> tokenStack = new Stack<RpnToken>();
        internal Queue<RpnToken> rpnQueue = new Queue<RpnToken>();
        bool functionBrackets;

        internal RpnStream(StreamReader stream, bool brackets = false) : this(new TokenStream(stream), brackets)
        {

        }

        internal RpnStream(TokenStream tokenStream, bool functionBrackets = false)
        {
            _tokenStream = tokenStream;
            this.functionBrackets = functionBrackets;
            if (functionBrackets)
            {
                RpnToken startToken = new RpnToken("{", RpnTokenType.StartFunctionBlock);
                tokenStack.Push(startToken);
                rpnQueue.Enqueue(startToken);
            }

        }

        internal RpnStream(string text, bool brackets = false) : this(new TokenStream(text), brackets)
        {
        }

        bool finished = false;

        private StreamToken NextCodeToken()
        {
            return _tokenStream.Next();
        }

        private RpnToken InternalNext()
        {
            RpnToken result = new RpnToken("", RpnTokenType.End);


            while (true)
            {
                if (rpnQueue.Count > 0)
                    return rpnQueue.Dequeue();
                if (finished)
                    break;

                StreamToken token = NextCodeToken();

                switch (token.Type)
                {
                    case StreamTokenType.Global:
                        return new RpnToken(token.Text, RpnTokenType.Global);
                    case StreamTokenType.This:
                        return new RpnToken(token.Text, RpnTokenType.This);
                    case StreamTokenType.Null:
                        return new RpnToken(token.Text, RpnTokenType.Null);
                    case StreamTokenType.Unassigned:
                        return new RpnToken(token.Text, RpnTokenType.Unassigned);
                    case StreamTokenType.Symbol:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Type == RpnTokenType.Var)
                        {
                            tokenStack.Pop();
                            return new RpnToken(token.Text, RpnTokenType.NewSymbol);
                        }
                        return new RpnToken(token.Text, RpnTokenType.Symbol);
                    case StreamTokenType.Member:
                        return new RpnToken(token.Text.Substring(1), RpnTokenType.Member);
                    case StreamTokenType.TrueValue:
                        return new RpnToken(token.Text, RpnTokenType.Value, true);
                    case StreamTokenType.FalseValue:
                        return new RpnToken(token.Text, RpnTokenType.Value, false);
                    case StreamTokenType.IntValue:
                        return new RpnToken(token.Text, RpnTokenType.Value, int.Parse(token.Text));
                    case StreamTokenType.LongValue:
                        return new RpnToken(token.Text, RpnTokenType.Value, long.Parse(token.Text));
                    case StreamTokenType.FloatValue:
                        return new RpnToken(token.Text, RpnTokenType.Value, float.Parse(token.Text));
                    case StreamTokenType.DoubleValue:
                        return new RpnToken(token.Text, RpnTokenType.Value, double.Parse(token.Text));
                    case StreamTokenType.StringValue:
                        return new RpnToken(token.Text, RpnTokenType.Value, token.Text);
                    case StreamTokenType.Break:
                        return new RpnToken(token.Text, RpnTokenType.Break);
                    case StreamTokenType.Continue:
                        return new RpnToken(token.Text, RpnTokenType.Continue);
                    case StreamTokenType.CharValue:
                        if (token.Text.Length < 1)
                            throw new ParserException(token, "Invalid Token");
                        if (token.Text.Length == 1)
                            return new RpnToken(token.Text, RpnTokenType.Value, token.Text[0]);
                        else
                            return new RpnToken(token.Text, RpnTokenType.Value, token.Text);


                    case StreamTokenType.Assign:
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Assign));
                        break;
                    case StreamTokenType.Colon:
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Colon));
                        break;
                    case StreamTokenType.New:
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.New));
                        break;
                    case StreamTokenType.Var:
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Var));
                        break;
                    case StreamTokenType.Operator:

                        if (token.Text == "++" || token.Text == "--")
                        {
                            if (
                                _tokenStream.PreviousToken.Type == StreamTokenType.Member ||
                                _tokenStream.PreviousToken.Type == StreamTokenType.Symbol ||
                                _tokenStream.PreviousToken.Type == StreamTokenType.SquareBracketClose)
                            {
                                token.Text = "?" + token.Text;
                            }
                            else
                            {
                                token.Text = token.Text + "?";
                            }
                        }
                        if (token.Text == "-" || token.Text == "+")
                        {
                            if (
                                _tokenStream.PreviousToken.Type != StreamTokenType.Member &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.Symbol &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.SquareBracketClose &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.RoundBracketClose &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.IntValue &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.StringValue &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.CharValue &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.LongValue &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.FloatValue &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.DoubleValue &&
                                _tokenStream.PreviousToken.Type != StreamTokenType.EvalValue
                                )
                                token.Text += "?";

                        }
                        ScriptOperator op1 = ScriptExecutor.FindOperator(token.Text);
                        if (op1.Type == ScriptOperatorType.PostUnaryOperator)
                            return new RpnToken(token.Text, RpnTokenType.Operator);
                        while (true)
                        {
                            if (tokenStack.Count > 0)
                            {
                                RpnToken opPeekToken = tokenStack.Peek();
                                if (opPeekToken.Type == RpnTokenType.Operator)
                                {
                                    ScriptOperator op2 = ScriptExecutor.FindOperator(opPeekToken.Text);
                                    if (
                                        (op1.Type == ScriptOperatorType.RightOperator && op1.Priority <= op2.Priority) ||
                                        (op1.Type == ScriptOperatorType.LeftOperator && op1.Priority < op2.Priority) ||
                                        (op2.Type == ScriptOperatorType.UnaryOperator && op1.Type != ScriptOperatorType.UnaryOperator)
                                        )
                                    {
                                        rpnQueue.Enqueue(tokenStack.Pop());
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                            else
                                break;
                        }
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Operator));
                        break;
                    case StreamTokenType.SquareBracketOpen:
                        if (
                           _tokenStream.PreviousToken.Type == StreamTokenType.This ||
                           _tokenStream.PreviousToken.Type == StreamTokenType.Global ||
                           _tokenStream.PreviousToken.Type == StreamTokenType.Symbol ||
                           _tokenStream.PreviousToken.Type == StreamTokenType.Member ||
                           _tokenStream.PreviousToken.Type == StreamTokenType.Function ||
                           _tokenStream.PreviousToken.Type == StreamTokenType.Class ||
                           _tokenStream.PreviousToken.Type == StreamTokenType.RoundBracketClose ||
                           _tokenStream.PreviousToken.Type == StreamTokenType.SquareBracketClose ||
                           _tokenStream.PreviousToken.Type == StreamTokenType.BlockEnd)
                        {
                            tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartSquareCortage));
                            return tokenStack.Peek();
                        }
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.SquareBracesOpen));
                        return tokenStack.Peek();
                    case StreamTokenType.SquareBracketClose:
                        while (true)
                        {
                            if (tokenStack.Count == 0)
                                throw new ParserException(token, "Can't Find Opening Bracket");
                            RpnToken pop = tokenStack.Pop();

                            if (pop.Type == RpnTokenType.StartSquareCortage)
                            {
                                if (tokenStack.Count > 0 && tokenStack.Peek().Type == RpnTokenType.New)
                                {
                                    tokenStack.Pop();
                                    rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.EndSquareCortageConstructor));
                                }
                                else
                                    rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.EndSquareCortage));

                                break;
                            }
                            else if (pop.Type == RpnTokenType.SquareBracesOpen)
                            {
                                rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.SquareBracesClose));
                                break;
                            }
                            else
                            {
                                rpnQueue.Enqueue(pop);
                            }

                        }
                        break;
                    case StreamTokenType.RoundBracketOpen:

                        if (_tokenStream.PreviousToken.Type == StreamTokenType.Symbol ||
                            _tokenStream.PreviousToken.Type == StreamTokenType.Member ||
                            _tokenStream.PreviousToken.Type == StreamTokenType.Function ||
                            _tokenStream.PreviousToken.Type == StreamTokenType.Class ||
                            _tokenStream.PreviousToken.Type == StreamTokenType.RoundBracketClose ||
                            _tokenStream.PreviousToken.Type == StreamTokenType.SquareBracketClose ||
                            _tokenStream.PreviousToken.Type == StreamTokenType.BlockEnd)
                        {
                            tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartRoundCortage));
                            return tokenStack.Peek();
                        }
                        if (tokenStack.Count > 0 && tokenStack.Peek().Type == RpnTokenType.Foreach)
                            tokenStack.Push(new RpnToken(token.Text, RpnTokenType.ForeachRoundBracketOpen));
                        else
                            tokenStack.Push(new RpnToken(token.Text, RpnTokenType.RoundBracketOpen));
                        break;
                    case StreamTokenType.RoundBracketClose:
                        while (true)
                        {
                            if (tokenStack.Count == 0)
                                throw new ParserException(token, "Can't Find Opening Bracket");
                            RpnToken pop = tokenStack.Pop();

                            if (pop.Type == RpnTokenType.StartRoundCortage)
                            {
                                if (tokenStack.Count > 0 && tokenStack.Peek().Type == RpnTokenType.New)
                                {
                                    tokenStack.Pop();
                                    rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.EndRoundCortageConstructor));
                                }
                                else
                                    rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.EndRoundCortage));

                                break;
                            }
                            else if (pop.Type == RpnTokenType.RoundBracketOpen || pop.Type == RpnTokenType.ForeachRoundBracketOpen)
                            {
                                break;
                            }
                            else
                            {
                                rpnQueue.Enqueue(pop);
                            }
                        }

                        break;
                    case StreamTokenType.Comma:
                        while (true)
                        {
                            if (tokenStack.Count == 0)
                                throw new ParserException(token, "Unexpected");
                            RpnToken peek = tokenStack.Peek();
                            if (peek.Type == RpnTokenType.StartSquareCortage)
                            {
                                rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.ArrayArgumentsComma));
                                break;
                            }
                            if (peek.Type == RpnTokenType.StartRoundCortage)
                            {
                                rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.FunctionArgumentsComma));
                                break;
                            }
                            if (peek.Type == RpnTokenType.SquareBracesOpen)
                            {
                                rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.ListComma));
                                break;
                            }
                            if (peek.Type == RpnTokenType.StartBlock)
                            {
                                rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.BlockComma));
                                break;
                            }
                            rpnQueue.Enqueue(tokenStack.Pop());
                        }
                        break;
                    case StreamTokenType.Class:
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Constructor));
                        return tokenStack.Peek();
                    case StreamTokenType.Function:
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Function));
                        return tokenStack.Peek();
                    case StreamTokenType.If:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.If));
                        return tokenStack.Peek();
                    case StreamTokenType.Using:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Using));
                        return tokenStack.Peek();
                    case StreamTokenType.Try:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Try));
                        break;
                    case StreamTokenType.Catch:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Catch));
                        break;
                    case StreamTokenType.Finally:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Finally));
                        break;
                    case StreamTokenType.Else:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Else));
                        return tokenStack.Peek();
                    case StreamTokenType.ElseIF:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.ElseIf));
                        return tokenStack.Peek();
                    case StreamTokenType.While:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.While));
                        return tokenStack.Peek();
                    case StreamTokenType.Foreach:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Foreach));
                        break;
                    case StreamTokenType.In:
                        if (tokenStack.Count > 0)
                        {
                            RpnToken peek = tokenStack.Peek();
                            if (peek.Type == RpnTokenType.ForeachRoundBracketOpen)
                            {
                                tokenStack.Push(new RpnToken(token.Text, RpnTokenType.ForeachIn));
                                break;
                            }
                            if (peek.Type == RpnTokenType.Foreach)
                            {
                                throw new ParserException(token, "foreach expression must be inside ()");
                            }
                        }
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Operator));
                        break;
                    case StreamTokenType.Return:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Return));
                        break;
                    case StreamTokenType.Throw:
                        if (tokenStack.Count > 0 && tokenStack.Peek().Text != "{")
                            throw new ParserException(token, "Probably Forgotten \";\"");
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.Throw));
                        break;
                    case StreamTokenType.Semicolon:
                        while (tokenStack.Count > 0)
                        {
                            if (tokenStack.Peek().Text == "{")
                                break;
                            rpnQueue.Enqueue(tokenStack.Pop());
                        }
                        rpnQueue.Enqueue(new RpnToken(";", RpnTokenType.Semicolon));
                        break;
                    case StreamTokenType.BlockStart:
                        if (tokenStack.Count > 0)
                        {
                            RpnToken peek = tokenStack.Peek();
                            switch (peek.Type)
                            {
                                case RpnTokenType.While:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartWhileBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.Foreach:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartForeachBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.If:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartIfBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.Else:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartElseBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.ElseIf:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartElseIfBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.Constructor:
                                case RpnTokenType.Function:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartFunctionBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.Try:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartTryBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.Catch:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartCatchBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.Finally:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartFinallyBlock));
                                    return tokenStack.Peek();
                                case RpnTokenType.Using:
                                    tokenStack.Pop();
                                    tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartUsingBlock));
                                    return tokenStack.Peek();
                            }
                        }
                        tokenStack.Push(new RpnToken(token.Text, RpnTokenType.StartBlock));
                        return tokenStack.Peek();
                    case StreamTokenType.BlockEnd:
                        while (true)
                        {
                            if (tokenStack.Count == 0)
                                throw new ParserException(token, "Unexpected");
                            RpnToken pop = tokenStack.Pop();
                            if (pop.Text == "{")
                                break;
                            rpnQueue.Enqueue(pop);
                        }
                        rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.EndBlock));
                        break;

                    case StreamTokenType.End:
                        
                        if (functionBrackets)
                        {
                            while (true)
                            {
                                if (tokenStack.Count == 0)
                                    throw new ParserException(token, "Unexpected");
                                RpnToken pop = tokenStack.Pop();
                                if (pop.Text == "{")
                                    break;
                                rpnQueue.Enqueue(pop);
                            }
                            rpnQueue.Enqueue(new RpnToken(token.Text, RpnTokenType.EndBlock));
                            functionBrackets = false;
                        }
                        else
                        {
                            while (tokenStack.Count > 0)
                                rpnQueue.Enqueue(tokenStack.Pop());
                            finished = true;
                        }
                        break;

                }
            }
            return result;
        }

        internal RpnToken Next()
        {
            RpnToken result = InternalNext();
            lastCommitedToken = result;
            return result;
        }
    }
}
