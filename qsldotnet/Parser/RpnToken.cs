namespace qsldotnet
{
    internal enum RpnTokenType
    {
        Symbol,
        NewSymbol,
        ElseIf,
        Member,
        Value,
        Type,
        New,
        Var,
        If,
        Else,
        Null,
        Global,
        This,
        Unassigned,
        Return,
        Continue,
        Break,
        Function,
        Constructor,
        While,
        Foreach,
        In,
        ForeachIn,
        Operator,
        Assign,
        End,
        Try,
        Catch,
        Finally,
        Using,
        RoundBracketOpen,
        RoundBracketClose,
        ForeachRoundBracketOpen,
        StartRoundCortage,
        EndRoundCortage,
        EndRoundCortageConstructor,
        SquareBracesOpen,
        SquareBracesClose,
        StartSquareCortage,
        EndSquareCortage,
        EndSquareCortageConstructor,
        StartBlock,
        StartFunctionBlock,
        StartWhileBlock,
        StartForeachBlock,
        StartIfBlock,
        StartElseBlock,
        StartElseIfBlock,
        StartTryBlock,
        StartCatchBlock,
        StartFinallyBlock,
        StartUsingBlock,
        Colon,
        EndBlock,
        Semicolon,
        BlockComma,
        ArrayArgumentsComma,
        FunctionArgumentsComma,
        ListComma,
        Throw,


    }

    internal struct RpnToken
    {
        internal string Text;
        internal object Value;
        internal RpnTokenType Type;

        internal RpnToken(string text, RpnTokenType type, object value)
        {
            Text = text;
            Type = type;
            Value = value;
        }

        internal RpnToken(string text, RpnTokenType type)
        {
            Text = text;
            Type = type;
            Value = null;
        }

        public override string ToString()
        {
            return string.Format("[{0}] '{1}'", Type, Text);
        }
    }
}
