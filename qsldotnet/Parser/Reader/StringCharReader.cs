﻿namespace qsldotnet
{
    internal class StringCharReader : CharReader
    {
        string str;
        int index = 0;

        internal StringCharReader(string str)
        {
            this.str = str;
        }

        internal override bool Done
        {
            get
            {
                return index >= str.Length;
            }
        }

        internal override char ReadChar()
        {
            if (index >= str.Length)
                return '\0';
            return str[index++];
        }
    }
}
