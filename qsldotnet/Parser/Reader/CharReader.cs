﻿namespace qsldotnet
{
    internal abstract class CharReader
    {
        internal abstract bool Done { get; }
        internal abstract char ReadChar();
    }
}
