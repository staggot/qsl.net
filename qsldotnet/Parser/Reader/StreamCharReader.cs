﻿using System;
using System.IO;
namespace qsldotnet
{
    internal class StreamCharReader : CharReader
    {
        char[] buffer;
        int bufferLength = 0;
        int bufferIndex = 0;
        StreamReader stream;

        internal StreamCharReader(StreamReader stream, int bufferSize = 1024)
        {
            this.stream = stream;
            this.buffer = new char[bufferSize];
            this.bufferIndex = buffer.Length;
        }


        bool BufferDone
        {
            get
            {
                return bufferIndex >= bufferLength;
            }
        }

        bool StreamDone
        {
            get { return stream.EndOfStream; }
        }

        void FillBuffer()
        {
            bufferLength = stream.Read(buffer, 0, buffer.Length);
            bufferIndex = 0;
        }

        internal override bool Done
        {
            get
            {
                return StreamDone && BufferDone;
            }
        }


        internal override char ReadChar()
        {
            if (BufferDone)
                FillBuffer();
            if (Done)
                return '\0';
            return buffer[bufferIndex++];
        }
    }
}
