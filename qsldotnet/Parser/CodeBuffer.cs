using System;

namespace qsldotnet.core
{
    internal class CodeBuffer
    {
        RpnStream stream;
        
        long currentPC = 0;

        RpnToken[] data;

        int head = 0;
        int tail = 0;
        int capacity;
        int count = 0;

        private long lastNeededPC = 0;


        internal long LastNeededPC
        {
            get { return lastNeededPC; }
            set
            {
                lastNeededPC = value;
                while (currentPC < value)
                {
                    currentPC++;
                    count--;
                    tail = (tail + 1) % capacity;
                }
            }
        }

        internal long LastReadedPC
        {
            get
            {
                return currentPC + count - 1;
            }
        }

        internal int Count
        {
            get { return count; }
        }

        internal int Capacity
        {
            get { return capacity; }
        }

        internal RpnToken this[long pc]
        {
            get
            {
                while (LastReadedPC < pc)
                    ReadNext();
                return data[(pc - currentPC + tail) % capacity];
            }
        }

        void ReadNext()
        {
            if (count == capacity)
            {
                Reallocate(capacity * 2);
            }
            count++;
            data[head] = stream.Next();
            head = (head + 1) % capacity;
        }

        void Reallocate(int newCapacity)
        {
            if (newCapacity < count)
                throw new Exception();
            RpnToken[] newData = new RpnToken[newCapacity];
            for (int i = 0; i < count; i++)
            {
                newData[i] = data[(head + i) % capacity];
            }
            head = 0;
            tail = count;
            data = newData;
            capacity = newCapacity;
        }

        internal CodeBuffer(RpnStream stream) : this(stream, 512)
        {
        }

        internal CodeBuffer(RpnStream stream, int initialCapacity)
        {
            this.stream = stream;
            capacity = initialCapacity;
            data = new RpnToken[capacity];
        }
    }
}
