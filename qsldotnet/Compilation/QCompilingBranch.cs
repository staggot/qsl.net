﻿using System;
using System.Reflection.Emit;

namespace qsldotnet.core
{
    internal struct CompilationBranch
    {
        internal BranchEnum Type;
        internal Label StartLabel;
        internal Label EndLabel;
        internal LocalBuilder Iterator;
        internal LocalBuilder Enumerator;
    }
}
