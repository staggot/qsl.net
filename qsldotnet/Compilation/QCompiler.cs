﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace qsldotnet.core
{
    internal static class QCompiler
    {
        internal static void EmitValue(ILGenerator generator, object obj)
        {
            if (obj is null)
            {
                generator.Emit(OpCodes.Ldnull);
            }
            else if (obj is int)
            {
                generator.Emit(OpCodes.Ldc_I4, (int)obj);
                generator.Emit(OpCodes.Box, typeof(int));
            }
            else if (obj is uint)
            {
                generator.Emit(OpCodes.Ldc_I4, (uint)obj);
                generator.Emit(OpCodes.Box, typeof(uint));
            }
            else if (obj is long)
            {
                generator.Emit(OpCodes.Ldc_I8, (long)obj);
                generator.Emit(OpCodes.Box, typeof(long));
            }
            else if (obj is ulong)
            {
                generator.Emit(OpCodes.Ldc_I8, (ulong)obj);
                generator.Emit(OpCodes.Box, typeof(ulong));
            }
            else if (obj is double)
            {
                generator.Emit(OpCodes.Ldc_R8, (double)obj);
                generator.Emit(OpCodes.Box, typeof(double));
            }
            else if (obj is float)
            {
                generator.Emit(OpCodes.Ldc_R4, (float)obj);
                generator.Emit(OpCodes.Box, typeof(float));
            }
            else if (obj is bool)
            {
                generator.Emit(OpCodes.Ldc_I4, (bool)obj ? 1 : 0);
                generator.Emit(OpCodes.Box, typeof(bool));
            }
            else if (obj is string)
            {
                generator.Emit(OpCodes.Ldstr, (string)obj);
            }
            else if (obj is char)
            {
                generator.Emit(OpCodes.Ldc_I4, (char)obj);
                generator.Emit(OpCodes.Box, typeof(char));
            }
            else if (obj is short)
            {
                generator.Emit(OpCodes.Ldc_I4, (short)obj);
                generator.Emit(OpCodes.Box, typeof(short));
            }
            else if (obj is ushort)
            {
                generator.Emit(OpCodes.Ldc_I4, (ushort)obj);
                generator.Emit(OpCodes.Box, typeof(ushort));
            }
            else if (obj is byte)
            {
                generator.Emit(OpCodes.Ldc_I4, (byte)obj);
                generator.Emit(OpCodes.Box, typeof(byte));
            }
            else if (obj is sbyte)
            {
                generator.Emit(OpCodes.Ldc_I4, (sbyte)obj);
                generator.Emit(OpCodes.Box, typeof(sbyte));
            }
            else
            {
                throw new CompilerException(null, "Unsupportet Emit Type: " + obj.GetType());
            }
        }

        internal static QFunction CompileSource(QModule module, TokenStream sourceStream)
        {
            RpnStream rpnStream = new RpnStream(sourceStream, true);
            CodeBuffer code = new CodeBuffer(rpnStream);
            long PC = 0;

            DynamicMethod funcMethod =
                new DynamicMethod(
                    Guid.NewGuid().ToString(),
                    MethodAttributes.Static | MethodAttributes.Public,
                    CallingConventions.Standard,
                    typeof(object),
                    new Type[] { typeof(QModule), typeof(FunctionArguments) },
                    typeof(QEngine),
                    true);

            ILGenerator ilGen = funcMethod.GetILGenerator();

            LocalBuilder contextLocal = ilGen.DeclareLocal(typeof(QContext));
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Callvirt, QContext.CreateContextMethod);
            ilGen.Emit(OpCodes.Stloc, contextLocal);

            //Arguments
            ilGen.Emit(OpCodes.Ldarg_1);
            //Context
            ilGen.Emit(OpCodes.Ldloc, contextLocal);

            //passing arguments to context
            ilGen.Emit(OpCodes.Call, ScriptExecutor.FunctionArgumentsAppyDictMethod);

            LocalBuilder stackLocal = CompileFunction(module, ilGen, code, ref PC, contextLocal, -1);

            ilGen.Emit(OpCodes.Ldloc, stackLocal);
            ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopReturnMethod);
            ilGen.Emit(OpCodes.Ret);

            return funcMethod.CreateDelegate(typeof(QFunction), module) as QFunction;
        }



        internal static int CompileFunction(QModule module, int thisDeep, CodeBuffer codeBuffer, ref long PC)
        {
            DynamicMethod funcMethod =
                new DynamicMethod(
                    Guid.NewGuid().ToString(),
                    MethodAttributes.Static | MethodAttributes.Public,
                    CallingConventions.Standard,
                    typeof(object),
                    new Type[] { typeof(QContext), typeof(FunctionArguments) },
                    typeof(QContext),
                    true);

            ILGenerator ilGen = funcMethod.GetILGenerator();

            LocalBuilder contextLocal = EmitReadArguments(module, ilGen, codeBuffer, ref PC);

            LocalBuilder stackLocal = CompileFunction(module, ilGen, codeBuffer, ref PC, contextLocal, thisDeep);
            ilGen.Emit(OpCodes.Ldloc, stackLocal);
            ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopReturnMethod);
            ilGen.Emit(OpCodes.Ret);

            return module.AddCompiledMethod(funcMethod);
        }

        internal static int CompileConstructor(QModule module, CodeBuffer codeBuffer, ref long PC)
        {
            string id = Guid.NewGuid().ToString();
            DynamicMethod funcMethod =
                new DynamicMethod(
                    id,
                    MethodAttributes.Static | MethodAttributes.Public,
                    CallingConventions.Standard,
                    typeof(QContext),
                    new Type[] { typeof(QContext), typeof(FunctionArguments) },
                    typeof(QContext),
                    true);
            ILGenerator ilGen = funcMethod.GetILGenerator();

            LocalBuilder contextLocal = EmitReadArguments(module, ilGen, codeBuffer, ref PC);

            ilGen.Emit(OpCodes.Ldloc, contextLocal);
            ilGen.Emit(OpCodes.Ldstr, id);
            ilGen.Emit(OpCodes.Stfld, typeof(QContext).GetField(nameof(QContext.ClassId)));

            LocalBuilder stackLocal = CompileFunction(module, ilGen, codeBuffer, ref PC, contextLocal, 0);

            ilGen.Emit(OpCodes.Ldloc, contextLocal);
            ilGen.Emit(OpCodes.Ret);

            return module.AddCompiledMethod(funcMethod);
        }

        private static LocalBuilder EmitReadArguments(QModule engine, ILGenerator ilGen, CodeBuffer codeBuffer, ref long PC)
        {
            LocalBuilder contextLocal = ilGen.DeclareLocal(typeof(QContext));
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Callvirt, QContext.CreateContextMethod);
            ilGen.Emit(OpCodes.Stloc, contextLocal);
            byte argsState = 0;
            List<string> argNames = new List<string>();
            List<object> argValues = new List<object>();
            bool readingArgs = true;
            while (readingArgs)
            {
                RpnToken argToken = codeBuffer[PC++];

                switch (argsState)
                {
                    case 0:
                        if (argToken.Text != "(")
                            throw new CompilerException(engine.Engine, string.Format("Expected \"(\" Instead \"{0}\"", argToken.Text));
                        argsState = 1;
                        break;
                    case 1:
                        if (argToken.Type == RpnTokenType.Symbol)
                        {
                            argNames.Add(argToken.Text);
                            argsState = 2;

                        }
                        else if (argToken.Text == ")")
                        {
                            readingArgs = false;
                        }
                        else
                            throw new CompilerException(engine.Engine, string.Format("Expected \")\" Or (Symbol) Instead \"{0}\"", argToken.Text));

                        break;
                    case 2:
                        if (argToken.Type == RpnTokenType.FunctionArgumentsComma)
                        {
                            argsState = 1;
                            argValues.Add(null);
                        }
                        else if (argToken.Type == RpnTokenType.Value)
                        {
                            argValues.Add(argToken.Value);
                            argsState = 3;
                        }
                        else if (argToken.Text == ")")
                        {
                            argValues.Add(null);
                            readingArgs = false;
                        }
                        else
                            throw new CompilerException(engine.Engine, string.Format("Expected \")\" Or \",\" Instead \"{0}\"", argToken.Text));
                        break;
                    case 3:
                        if (argToken.Type == RpnTokenType.FunctionArgumentsComma)
                            argsState = 1;
                        else if (argToken.Text == ")")
                            readingArgs = false;
                        else if (argToken.Text != "=")
                            throw new CompilerException(engine.Engine, string.Format("Expected \"=\" Between Argument Name And Default Value Instead \"{0}\"", argToken.Text));

                        break;
                }
            }


            //Arguments
            ilGen.Emit(OpCodes.Ldarg_1);
            //String Array
            ilGen.Emit(OpCodes.Ldc_I4_S, argNames.Count);
            ilGen.Emit(OpCodes.Newarr, typeof(string));
            for (int i = 0; i < argNames.Count; i++)
            {
                ilGen.Emit(OpCodes.Dup);
                ilGen.Emit(OpCodes.Ldc_I4, i);
                ilGen.Emit(OpCodes.Ldstr, argNames[i]);
                ilGen.Emit(OpCodes.Stelem_Ref);
            }

            ///////////////////////
            //DefaultValues
            ilGen.Emit(OpCodes.Ldc_I4_S, argValues.Count);
            ilGen.Emit(OpCodes.Newarr, typeof(object));
            for (int i = 0; i < argValues.Count; i++)
            {
                ilGen.Emit(OpCodes.Dup);
                ilGen.Emit(OpCodes.Ldc_I4, i);
                EmitValue(ilGen, argValues[i]);
                ilGen.Emit(OpCodes.Stelem_Ref);
            }
            /////////////////////////
            /// 
            /// 
            //Context
            ilGen.Emit(OpCodes.Ldloc, contextLocal);

            //passing arguments to context
            ilGen.Emit(OpCodes.Call, ScriptExecutor.AppyInsideMethod);
            return contextLocal;
        }

        #region Big Compiler
        private static LocalBuilder CompileFunction(QModule module, ILGenerator ilGen, CodeBuffer codeBuffer, ref long PC, LocalBuilder contextLocal, int thisDeep)
        {
            LocalBuilder stackLocal = ilGen.DeclareLocal(typeof(ExecutionStack));
            ilGen.Emit(OpCodes.Ldloc, contextLocal);
            ilGen.Emit(OpCodes.Call, ScriptExecutor.CreateStackMethod);
            ilGen.Emit(OpCodes.Stloc, stackLocal);
            Label returnLabel = ilGen.DefineLabel();
            //Preparation Finished
            RpnToken token = codeBuffer[PC++];
            if (token.Type != RpnTokenType.StartFunctionBlock)
                throw new CompilerException(module.Engine, string.Format("Expected \"{{\" Instead \"{0}\"", token.Text));
            List<CompilationBranch> functionBranchStack = new List<CompilationBranch>();
            CompilationBranch currentBranch;
            bool compiling = true;
            while (compiling)
            {
                token = codeBuffer[PC++];

                switch (token.Type)
                {
                    case RpnTokenType.SquareBracesOpen:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteStartListMethod);
                        break;
                    case RpnTokenType.SquareBracesClose:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteEndListMethod);
                        break;
                    case RpnTokenType.ListComma:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteListCommaMethod);
                        break;
                    case RpnTokenType.StartBlock:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteStartBlockMethod);
                        break;
                    case RpnTokenType.Colon:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteBlockAssignMethod);
                        break;
                    case RpnTokenType.Assign:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteAssignMethod);
                        break;
                    case RpnTokenType.Throw:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Callvirt, ScriptExecutor.ExecuteThrowMethod);
                        break;
                    case RpnTokenType.This:
                        if (thisDeep < 0)
                        {
                            throw new CompilerException(module.Engine, "(this) Can Be Used Only In Constructor Definition");
                        }
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldc_I4, thisDeep);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteThisMethod);
                        break;
                    case RpnTokenType.Global:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteGlobalMethod);
                        break;
                    case RpnTokenType.Symbol:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldstr, token.Text);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteSymbolMethod);
                        break;
                    case RpnTokenType.NewSymbol:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldstr, token.Text);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteNewSymbolMethod);
                        break;
                    case RpnTokenType.Member:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldstr, token.Text);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteMemberMethod);
                        break;
                    case RpnTokenType.Operator:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldstr, token.Text);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteOperatorMethod);
                        break;
                    case RpnTokenType.Value:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);

                        EmitValue(ilGen, token.Value);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteValueMethod);
                        break;
                    case RpnTokenType.Null:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldnull);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteValueMethod);
                        break;
                    case RpnTokenType.Unassigned:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);

                         ilGen.Emit(OpCodes.Call, Unassigned.GetUnassignedMethod);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteValueMethod);
                        break;
                    case RpnTokenType.StartRoundCortage:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteStartArgumentsMethod);
                        break;
                    case RpnTokenType.Semicolon:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteSemicolonMethod);
                        break;
                    case RpnTokenType.EndRoundCortage:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteEndCortageFunctionCallMethod);
                        break;
                    case RpnTokenType.EndRoundCortageConstructor:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteEndCortageConstructorCallMethod);
                        break;
                    case RpnTokenType.StartSquareCortage:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteStartCortageMethod);
                        break;
                    case RpnTokenType.EndSquareCortage:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteEndArrayCortageMethod);
                        break;
                    case RpnTokenType.EndSquareCortageConstructor:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteEndContructorArrayCortageMethod);
                        break;

                    case RpnTokenType.ArrayArgumentsComma:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteArrayArgumentsCommaMethod);
                        break;
                    case RpnTokenType.FunctionArgumentsComma:
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteFunctonArgumentsCommaMethod);
                        break;
                    case RpnTokenType.Break:
                        int breakIndex = functionBranchStack.Count - 1;
                        while (breakIndex >= 0)
                        {
                            if (
                                functionBranchStack[breakIndex].Type == BranchEnum.While ||
                                functionBranchStack[breakIndex].Type == BranchEnum.Foreach
                                )
                            {
                                ilGen.Emit(OpCodes.Br, functionBranchStack[breakIndex].EndLabel);
                                break;
                            }
                            breakIndex--;
                        }
                        break;
                    case RpnTokenType.Continue:
                        int continueIndex = functionBranchStack.Count - 1;
                        while (continueIndex >= 0)
                        {
                            if (
                                functionBranchStack[continueIndex].Type == BranchEnum.While ||
                                functionBranchStack[continueIndex].Type == BranchEnum.Foreach
                                )
                            {
                                ilGen.Emit(OpCodes.Br, functionBranchStack[continueIndex].StartLabel);
                                break;
                            }
                            continueIndex--;
                        }
                        break;
                    case RpnTokenType.While:
                        currentBranch = new CompilationBranch()
                        {
                            StartLabel = ilGen.DefineLabel(),
                            EndLabel = ilGen.DefineLabel(),
                            Type = BranchEnum.While
                        };
                        functionBranchStack.Add(currentBranch);
                        ilGen.MarkLabel(currentBranch.StartLabel);
                        break;
                    case RpnTokenType.ForeachIn:
                        currentBranch = new CompilationBranch()
                        {
                            StartLabel = ilGen.DefineLabel(),
                            EndLabel = ilGen.DefineLabel(),
                            Type = BranchEnum.Foreach,
                            Iterator = ilGen.DeclareLocal(typeof(ExecutionStackWord)),
                            Enumerator = ilGen.DeclareLocal(typeof(IEnumerator))
                        };
                        functionBranchStack.Add(currentBranch);
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteGetEnumeratorMethod);

                        //enumerator
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopValueMethod);
                        ilGen.Emit(OpCodes.Stloc, currentBranch.Enumerator);

                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopMethod);
                        ilGen.Emit(OpCodes.Stloc, currentBranch.Iterator);
                        break;

                    case RpnTokenType.If:
                        currentBranch = new CompilationBranch()
                        {
                            StartLabel = ilGen.DefineLabel(),
                            EndLabel = ilGen.DefineLabel(),
                            Type = BranchEnum.If

                        };
                        functionBranchStack.Add(currentBranch);
                        break;
                    case RpnTokenType.StartTryBlock:
                        currentBranch = new CompilationBranch()
                        {
                            EndLabel = ilGen.DefineLabel(),
                            Type = BranchEnum.Try,
                        };
                        ilGen.BeginExceptionBlock();
                        functionBranchStack.Add(currentBranch);
                        break;
                    case RpnTokenType.StartCatchBlock:
                        throw new CompilerException(module.Engine, "Unexpected catch block");
                    case RpnTokenType.StartUsingBlock:
                        currentBranch = new CompilationBranch()
                        {
                            EndLabel = ilGen.DefineLabel(),
                            Type = BranchEnum.Using,
                            Enumerator = ilGen.DeclareLocal(typeof(IDisposable))
                        };
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopValueMethod);
                        ilGen.Emit(OpCodes.Stloc, currentBranch.Enumerator);
                        ilGen.BeginExceptionBlock();
                        functionBranchStack.Add(currentBranch);
                        break;
                    case RpnTokenType.StartIfBlock:
                        currentBranch = functionBranchStack[functionBranchStack.Count - 1];
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopTrueMethod);
                        ilGen.Emit(OpCodes.Brfalse, currentBranch.StartLabel);
                        break;
                    case RpnTokenType.StartElseIfBlock:
                        currentBranch = functionBranchStack[functionBranchStack.Count - 1];
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopTrueMethod);
                        ilGen.Emit(OpCodes.Brfalse, currentBranch.StartLabel);
                        break;
                    case RpnTokenType.StartElseBlock:
                        break;
                    case RpnTokenType.Return:
                        ilGen.Emit(OpCodes.Br, returnLabel);
                        break;
                    case RpnTokenType.StartWhileBlock:
                        currentBranch = functionBranchStack[functionBranchStack.Count - 1];
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopTrueMethod);
                        ilGen.Emit(OpCodes.Brfalse, currentBranch.EndLabel);
                        break;
                    case RpnTokenType.StartForeachBlock:
                        currentBranch = functionBranchStack[functionBranchStack.Count - 1];
                        ilGen.MarkLabel(currentBranch.StartLabel);
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldloc, currentBranch.Enumerator);
                        ilGen.Emit(OpCodes.Ldloc, currentBranch.Iterator);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteNextForeachMethod);
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Callvirt, ExecutionStack.PopTrueMethod);
                        ilGen.Emit(OpCodes.Brfalse, currentBranch.EndLabel);
                        break;
                    case RpnTokenType.Constructor:
                        int idConstructor = CompileConstructor(module, codeBuffer, ref PC);
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldc_I4, idConstructor);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteCompiledConstructorMethod);
                        break;
                    case RpnTokenType.Function:
                        int idFunc = CompileFunction(module, thisDeep >= 0 ? thisDeep + 1 : -1, codeBuffer, ref PC);
                        ilGen.Emit(OpCodes.Ldloc, stackLocal);
                        ilGen.Emit(OpCodes.Ldc_I4, idFunc);
                        ilGen.Emit(OpCodes.Call, ScriptExecutor.ExecuteCompiledFunctionMethod);
                        break;
                    case RpnTokenType.EndBlock:
                        if (functionBranchStack.Count == 0)
                        {
                            compiling = false;
                            break;
                        }
                        currentBranch = functionBranchStack[functionBranchStack.Count - 1];
                        switch (currentBranch.Type)
                        {
                            case BranchEnum.Using:
                                ilGen.Emit(OpCodes.Leave, currentBranch.EndLabel);
                                //finally  {if (value is {}) value.Dispose(); }
                                ilGen.BeginFinallyBlock();
                                Label disposeLabel = ilGen.DefineLabel();
                                ilGen.Emit(OpCodes.Ldloc, currentBranch.Enumerator);
                                ilGen.Emit(OpCodes.Ldnull);
                                ilGen.Emit(OpCodes.Cgt_Un);
                                ilGen.Emit(OpCodes.Brfalse, disposeLabel);
                                ilGen.Emit(OpCodes.Ldloc, currentBranch.Enumerator);
                                ilGen.Emit(OpCodes.Callvirt, typeof(IDisposable).GetMethod(nameof(IDisposable.Dispose)));
                                ilGen.MarkLabel(disposeLabel);
                                functionBranchStack.RemoveAt(functionBranchStack.Count - 1);
                                ilGen.Emit(OpCodes.Leave, currentBranch.EndLabel);
                                ilGen.EndExceptionBlock();
                                ilGen.MarkLabel(currentBranch.EndLabel);
                                break;
                            case BranchEnum.Try:
                                ilGen.Emit(OpCodes.Leave, currentBranch.EndLabel);
                                functionBranchStack.RemoveAt(functionBranchStack.Count - 1);
                                if (codeBuffer[PC].Type == RpnTokenType.StartCatchBlock)
                                {
                                    PC++;

                                    ilGen.BeginCatchBlock(typeof(Exception));
                                    LocalBuilder errLocal = ilGen.DeclareLocal(typeof(Exception));
                                    ilGen.Emit(OpCodes.Stloc, errLocal);
                                    ilGen.Emit(OpCodes.Ldloc, contextLocal);
                                    ilGen.Emit(OpCodes.Ldstr, "_error");
                                    ilGen.Emit(OpCodes.Ldloc, errLocal);
                                    ilGen.Emit(OpCodes.Call, ScriptExecutor.SetVarLocalMethod);

                                    currentBranch = new CompilationBranch()
                                    {
                                        Type = BranchEnum.Catch,
                                        EndLabel = currentBranch.EndLabel

                                    };
                                    functionBranchStack.Add(currentBranch);
                                }
                                else if (codeBuffer[PC].Type == RpnTokenType.StartFinallyBlock)
                                {
                                    PC++;
                                    ilGen.BeginFinallyBlock();

                                    currentBranch = new CompilationBranch()
                                    {
                                        Type = BranchEnum.Finally,
                                        EndLabel = currentBranch.EndLabel

                                    };
                                    functionBranchStack.Add(currentBranch);
                                }
                                else
                                {
                                    throw new CompilerException(null, "Expected catch or finally block after try");
                                }
                                break;
                            case BranchEnum.Catch:
                                functionBranchStack.RemoveAt(functionBranchStack.Count - 1);
                                ilGen.Emit(OpCodes.Leave, currentBranch.EndLabel);
                                if (codeBuffer[PC].Type == RpnTokenType.StartFinallyBlock)
                                {
                                    PC++;
                                    ilGen.BeginFinallyBlock();

                                    currentBranch = new CompilationBranch()
                                    {
                                        Type = BranchEnum.Finally,
                                        EndLabel = currentBranch.EndLabel
                                    };
                                    functionBranchStack.Add(currentBranch);
                                }
                                else
                                {
                                    ilGen.EndExceptionBlock();
                                    ilGen.MarkLabel(currentBranch.EndLabel);
                                }
                                break;
                            case BranchEnum.Finally:
                                functionBranchStack.RemoveAt(functionBranchStack.Count - 1);
                                ilGen.Emit(OpCodes.Leave, currentBranch.EndLabel);
                                ilGen.EndExceptionBlock();
                                ilGen.MarkLabel(currentBranch.EndLabel);
                                break;
                            case BranchEnum.While:
                                ilGen.Emit(OpCodes.Br, currentBranch.StartLabel);
                                ilGen.MarkLabel(currentBranch.EndLabel);
                                functionBranchStack.RemoveAt(functionBranchStack.Count - 1);
                                break;
                            case BranchEnum.Foreach:
                                ilGen.Emit(OpCodes.Br, currentBranch.StartLabel);
                                ilGen.MarkLabel(currentBranch.EndLabel);
                                functionBranchStack.RemoveAt(functionBranchStack.Count - 1);
                                break;
                            case BranchEnum.Else:
                                ilGen.MarkLabel(currentBranch.EndLabel);
                                functionBranchStack.RemoveAt(functionBranchStack.Count - 1);
                                break;
                            case BranchEnum.ElseIf:
                            case BranchEnum.If:
                                Label endIfLabel = currentBranch.EndLabel;
                                Label elseLabel = currentBranch.StartLabel;
                                functionBranchStack.RemoveAt(functionBranchStack.Count - 1);
                                if (codeBuffer[PC].Type == RpnTokenType.Else)
                                {
                                    PC++;
                                    ilGen.Emit(OpCodes.Br, endIfLabel);
                                    ilGen.MarkLabel(elseLabel);
                                    currentBranch = new CompilationBranch()
                                    {
                                        Type = BranchEnum.Else,
                                        EndLabel = endIfLabel
                                    };
                                    functionBranchStack.Add(currentBranch);
                                }
                                else if (codeBuffer[PC].Type == RpnTokenType.ElseIf)
                                {
                                    PC++;
                                    ilGen.Emit(OpCodes.Br, endIfLabel);
                                    ilGen.MarkLabel(elseLabel);
                                    currentBranch = new CompilationBranch()
                                    {
                                        Type = BranchEnum.ElseIf,
                                        StartLabel = ilGen.DefineLabel(),
                                        EndLabel = endIfLabel
                                    };
                                    functionBranchStack.Add(currentBranch);
                                }
                                else
                                {
                                    ilGen.Emit(OpCodes.Br, endIfLabel);
                                    ilGen.MarkLabel(elseLabel);
                                    ilGen.MarkLabel(endIfLabel);
                                }

                                break;
                        }
                        break;
                }
            }
            ilGen.MarkLabel(returnLabel);
            return stackLocal;
        }
        #endregion

        internal static MethodInfo DelegateDynamicInvokeMethod = typeof(QFunction).GetMethod(nameof(QFunction.Invoke), new Type[] { typeof(FunctionArguments) });
        internal static Delegate EmitDelegate(Type delegateType, QFunction function)
        {
            MethodInfo method = delegateType.GetMethod("Invoke");
            ParameterInfo[] inParameters = method.GetParameters();
            Type[] inTypes = new Type[inParameters.Length + 1];
            inTypes[0] = typeof(QFunction);
            for (int i = 0; i < inParameters.Length; i++)
                inTypes[i + 1] = inParameters[i].ParameterType;
            DynamicMethod dymMethod = new DynamicMethod(
                "QSDelegate",
                method.ReturnType,
                inTypes,
                typeof(QFunction)
            );

            ILGenerator generator = dymMethod.GetILGenerator();

            generator.Emit(OpCodes.Ldarg_0);

             generator.Emit(OpCodes.Ldc_I4_S, inTypes.Length);
            generator.Emit(OpCodes.Newarr, typeof(object));
            for (int i = 0; i < inParameters.Length; i++)
            {
                generator.Emit(OpCodes.Dup);
                generator.Emit(OpCodes.Ldc_I4, i);
                generator.Emit(OpCodes.Ldarg, i + 1);
                if (!inParameters[i].ParameterType.IsByRef)
                    generator.Emit(OpCodes.Box, inParameters[i].ParameterType);
                generator.Emit(OpCodes.Stelem_Ref);
            }
            generator.Emit(OpCodes.Newobj, typeof(FunctionArguments).GetConstructor(new Type[] { typeof(object[]) }));
            generator.Emit(OpCodes.Callvirt, DelegateDynamicInvokeMethod);
            if (method.ReturnType == typeof(void))
                generator.Emit(OpCodes.Pop);
            generator.Emit(OpCodes.Ret);

            return dymMethod.CreateDelegate(delegateType, function);
        }
    }
}
