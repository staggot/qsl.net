﻿using System;
using System.Collections.Generic;
using System.Reflection;
using qsldotnet.lib;

namespace qsldotnet.tests
{
    /// <summary>
    /// Provides testbook to create and run tests
    /// Just inherit class TestBook and create methods in this class like so
    /// public void TestEngineEval(TestCase test)
    /// They must be public, void, name should start with 'test' and argument type should be TestCase
    /// </summary>
    public abstract class TestBook
    {
        public TestBook()
        {
            Init();
        }

        List<TestCase> tests = new List<TestCase>();
        protected void Init()
        {
            List<MethodInfo> testMethods = new List<MethodInfo>();
            foreach (MethodInfo method in this.GetType().GetMethods())
            {
                if (method.Name.ToUpper().StartsWith("TEST", StringComparison.InvariantCulture))
                    testMethods.Add(method);
            }
            foreach (MethodInfo testMethod in testMethods)
            {
                tests.Add(new TestCase(this, testMethod, 1000000, null));
            }
        }

        /// <summary>
        /// Run tests, log to stdout
        /// </summary>
        /// <param name="stdout">Do log to stdout</param>
        /// <param name="stderr">Do log to stderr</param>
        /// <param name="logFile">Do log to file</param>
        /// <returns>Failed tests count</returns>
        public int RunTests(bool stdout = true, bool stderr = false, string logFile = null)
        {
            int failedTests = 0;
            Logger logger = new Logger();
            if (stdout)
                logger.AddStdoutLogger("stdout", LogLevel.ALL);
            if (stderr)
                logger.AddStdoutLogger("stderr", LogLevel.ALL);
            if (logFile is {})
                logger.AddFileLogger("stderr", logFile, LogLevel.ALL);

            foreach (TestCase test in tests)
            {
                logger.Info(string.Format("Running: {0}", test.Name));
                try
                {
                    test.Start();
                }
                catch (Exception ex)
                {
                    failedTests++;
                    logger.Error(string.Format("Failed: {0}", test.Name), ex.InnerException);
                    continue;
                }
                finally
                {
                    if (test.DisposableResource is {})
                        test.DisposableResource.Dispose();
                }
                logger.Info(string.Format("Success: {0}", test.Name));
            }
            logger.Dispose(true);
            return failedTests;
        }

        /// <summary>
        /// Checks if expected and given values are equal, else throws UnexpectedTestResultException
        /// </summary>
        /// <param name="expected">Expected value</param>
        /// <param name="given">Give value</param>
        public static void AssertValue(object expected, object given)
        {
            if (expected is null && given is null)
                return;
            if (expected is null || given is null)
                throw new UnexpectedTestResultException(expected, given);

            dynamic a = given;
            dynamic b = expected;
            if (a == b)
                return;
            throw new UnexpectedTestResultException(expected, given);
        }
    }
}
