﻿using System;
namespace qsldotnet.tests
{
    public class TestTimeoutException : TestException
    {
        public TestTimeoutException(int expectedMilliseconds) : base(string.Format("Test ellapces more than {0}ms", expectedMilliseconds))
        {
        }
    }
}
