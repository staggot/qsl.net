﻿using System;

namespace qsldotnet.tests
{
    /// <summary>
    /// Throws on method CheckValue if values are not equal
    /// </summary>
    public class UnexpectedTestResultException : TestException
    {
        string msg;

        public UnexpectedTestResultException(object expected, object given)
        {
            if (expected is null)
                expected = "null";
            else
                expected = string.Format(
                    "[{0}] {1}",
                    expected.GetType(),
                    expected
                    );
            if (given is null)
                given = "null";
            else
                given = string.Format(
                    "[{0}] {1}",
                    given.GetType(),
                    given
                    );
            msg = string.Format("Expected: {0}, given: {1}", expected, given);
        }

        public override string Message
        {
            get { return msg; }
        }
    }
}
