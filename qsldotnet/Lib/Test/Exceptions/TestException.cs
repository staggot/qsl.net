﻿using System;
namespace qsldotnet.tests
{
    public abstract class TestException : Exception
    {
        public TestException() { }
        public TestException(string message) : base(message) { }
    }
}
