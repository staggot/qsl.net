﻿using System;
using System.Threading;
using System.Reflection;

namespace qsldotnet.tests
{
    public class TestCase
    {
        /// <summary>
        /// Thrown exception during test
        /// </summary>
        volatile internal Exception Error;

        /// <summary>
        /// Expected exception type during test
        /// </summary>
        public readonly Type ExpectedExceptionType;

        /// <summary>
        /// Test start time
        /// </summary>
        public DateTime StartTime { get; private set; }

        /// <summary>
        /// Test Finish time
        /// </summary>
        public DateTime FinishTime { get; private set; }

        /// <summary>
        /// Says that test is done
        /// </summary>
        public volatile bool IsFinished;

        /// <summary>
        /// Maximum time given for test execution in milliseconds
        /// </summary>
        public readonly int Timeout;

        /// <summary>
        /// Test is passed succesfully
        /// </summary>
        public bool Passed { get; private set; }

        /// <summary>
        /// Test name, equal to method name
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// This object will be disposed after test is done
        /// </summary>
        public IDisposable DisposableResource;



        internal Thread thread;
        TestBook testBook;
        internal MethodInfo testMethod;


        internal TestCase(TestBook book, MethodInfo testMethod, int timeoutMs, Type expectedExceptionType)
        {
            this.Name = testMethod.Name;
            this.testBook = book;
            this.testMethod = testMethod;
            this.Timeout = timeoutMs;
            this.ExpectedExceptionType = expectedExceptionType;
        }


        internal void Start()
        {
            Passed = false;
            thread = new Thread(new ThreadStart(Run));
            thread.Start();
            while (true)
            {
                Thread.Sleep(10);
                if (Error is {} || IsFinished)
                    break;
                int timeDiff = (int)(DateTime.Now - StartTime).TotalMilliseconds;
                if (timeDiff > Timeout)
                {
                    Error = new TestTimeoutException(Timeout);
                    break;
                }
            }
            if (!IsFinished)
                thread.Abort();
            if (ExpectedExceptionType is null && Error is null)
                return;
            if (ExpectedExceptionType is null && Error is { })
            {
                Passed = false;
                throw Error;
            }
            if (ExpectedExceptionType is { } && Error is null)
            {
                Passed = false;
                throw new UnexpectedTestResultException(ExpectedExceptionType, null);
            }
            if (ExpectedExceptionType is { } && Error is { })
            {
                if (!Error.GetType().IsSubclassOf(ExpectedExceptionType))
                {
                    Passed = false;
                    throw new UnexpectedTestResultException(ExpectedExceptionType, Error.GetType());
                }
            }
            Passed = true;
        }

        void Run()
        {
            IsFinished = false;
            StartTime = DateTime.Now;
            Error = null;
            try
            {
                testMethod.Invoke(testBook, new object[0]);
            }
            catch (Exception ex)
            {
                Error = ex.InnerException;
            }
            finally
            {
                FinishTime = DateTime.Now;
                IsFinished = true;
            }
        }
    }
}
