﻿using System;
using System.Threading;
using System.Collections.Concurrent;

namespace qsldotnet.lib
{
    public abstract class LoggerStream
    {
        public readonly LogLevel LogLevel;
        string[] queue;
        volatile int startQueue = 0;
        volatile int endQueue = 0;
        int maxQueueSize;
        Mutex mutex = new Mutex();

        protected bool CanDequeue
        {
            get { return startQueue != endQueue; }
        }

        internal void PushQueue(string line)
        {
            int newEnd = (endQueue + 1) % maxQueueSize;
            if (newEnd == startQueue)
                DequeueWrapper();

            queue[endQueue] = line;
            endQueue = newEnd;
        }

        protected string Pop()
        {
            if (CanDequeue)
            {
                int newStart = (startQueue + 1) % maxQueueSize;
                string result = queue[startQueue];
                startQueue = newStart;
                return result;
            }
            return null;
        }

        public LoggerStream(LogLevel logLevel = LogLevel.WARNING | LogLevel.CRITICAL | LogLevel.FATAL, int maxQueueSize = 1024)
        {
            this.maxQueueSize = maxQueueSize;
            queue = new string[maxQueueSize];
            this.LogLevel = logLevel;
        }

        internal void DequeueWrapper()
        {
            mutex.WaitOne();
            try
            {
                Dequeue();
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        protected abstract void Dequeue();
    }
}
