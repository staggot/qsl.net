﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace qsldotnet.lib
{
    /// <summary>
    /// Provides logging system
    /// </summary>
    public class Logger : IDisposable, IDictionary<string, LoggerStream>
    {
        Dictionary<string, LoggerStream> streams = new Dictionary<string, LoggerStream>();
        const string DefaultMessageFormat = "{time} {logger} {level} {message}";

        /// <summary>
        /// Log message format:
        /// {time} - DateTime format
        /// {logger} - logger name
        /// {level} - loglevel
        /// {message} - your log message
        /// </summary>
        public string MessageFormat = DefaultMessageFormat;

        /// <summary>
        /// Log message time format
        /// See: DateTime.Tostring(format)
        /// </summary>
        public string TimeFormat = "yyyy-MM-dd HH:mm:ss.fff";

        /// <summary>
        /// Timeout between queue writes
        /// </summary>
        public ushort PushTimeout = 100;

        /// <summary>
        /// Set this flag to true if you want to log write errors to stderr
        /// </summary>
        public bool IsLogWritesErrors = true;

        volatile Thread pushThread;
        volatile bool isAlive;
        volatile Semaphore disposeSemaphore;

        public Logger()
        {
            isAlive = true;
            pushThread = new Thread(new ThreadStart(PushLoop));
            pushThread.Start();
        }

        /// <summary>
        /// Log debug level message
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="logLevel">Log level override</param>
        public void Debug(string message, LogLevel logLevel = LogLevel.DEBUG)
        {
            this.Log(message, logLevel);
        }


        /// <summary>
        /// Log info level message
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="logLevel">Log level override</param>
        public void Info(string message, LogLevel logLevel = LogLevel.INFO)
        {
            this.Log(message, logLevel);
        }

        /// <summary>
        /// Log warning level message
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="logLevel">Log level override</param>
        public void Warning(string message, LogLevel logLevel = LogLevel.WARNING)
        {
            this.Log(message, logLevel);
        }


        /// <summary>
        /// Log error level message
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="logLevel">Log level override</param>
        public void Error(string message, LogLevel logLevel = LogLevel.ERROR)
        {
            this.Log(message, logLevel);
        }


        /// <summary>
        /// Log error level message
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="exception">Exception</param>
        /// <param name="logLevel">Log level override</param>
        public void Error(string message, Exception exception, LogLevel logLevel = LogLevel.ERROR)
        {
            this.Log(message, exception, logLevel);
        }

        /// <summary>
        /// Log critical level message
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="logLevel">Log level override</param>
        public void Criticat(string message, LogLevel logLevel = LogLevel.ERROR)
        {
            this.Log(message, logLevel);
        }

        /// <summary>
        /// Log fatal level message
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="logLevel">Log level override</param>
        public void Fatal(string message, LogLevel logLevel = LogLevel.FATAL)
        {
            this.Log(message, logLevel);
        }

        /// <summary>
        /// Logs message 
        /// </summary>
        /// <param name="logLevel">Message log level</param>
        /// <param name="message">Message</param>
        public void Log(string message, LogLevel logLevel = LogLevel.INFO)
        {
            if (!isAlive)
                return;
            if (IsDisposed)
                throw new Exception("Logger is disposed");
            IDictionary<string, object> values = new Dictionary<string, object>();
            values["time"] = DateTime.Now.ToString(TimeFormat);
            values["level"] = logLevel.ToString();
            values["message"] = message;
            
            foreach (var stream in streams)
            {
                values["logger"] = stream.Key;
                if ((logLevel & stream.Value.LogLevel) == 0)
                    continue;
                string line = MessageFormat.Format(values);
                stream.Value.PushQueue(line);
            }
        }

        /// <summary>
        /// Logs message 
        /// </summary>
        /// <param name="logLevel">Message log level</param>
        /// <param name="message">Message</param>
        /// <param name="exception">Additional exception to log</param>
        public void Log(string message, Exception exception, LogLevel logLevel = LogLevel.ERROR)
        {
            if (!isAlive)
                return;
            if (IsDisposed)
                throw new Exception("Logger is disposed");
            IDictionary<string, object> values = new Dictionary<string, object>();
            values["time"] = DateTime.Now.ToString(TimeFormat);
            values["level"] = logLevel.ToString();
            values["message"] = message;
            string exceptionMessage = (exception is null) ? "" : exception.ToInflatedString();
            foreach (var stream in streams)
            {
                values["logger"] = stream.Key;
                if ((logLevel & stream.Value.LogLevel) == 0)
                    continue;
                string line = MessageFormat.Format(values) + Environment.NewLine + exceptionMessage;
                stream.Value.PushQueue(line);
            }
        }


        /// <summary>
        /// Add new STDOUT logger stream
        /// </summary>
        /// <param name="name">LoggerStream name</param>
        /// <param name="logLevel">Message log level</param>
        /// <param name="queueSize">Stream queue size</param>
        public void AddStdoutLogger(string name, LogLevel logLevel, int queueSize = 1024)
        {
            streams.Add(name, new LoggerStdoutStream(logLevel, queueSize));
        }

        /// <summary>
        /// Add new STDERR logger stream
        /// </summary>
        /// <param name="name">LoggerStream name</param>
        /// <param name="logLevel">Message log level</param>
        /// <param name="queueSize">Stream queue size</param>
        public void AddStderrLogger(string name, LogLevel logLevel, int queueSize = 1024)
        {
            streams.Add(name, new LoggerStdoutStream(logLevel, queueSize));
        }

        /// <summary>
        /// Add new logger stream that writes to file
        /// </summary>
        /// <param name="name">LoggerStream name</param>
        /// <param name="name">Log file name</param>
        /// <param name="logLevel">Message log level</param>
        /// <param name="queueSize">Stream queue size</param>
        public void AddFileLogger(string name, string filePath, LogLevel logLevel, int queueSize = 1024)
        {
            streams.Add(name, new LoggerFileStream(filePath, logLevel, queueSize));
        }

        void PushLoop()
        {
            while (true)
            {
                foreach (var stream in streams)
                {
                    try
                    {
                        stream.Value.DequeueWrapper();
                    }
                    catch(Exception ex)
                    {
                        if (IsLogWritesErrors)
                        {
                            IDictionary<string, object> values = new Dictionary<string, object>();
                            values["time"] = DateTime.Now.ToString(TimeFormat);
                            values["level"] = LogLevel.CRITICAL;
                            values["logger"] = stream.Key;
                            values["message"] = "Can't write to logger stream";
                            string message = DefaultMessageFormat.Format(values) + "\n" + ex.ToInflatedString();
                            Console.Error.WriteLine(message);
                        }
                    }
                }
                if (!isAlive)
                    break;
                Thread.Sleep(PushTimeout);
            }
            if (disposeSemaphore is {})
                disposeSemaphore.Release();
            pushThread = null;
        }

        /// <summary>
        /// Is logger disposed and no longer working
        /// </summary>
        public bool IsDisposed
        {
            get { return pushThread is null; }
        }

        public ICollection<string> Keys => streams.Keys;

        public ICollection<LoggerStream> Values => streams.Values;

        public int Count => streams.Count;

        public bool IsReadOnly => false;

        /// <summary>
        /// Gets or sets logger stream
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public LoggerStream this[string name]
        {
            get { return streams[name]; }
            set { streams[name] = value; }
        }

        /// <summary>
        /// Stops logger thread
        /// </summary>
        public void Dispose()
        {
            Dispose(false);
        }


        /// <summary>
        /// Stops logger thread
        /// </summary>
        /// <param name="wait">Do wait for logger thread to stop</param>
        public void Dispose(bool wait)
        {
            if (wait)
                disposeSemaphore = new Semaphore(0, 1);
            isAlive = false;
            if (wait)
                disposeSemaphore.WaitOne();
        }

        /// <summary>
        /// Add logger stream
        /// </summary>
        /// <param name="name">Logger stream name</param>
        /// <param name="value">Logger stream</param>
        public void Add(string name, LoggerStream value)
        {
            streams.Add(name, value);
        }

        public bool ContainsKey(string key)
        {
            return streams.ContainsKey(key);
        }

        public bool Remove(string key)
        {
            return streams.Remove(key);
        }

        public bool TryGetValue(string key, out LoggerStream value)
        {
            return streams.TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<string, LoggerStream> item)
        {
            streams.Add(item.Key, item.Value);
        }

        public void Clear()
        {
            streams.Clear();
        }

        public bool Contains(KeyValuePair<string, LoggerStream> item)
        {
            return streams.ContainsKey(item.Key);
        }

        public void CopyTo(KeyValuePair<string, LoggerStream>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<string, LoggerStream> item)
        {
            return streams.Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<string, LoggerStream>> GetEnumerator()
        {
            return streams.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return streams.GetEnumerator();
        }
    }
}
