﻿using System;
using System.IO;

namespace qsldotnet.lib
{
    public class LoggerFileStream : LoggerStream
    {
        /// <summary>
        /// Logger output file
        /// </summary>
        public readonly FileInfo File;

        protected void CreateDirectory()
        {
            if (!File.Directory.Exists)
                CreateDictionary(File.Directory);
        }

        protected void CreateDictionary(DirectoryInfo directory)
        {
            if (!directory.Parent.Exists)
                CreateDictionary(directory.Parent);
            directory.Create();
        }

        /// <summary>
        /// Creates LoggerStream that writes into a file
        /// </summary>
        /// <param name="filename">Filename</param>
        public LoggerFileStream(string filename, LogLevel loglevel, int queueSize = 1024) : base(loglevel, queueSize)
        {
            File = new FileInfo(filename);
            CreateDirectory();
            if (!File.Exists)
                File.Create().Close();
        }

        protected override void Dequeue()
        {
            CreateDirectory();
            using (StreamWriter writer = new StreamWriter(File.FullName, true))
            {
                while (CanDequeue)
                {
                    writer.WriteLine(Pop());
                }
            }
        }
    }
}
