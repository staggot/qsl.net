﻿using System;
using System.IO;

namespace qsldotnet.lib
{
	public class LoggerStdoutStream : LoggerStream
	{
		/// <summary>
		/// Creates LoggerStream that writes into stdout
		/// </summary>
		public LoggerStdoutStream(LogLevel loglevel, int queueSize = 1024) : base(loglevel, queueSize)
		{
		}

		protected override void Dequeue()
		{
			while (CanDequeue)
			{
				Console.Out.WriteLine(Pop());
			}
		}
	}
}