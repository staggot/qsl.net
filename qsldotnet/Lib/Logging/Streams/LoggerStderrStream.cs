﻿using System;
using System.IO;

namespace qsldotnet.lib
{
    public class LoggerStderrStream : LoggerStream
    {
        /// <summary>
        /// Creates LoggerStream that writes into stderr
        /// </summary>
        public LoggerStderrStream(LogLevel loglevel, int queueSize = 1024) : base(loglevel, queueSize)
        {
        }

        protected override void Dequeue()
        {
            while (CanDequeue)
            {
                Console.Error.WriteLine(Pop());
            }
        }
    }
}
