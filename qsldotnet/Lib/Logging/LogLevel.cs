﻿using System;
namespace qsldotnet.lib
{
    [Flags]
    public enum LogLevel : byte
    {
        NONE = 0,
        DEBUG = 0x01,
        INFO = 0x02,
        WARNING = 0x04,
        ERROR = 0x08,
        CRITICAL = 0x10,
        FATAL = 0x20,
        ALL = 0x3F
    }
}
