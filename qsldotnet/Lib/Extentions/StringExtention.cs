﻿using System;
using System.Text;
using System.Collections.Generic;

namespace qsldotnet.lib
{
    public static class StringExtention
    {
        public static string Format(this string str, IDictionary<string, object> kv)
        {
            StringBuilder sb = new StringBuilder(str.Length);
            StringBuilder name = new StringBuilder();
            bool readingName = false;
            char prev = '\0';
            for (int i = 0; i < str.Length; i++)
            {
                char c = str[i];

                if (readingName)
                {
                    if (c == '}' && prev == '}')
                    {
                        prev = '\0';
                        name.Append('}');
                        continue;
                    }
                    if (c == '}' && prev == '{')
                    {
                        throw new Exception("Bad Format: " + str);
                    }
                    if (c == '}')
                    {
                        prev = c;
                        continue;
                    }
                    if (c == '{' && prev == '{')
                    {
                        prev = '\0';
                        name.Append('{');
                        continue;
                    }
                    if (c == '{' && prev == '}')
                    {
                        sb.Append(kv[name.ToString()]);
                        readingName = false;
                        prev = c;
                        continue;
                    }
                    if (c == '{')
                    {
                        prev = c;
                        continue;
                    }
                    if (prev == '{')
                    {
                        throw new Exception("Bad Format: " + str);
                    }
                    if (prev == '}')
                    {
                        sb.Append(kv[name.ToString()]);
                        readingName = false;
                        sb.Append(c);
                        prev = '\0';
                        continue;
                    }
                    name.Append(c);
                    prev = '\0';
                }
                else
                {
                    if (c == '}' && prev == '}')
                    {
                        prev = '\0';
                        sb.Append('}');
                        continue;
                    }
                    if (c == '}' && prev == '{')
                    {
                        throw new Exception("Bad Format: " + str);
                    }
                    if (c == '}')
                    {
                        prev = c;
                        continue;
                    }
                    if (c == '{' && prev == '{')
                    {
                        prev = '\0';
                        sb.Append('{');
                        continue;
                    }
                    if (c == '{' && prev == '}')
                    {
                        sb.Append(kv[name.ToString()]);
                        readingName = false;
                        prev = '{';
                        continue;
                    }
                    if (c == '{')
                    {
                        prev = c;
                        continue;
                    }
                    if (prev == '{')
                    {
                        name.Clear();
                        name.Append(c);
                        readingName = true;
                        prev = '\0';
                        continue;

                    }
                    if (prev == '}')
                    {
                        throw new Exception("Bad Format: " + str);
                    }
                    sb.Append(c);
                    prev = '\0';
                }
            }
            if (readingName && prev == '}')
            {
                sb.Append(kv[name.ToString()]);
            }
            else if (prev == '}' || prev == '{')
                throw new Exception("Bad Format: " + str);
            return sb.ToString();
        }

    }
}
