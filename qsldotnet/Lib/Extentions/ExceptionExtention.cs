﻿using System;
using System.Text;

namespace qsldotnet.lib
{
    public static class ExteptionExtention
    {
        public static string ToInflatedString(this Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception is {})
            {
                stringBuilder.Append("[");
                stringBuilder.Append(exception.GetType().FullName);
                stringBuilder.Append("]:");
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);
                exception = exception.InnerException;
            }
            return stringBuilder.ToString();
        }
    }
}
