﻿using System;
using System.IO;

namespace qsldotnet.lib
{
    public static class QIO
    {
        public static void PipeStream(
            Stream inputStream,
            Stream outputStream,
            int bufferSize = 1024,
            long? inputFrom = null,
            long? bytesCount = null,
            long? outputFrom = null)
        {
            byte[] buffer = new byte[bufferSize];
            if (inputFrom is {})
                inputStream.Seek(inputFrom.Value, SeekOrigin.Begin);
            if (outputFrom is {})
                outputStream.Seek(outputFrom.Value, SeekOrigin.Begin);

            long read = 0;
            while (true)
            {
                if (bytesCount is {} && read == bytesCount.Value)
                    break;
                int expectedRead = bufferSize;
                if (bytesCount is {} && (bytesCount.Value - read < expectedRead))
                    expectedRead = (int)(bytesCount.Value - read);
                int count = inputStream.Read(buffer, 0, expectedRead);
                read += count;
                if (count == 0)
                    break;
                outputStream.Write(buffer, 0, count);
            }
            outputStream.Flush();
        }
    }
}
