﻿using System;

namespace qsldotnet.lib
{
    public enum ArgType
    {
        String = 1,
		Int = 2,
		Double = 3
    }
}
