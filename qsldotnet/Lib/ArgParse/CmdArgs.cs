﻿using System;
using System.Collections.Generic;
using qsldotnet.core;

namespace qsldotnet.lib
{
    public class CmdArgs
    {
        Dictionary<string, object> values = new Dictionary<string, object>();
        List<ArgInfo> args = new List<ArgInfo>();
        ArgParser parser;

        internal CmdArgs(ArgParser rootParser, string[] argv)
        {
            try
            {
                Parse(rootParser, argv);
            }
            catch (ArgumentParseException parsingException)
            {
                PrintHelp(parsingException.Parser);
                throw parsingException;
            }
        }

        void Parse(ArgParser rootParser, string[] argv)
        {
            parser = rootParser;
            foreach (ArgInfo arg in parser.arguments.Values)
                args.Add(arg);
            bool dashdash = false;
            int positionalIndex = 0;

            int index = 0;
            while (index < argv.Length)
            {
                string word = argv[index];
                if (!dashdash && word == "--")
                    dashdash = true;
                else if (parser.subParsers.ContainsKey(word))
                {
                    parser = parser.subParsers[word];
                    positionalIndex = 0;
                    foreach (ArgInfo arg in parser.arguments.Values)
                        args.Add(arg);
                }
                else if (word.StartsWith("--") && !dashdash)
                {
                    if (parser.arguments.ContainsKey(word))
                        ProcessArg(parser, parser.arguments[word], argv, ref index);
                    else
                        throw new ArgumentParseException(parser, string.Format("Unknown Argument {0}", word));

                }
                else if (word.StartsWith("-") && !dashdash)
                {
                    string line = word.TrimStart('-');
                    for (int i = 0; i < line.Length; i++)
                    {
                        char c = line[i];
                        if (!parser.shortArguments.ContainsKey(c))
                            throw new ArgumentParseException(parser, string.Format("Unknown Argument -{0}", c));
                        ProcessArg(parser, parser.shortArguments[c], argv, ref index);
                    }
                }
                else
                {
                    if (positionalIndex >= parser.positional.Count)
                        throw new ArgumentParseException(parser, string.Format("Unknown Argument -{0}", word));
                    ArgInfo pArg = parser.arguments[parser.positional[positionalIndex]];
                    if (!values.ContainsKey(pArg.Destination))
                        values[pArg.Destination] = new List<string>();
                    List<string> currentCollection = values[pArg.Destination] as List<string>;
                    currentCollection.Add(word);
                    if (pArg.ArgumentsNumber is {})
                    {
                        if (currentCollection.Count >= pArg.ArgumentsNumber)
                            positionalIndex++;
                    }
                }
                index++;
            }

            foreach (ArgInfo arg in args)
            {
                if (!values.ContainsKey(arg.Destination))
                {
                    values[arg.Destination] = arg.Default;
                    continue;
                }
                switch (arg.Action)
                {
                    case ArgAction.Count:
                        if (!values[arg.Destination].IsNumber())
                            throw new ArgParserException(parser, string.Format("Wrong argument type: {0}", arg.Name));
                        values[arg.Destination] = (int)values[arg.Destination];
                        break;
                    case ArgAction.StoreAppend:
                        List<string> appendList = values[arg.Destination] as List<string>;
                        switch (arg.Type)
                        {
                            case ArgType.Double:
                                double[] doubleResult = new double[appendList.Count];
                                for (int i = 0; i < doubleResult.Length; i++)
                                    if (!double.TryParse(appendList[i], out doubleResult[i]))
                                        throw new ArgumentParseException(parser, string.Format("Expected type double for argument {0}", arg.Name));
                                values[arg.Destination] = doubleResult;
                                break;
                            case ArgType.Int:
                                int[] intResult = new int[appendList.Count];
                                for (int i = 0; i < intResult.Length; i++)
                                    if (!int.TryParse(appendList[i], out intResult[i]))
                                        throw new ArgumentParseException(parser, string.Format("Expected type int for argument {0}", arg.Name));
                                values[arg.Destination] = intResult;
                                break;
                            case ArgType.String:
                                string[] stringResult = new string[appendList.Count];
                                for (int i = 0; i < stringResult.Length; i++)
                                    stringResult[i] = appendList[i];
                                values[arg.Destination] = stringResult;
                                break;
                        }
                        break;
                    case ArgAction.StoreValue:
                        switch (arg.Type)
                        {
                            case ArgType.Double:
                                double doubleResult;
                                if (!double.TryParse((string)values[arg.Destination], out doubleResult))
                                    throw new ArgumentParseException(parser, string.Format("Expected type double for argument {0}", arg.Name));
                                values[arg.Destination] = doubleResult;
                                break;
                            case ArgType.Int:
                                int intResult;
                                if (!int.TryParse((string)values[arg.Destination], out intResult))
                                    throw new ArgumentParseException(parser, string.Format("Expected type int for argument {0}", arg.Name));
                                values[arg.Destination] = intResult;
                                break;
                        }
                        break;
                }
            }
        }

        internal object this[string name]
        {
            get { return values[name]; }    
        }

        internal IEnumerable<string> Keys
        {
            get { return values.Keys; }
        }

        private void ProcessArg(ArgParser parser, ArgInfo arg, string[] argv, ref int index)
        {
            switch (arg.Action)
            {
                case ArgAction.Count:
                    if (!values.ContainsKey(arg.Destination))
                        values[arg.Destination] = 0;
                    values[arg.Destination] = (int)values[arg.Destination] + 1;
                    break;
                case ArgAction.StoreBool:
                    if (values.ContainsKey(arg.Destination))
                        throw new ArgumentParseException(parser, string.Format("Unexpected dublicate argument {0}", arg.Name));
                    values[arg.Destination] = true;
                    break;
                case ArgAction.StoreAppend:
                    if (!values.ContainsKey(arg.Destination))
                        values[arg.Destination] = new List<string>();
                    index++;
                    if (index >= argv.Length)
                        throw new ArgumentParseException(parser, string.Format("Expected value for argument {0}", arg.Name));
                    (values[arg.Destination] as List<string>).Add(argv[index]);
                    break;
                case ArgAction.StoreValue:
                    if (values.ContainsKey(arg.Destination))
                        throw new ArgumentParseException(parser, string.Format("Unexpected dublicate argument {0}", arg.Name));
                    index++;
                    if (index >= argv.Length)
                        throw new ArgumentParseException(parser, string.Format("Expected value for argument {0}", arg.Name));
                    values[arg.Destination] = argv[index];
                    break;
            }
        }

        void PrintHelp()
        {
            PrintHelp(parser);
        }

        static void PrintHelp(ArgParser parser)
        {
            Console.WriteLine(parser.Help);
        }


        /// <summary>
        /// Execute provided function for resulting subparser
        /// </summary>
        public void Execute()
        {
            if (values.ContainsKey("help") && values["help"] is bool && (bool)values["help"])
                PrintHelp();
            else
                parser.Function?.Invoke(this);
        }
    }
}
