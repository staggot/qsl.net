﻿using System;
namespace qsldotnet.lib
{
    /// <summary>
    /// Argument handling action type
    /// </summary>
    public enum ArgAction
    {
		StoreValue,
        StoreAppend,
		StoreBool,
        Count
	}
}
