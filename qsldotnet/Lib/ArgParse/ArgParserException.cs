﻿using System;
namespace qsldotnet.lib
{
    public class ArgParserException : Exception
    {
        internal readonly ArgParser Parser;

        internal ArgParserException(ArgParser parser, string message) : base(message)
        {
            Parser = parser;
        }

        public override string Message
        {
            get { return Message; }
        }
    }
}
