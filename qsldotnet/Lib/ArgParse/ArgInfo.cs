﻿using System;

namespace qsldotnet.lib
{
	internal class ArgInfo
	{
		internal string Name;
		internal char ShortName;
		internal ArgType Type;
		internal ArgAction Action;
		internal string Help;
		internal string Destination;
		internal bool IsPositional;
		internal object Default;
		internal int? ArgumentsNumber;

		internal ArgInfo()
        {

        }
    }
}
