﻿using System;
namespace qsldotnet.lib
{
    public class ArgumentParseException : Exception
    {
		internal readonly ArgParser Parser;

        internal ArgumentParseException(ArgParser parser, string message) : base(message)
        {
			Parser = parser;
        }

        public override string Message
		{
			get
			{ return Message; }
		}
	}
}
