﻿using System;
using System.Text;
using System.Collections.Generic;

namespace qsldotnet.lib
{
    /// <summary>
    /// Provides Python like command line argument parser
    /// </summary>
    public class ArgParser
    {
		internal Dictionary<string, ArgInfo> arguments = new Dictionary<string, ArgInfo>();
		internal Dictionary<char, ArgInfo> shortArguments = new Dictionary<char, ArgInfo>();
		internal Dictionary<string, ArgParser> subParsers = new Dictionary<string, ArgParser>();
		internal Dictionary<string, ArgInfo> destinations = new Dictionary<string, ArgInfo>();
		internal List<string> positional = new List<string>();
        internal string help;

        
		internal ArgParser parent;

        /// <summary>
        /// Parser name
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Function to execute if arguments matches current parser
        /// </summary>
		public Action<CmdArgs> Function;

        /// <summary>
        /// Creates argument parser
        /// </summary>
        public ArgParser() : this(AppDomain.CurrentDomain.FriendlyName)
        {
        }

        /// <summary>
        /// Creates argument parser
        /// </summary>
        /// <param name="name">Application name</param>
        public ArgParser(string name, string help = null)
        {
            this.Name = name;
            this.help = help;
            AddArgument("--help", 'h', ArgAction.StoreBool, destination: "help", help: "Show this help");
        }

        /// <summary>
        /// Creates argument subparser
        /// </summary>
        /// <param name="name">Subparser name</param>
        /// <param name="help">Subparser help</param>
        /// <returns></returns>
        public ArgParser AddSupbarser(string name, string help = null)
        {
            ArgParser sub = new ArgParser(name, help);
            sub.parent = this;
            if (subParsers.ContainsKey(name))
                throw new ArgParserException(this, string.Format("Dublicate subparser name '{0}'", name));
            subParsers[name] = sub;
            return sub;
        }

		internal void AddArgument(
			string name,
			char shortName = '\0',
			ArgAction action = ArgAction.StoreValue,
			ArgType type = ArgType.String,
			int? argumentsNumber = 1,
			string destination = null,
			string help = null,
			object defaultValue = null
			)
		{
			bool dash;
			if (name.StartsWith("--"))
				dash = true;
			else if (name.StartsWith("-"))
				throw new Exception(string.Format("Invalid argument name {0}", name));
			else
				dash = false;

			if (destination is null)
			{
				destination = name.TrimStart('-');
			}

			if (help is null)
				help = "";

            switch(action)
            {
                case ArgAction.Count:
                    type = ArgType.Int;
                    if (defaultValue is null)
                        defaultValue = 0;
                    break;
                case ArgAction.StoreAppend:
                    if (defaultValue is {})
                        break;
                    switch(type)
                    {
                        case ArgType.Double:
                            defaultValue = new double[0]; 
                            break;
                        case ArgType.Int:
                            defaultValue = new int[0];
                            break;
                        case ArgType.String:
                            defaultValue = new string[0];
                            break;
                    }
                    break;
                case ArgAction.StoreBool:
                    if (!(defaultValue is bool))
                        defaultValue = false;
                    break;
                case ArgAction.StoreValue:
                    break;
            }

			ArgInfo arg = new ArgInfo()
			{
				Name = name,
				ShortName = shortName,
				ArgumentsNumber = argumentsNumber,
				Type = type,
				Action = action,
				Default = defaultValue,
				Destination = destination,
                IsPositional = !dash,
				Help = help
			};
			if (arg.IsPositional)
				positional.Add(arg.Name);
            if (arguments.ContainsKey(arg.Name))
                throw new ArgParserException(this, string.Format("Dublicate argument name '{0}'", arg.Name));
            if(destinations.ContainsKey(arg.Destination))
                throw new ArgParserException(this, string.Format("Dublicate argument destination '{0}'", arg.Destination));
            if (arg.ShortName != '\0' && shortArguments.ContainsKey(arg.ShortName))
                throw new ArgParserException(this, string.Format("Dublicate argument short name '{0}'", shortName));

            arguments[arg.Name] = arg;
			if (arg.ShortName != '\0')
				shortArguments[arg.ShortName] = arg;
			destinations[arg.Destination] = arg;
		}

        internal string Help
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                Stack<string> subpath = new Stack<string>();
                ArgParser parser = this;

                while(parser!=null)
                {
                    subpath.Push(parser.Name);
                    parser = parser.parent;
                }

                sb.Append(string.Format("usage:"));
                while (subpath.Count > 0)
                    sb.Append(" " + subpath.Pop());

                StringBuilder sbSub = new StringBuilder();
                sbSub.Append(Environment.NewLine + Environment.NewLine + "Subcommands:" + Environment.NewLine);
                foreach(var sub in subParsers)
                    sbSub.Append(string.Format("\t{0}\t{1}" + Environment.NewLine, sub.Key, sub.Value.help is null ? "" : sub.Value.help));

                StringBuilder sbOptArgs = new StringBuilder();
                sbOptArgs.Append(Environment.NewLine + "Optional arguments:" + Environment.NewLine);
                foreach (var arg in arguments)
                {
                    if (!arg.Value.IsPositional)
                    {
                        if(arg.Value.Action != ArgAction.StoreBool && arg.Value.Action != ArgAction.Count)
                        {
                            if(arg.Value.ShortName != '\0')
                            {
                                sbOptArgs.Append(string.Format("\t-{0} {1}, {2} {1}\t{3}" + Environment.NewLine,
                                    arg.Value.ShortName,
                                    arg.Value.Destination,
                                    arg.Value.Name,
                                    arg.Value.Help is null ? "" : arg.Value.Help
                                    ));
                                sb.Append(string.Format(" [-{0} {1}] [{2} {1}]",
                                    arg.Value.ShortName,
                                    arg.Value.Destination,
                                    arg.Value.Name
                                    ));
                            }
                            else
                            {
                                sbOptArgs.Append(string.Format("\t{0} {1}\t{2}" + Environment.NewLine,
                                    arg.Value.Name,
                                    arg.Value.Destination,
                                    arg.Value.Help is null ? "" : arg.Value.Help
                                    ));
                                sb.Append(string.Format(" [{0} {1}]",
                                    arg.Value.Name,
                                    arg.Value.Destination
                                    ));
                            }
                        }
                        else
                        {
                            if (arg.Value.ShortName != '\0')
                            {
                                sbOptArgs.Append(string.Format("\t-{0}, {1}\t{2}" + Environment.NewLine,
                                    arg.Value.ShortName,
                                    arg.Value.Name,
                                    arg.Value.Help is null ? "" : arg.Value.Help
                                    ));
                                sb.Append(string.Format(" [-{0}] [{1}]",
                                    arg.Value.ShortName,
                                    arg.Value.Name
                                    ));
                            }
                            else
                            {
                                sbOptArgs.Append(string.Format("\t{0} {1}" + Environment.NewLine,
                                    arg.Value.Name,
                                    arg.Value.Help is null ? "" : arg.Value.Help
                                    ));
                                sb.Append(string.Format(" [{0}]",
                                    arg.Value.Name
                                    ));
                            }
                        }
                    }
                }

                StringBuilder sbPosArgs = new StringBuilder();
                sbPosArgs.Append(Environment.NewLine + "Positional arguments:" + Environment.NewLine);
                foreach(var arg in arguments)
                {
                    if (arg.Value.IsPositional)
                    {
                        sbPosArgs.Append(string.Format("\t{0}\t{1}" + Environment.NewLine, arg.Key, arg.Value.Help));
                        if (arg.Value.ArgumentsNumber is null)
                            sb.Append(string.Format(" [{0} [{0} ...]]", arg.Key));
                        else
                            for (int i = 0; i < arg.Value.ArgumentsNumber; i++)
                                sb.Append(string.Format(" {0}", arg.Key));
                    }
                }

                if (help is {})
                    sb.Append(string.Format("{0}" + Environment.NewLine, help));
                sb.Append(sbSub);
                sb.Append(sbPosArgs);
                sb.Append(sbOptArgs);
                return sb.ToString();
            }
        }

		internal CmdArgs ParseArguments(string[] argv)
		{
            return new CmdArgs(this, argv);
		}
    }
}
