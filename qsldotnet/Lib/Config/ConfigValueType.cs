﻿using System;
namespace qsldotnet.lib
{
    public class ConfigValueType
    {
        public readonly Type Type;
        public readonly ConfigValueParser Parser;
        public readonly ConfigValueWriter Writer;

        internal ConfigValueType(Type type, ConfigValueParser parser, ConfigValueWriter writer)
        {
            Type = type;
            Parser = parser;
            Writer = writer;
        }

        public bool TryParse(string text, out object value)
        {
            if(TryParse(text, out value))
            {
                if (value is null || Type is null)
                    return true;
                if (Type is {} && value is null && !Type.IsByRef)
                    return false;
                if (Type is null || value is null)
                    return true;
                if (Type is {} && Type.IsAssignableFrom(value.GetType()))
                    return true;
                throw new ConfigurationException("Invalid type");
            }
            return false;
        }

        public string Write(object value)
        {
            return Writer(value);
        }
    }
}
