﻿using qsldotnet;

namespace qsldotnet.lib
{
    public partial class Configuration : IgnoreCaseMap<ConfigSection>
    {
        void InitDefaultTypes()
        {
            RegisterType(
                typeof(long),
                new ConfigValueParser((string text, out object value) =>
                {
                    long num;
                    bool res = long.TryParse(text, out num);
                    value = num;
                    return res;
                }),
                new ConfigValueWriter((value) =>
                {
                    return value.ToString();
                }));
            RegisterType(
                typeof(string),
                new ConfigValueParser((string text, out object value) =>
                {
                    long num;
                    bool res = long.TryParse(text, out num);
                    value = num;
                    return res;
                }),
                new ConfigValueWriter((value) =>
                {
                    return value.ToString();
                }));
        }
    }
}
