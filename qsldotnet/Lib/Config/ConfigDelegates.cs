﻿using System;

namespace qsldotnet.lib
{
    public delegate bool ConfigValueParser(string input, out object value);
    public delegate string ConfigValueWriter(object value);
}