﻿using System;
namespace qsldotnet.lib
{
    public class ConfigurationException : Exception
    {
        public ConfigurationException(string message) : base(message)
        {

        }
    }
}
