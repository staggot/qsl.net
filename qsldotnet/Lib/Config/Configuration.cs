﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using qsldotnet;

namespace qsldotnet.lib
{
    public partial class Configuration : IgnoreCaseMap<ConfigSection>
    {
        static Regex sectionRegex = new Regex(
            @"^\s*\[([a-zA-Z0-9_.]+)\]\s*$",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);


        readonly Dictionary<string, ConfigSection> sections = new Dictionary<string, ConfigSection>();
        readonly Dictionary<Type, ConfigValueType> valueTypes = new Dictionary<Type, ConfigValueType>(); 
        readonly bool isIgnoreCase = true;

        public override bool IsIgnoreCase => isIgnoreCase;

        Configuration(CharReader reader, bool ignoreCase = true)
        {
            this.isIgnoreCase = ignoreCase;
            ReadConfig(reader);
        }

        void ReadConfig(CharReader reader)
        {

        }

        public void RegisterType(Type type, ConfigValueParser parser, ConfigValueWriter writer)
        {

        }

        /// <summary>
        /// Gets configuration section
        /// </summary>
        /// <param name="key">Section name</param>
        /// <returns></returns>
        public override ConfigSection this[string key]
        {
            get
            {
                return base[key];
            }
            set
            {
                if (ContainsKey(key))
                    throw new Exception("Dublicate section");
                base[key] = value;
            }
        }

        /// <summary>
        /// Gets or Sets value from configuration
        /// </summary>
        /// <param name="section">Section name</param>
        /// <param name="field">Field name</param>
        /// <returns></returns>
        public object this[string section, string field]
        {
            get
            {
                return this[section][field];
            }
            set
            {
                this[section][field] = value;
            }
        }
    }
}
