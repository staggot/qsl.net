﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace qsldotnet.lib
{
    public class ConfigSection : IgnoreCaseMap<object>
    {
        Configuration config;

        internal ConfigSection(Configuration config)
        {
            this.config = config;
        }

        Dictionary<string, ConfigField> fields = new Dictionary<string, ConfigField>();

        public override bool IsIgnoreCase => config.IsIgnoreCase;

        public void AddField(string name, ConfigField field)
        {
            if (fields.ContainsKey(name))
                throw new ConfigurationException("Dublicate field in section");
            fields[name] = field;
            this[name] = field.DefaultValue;
        }

        public override bool Remove(string key)
        {
            key = GetKeyAlias(key);
            fields.Remove(key);
            return base.Remove(key);
        }
    }
}
