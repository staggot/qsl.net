﻿using System;
namespace qsldotnet.lib
{
    public class ConfigField
    {
        public readonly Type FieldType;
        public readonly object DefaultValue;
        public ConfigField(Type type, object defaultValue = null)
        {
            FieldType = type;
            DefaultValue = MatchType(defaultValue);
        }

        public object MatchType(object value)
        {
            if (value is {} && !FieldType.IsAssignableFrom(value.GetType()))
                value = Convert.ChangeType(value, FieldType);
            return value;
        }
    }
}
