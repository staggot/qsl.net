﻿using System.Collections.Generic;
using System.IO;
namespace qsldotnet
{
    internal class QsTemplateBank
    {
        Dictionary<string, QsTemplate> templates = new Dictionary<string, QsTemplate>();

        internal QsTemplateBank(QEngine engine,string directory, string pattern = "*.*") : this(engine, new DirectoryInfo(directory),pattern)
        {
        }

        internal QsTemplateBank(QEngine engine, DirectoryInfo directiory, string pattern = "*.*")
        {
            foreach (FileInfo fi in directiory.GetFiles(pattern))
            {
                QsTemplate template = new QsTemplate(engine, fi);
                templates[template.Name] = template;
            }
        }

        internal QFunction this[string name]
        {
            get
            {
                return templates[name].Format;
            }
        }
                
    }
}
