﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;

namespace qsldotnet
{
    internal class QsTemplate
    {
        const char Separator = '%';

        QEngine engine;
        string compiledPattern = "";
        int currentIndex = 0;
        QFunction[] delegates;

        internal string Name { get; private set; }
        internal QFunction Format { get; private set; }

        internal QsTemplate(QEngine engine, StreamReader stream, string name = null)
        {
            this.engine = engine;
            Name = name;
            Init();
            Read(new StreamCharReader(stream));
        }

        internal QsTemplate(QEngine engine, string template, string name = null)
        {
            Name = name;
            this.engine = engine;
            Init();
            Read(new StringCharReader(template));
        }
        internal QsTemplate(QEngine engine, FileInfo file)
        {
            Name = file.Name.Substring(0, file.Name.Length - file.Extension.Length);
            this.engine = engine;
            using (StreamReader fs = new StreamReader(file.FullName))
            {
                CharReader cr = new StreamCharReader(fs);
                Init();
                Read(cr);
            }
        }



        object FormatFunction(FunctionArguments args)
        {
            object[] results = new string[currentIndex];
            for (int i = 0; i < currentIndex; i++)
            {
                object result = null;
                try
                {
                    result = delegates[i](args).ToString();
                }
                catch (Exception ex)
                {
                    result = ex.Message;
                }
                if (result is null)
                    result = "null";
                else
                    result = result.ToString();
                results[i] = result;
            }
            return string.Format(compiledPattern, results);
        }

        void Init()
        {
            Format = new QFunction(FormatFunction);
        }


        void Read(CharReader stream)
        {
            List<QFunction> delegList = new List<QFunction>();
            StringBuilder sb = new StringBuilder();
            StringBuilder currentScript = new StringBuilder();
            bool wasSeparator = false;
            bool readReadingScript = false;
            while (!stream.Done)
            {
                char c = stream.ReadChar();
                if (readReadingScript)
                {
                    if (c == Separator && wasSeparator)
                    {
                        currentScript.Append('%');
                        wasSeparator = false;
                    }
                    else if (c == Separator && !wasSeparator)
                    {
                        wasSeparator = true;
                    }
                    else if (c != Separator && wasSeparator)
                    {
                        delegList.Add(engine.Compile(currentScript.ToString()));
                        currentScript.Clear();
                        readReadingScript = false;
                        wasSeparator = false;
                    }
                    else
                    {
                        currentScript.Append(c);
                        wasSeparator = false;
                    }
                }
                else
                {
                    if (c == Separator && wasSeparator)
                    {
                        sb.Append(Separator);
                        wasSeparator = false;
                    }
                    else if (c == Separator && !wasSeparator)
                    {
                        wasSeparator = true;
                    }
                    else if (c != Separator && wasSeparator)
                    {
                        sb.Append("{" + currentIndex + "}");
                        currentIndex++;
                        readReadingScript = true;
                        currentScript.Append(c);
                        wasSeparator = false;
                    }
                    else
                    {
                        sb.Append(c);
                        wasSeparator = false;
                    }
                }
            }

            delegates = delegList.ToArray();
            compiledPattern = sb.ToString();
        }
    }
}
