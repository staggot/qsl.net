﻿using System;
using System.IO;

namespace qsldotnet
{
    public class TextTemplate
    {
        public TextTemplate(string source)
        {
            Read(new StringCharReader(source));
        }

        public TextTemplate(StreamReader stream)
        {
            Read(new StreamCharReader(stream));
        }

        void Read(CharReader reader)
        {
            while (true)
            {
                char c = reader.ReadChar();
                if (c == '\0')
                    break;
            }
            //reader.ReadChar();
        }

        public string Render()
        {
            return null;
        }
    }
}
