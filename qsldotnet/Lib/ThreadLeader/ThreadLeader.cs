﻿using System;
using System.Threading;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Collections.Generic;

namespace qsldotnet.lib
{
    /// <summary>
    /// Provides simple thread pool and job queue
    /// </summary>
    public class ThreadLeader : IDisposable
    {
        const int DefaultMaxThread = 32;
        const int WatchdogTickTime = 10;
        const int DefaultMaxQueueProcessingTime = 10;

        volatile Thread watchdogThread;
        Thread[] pool;
        ConcurrentStack<int> freeIndexes = new ConcurrentStack<int>();
        ConcurrentQueue<Thread> threadQueue = new ConcurrentQueue<Thread>();
        ConcurrentQueue<ThreadLeaderJob> tasks = new ConcurrentQueue<ThreadLeaderJob>();
        private volatile bool isAlive;
        private volatile bool isPaused;


        /// <summary>
        /// Gets bool value is thread leader working
        /// </summary>
        public bool IsAlive { get => isAlive; private set => isAlive = value; }

        /// <summary>
        /// Gets bool value is thread leader paused
        /// </summary>
        public bool IsPaused { get => isPaused; private set => isPaused = value; }


        /// <summary>
        /// Gets or sets maximum time to process queue in milliseconds
        /// </summary>
        public int MaxQueueProcessTime { get; set; }

        /// <summary>
        /// Gets maximum number of concurrent threads
        /// </summary>
        public int MaxThreads { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxConcurrentThreads">Maximum number of concurrent threads</param>
        /// <param name="maxQueueProcessTime">Maximum time to process queue in milliseconds</param>
        public ThreadLeader(int maxConcurrentThreads = DefaultMaxThread, int maxQueueProcessTime = DefaultMaxQueueProcessingTime)
        {
            MaxThreads = maxConcurrentThreads;
            MaxQueueProcessTime = maxQueueProcessTime;
            pool = new Thread[maxConcurrentThreads];
            for (int i = 0; i < maxConcurrentThreads; i++)
                freeIndexes.Push(i);
            Start();
        }

        /// <summary>
        /// Runs action in concurrent thread
        /// </summary>
        /// <param name="action">Action to run</param>
        /// <returns>Thread instace</returns>
        public Thread Run(Action action)
        {
            if (!IsAlive)
                return null;
            Thread thread = new Thread(new ThreadStart(action));
            RunThread(thread);
            return thread;
        }

        /// <summary>
        /// Runs action in concurrent thread
        /// </summary>
        /// <param name="action">Action to run</param>
        /// <returns>Thread instace</returns>
        public Thread Run(QFunction action)
        {
            return Run(() => action(new FunctionArguments()));
        }

        /// <summary>
        /// Places job in queue to execute in ProcessQueue thread and waits for result
        /// </summary>
        /// <param name="function">Delegate to run</param>
        /// <returns>Function result</returns>
        public object RunQueued(Func<object> function)
        {
            ThreadLeaderJob job = new ThreadLeaderJob()
            {
                Sema = new SemaphoreSlim(1),
                RunDelegate = function
            };
            tasks.Enqueue(job);
            job.Sema.Wait();
            if (job.Error is {})
                throw job.Error;
            return job.Result;
        }

        /// <summary>
        /// Places job in queue to execute in ProcessQueue thread no wait for result
        /// </summary>
        /// <param name="action">Delegate to run</param>
        public void RunQueued(Action action)
        {
            ThreadLeaderJob job = new ThreadLeaderJob()
            {
                RunDelegate = action
            };
            tasks.Enqueue(job);
        }

        void RunThread(Thread thread)
        {
            threadQueue.Enqueue(thread);
        }

        /// <summary>
        /// Disabling thread leader and waits all tasks to finish
        /// </summary>
        /// <param name="timeoutMilliseconds">Timeout</param>
        public void StopAndWait(int timeoutMilliseconds = 0)
        {
            if (!isAlive)
                return;
            isAlive = false;
            if (timeoutMilliseconds <= 0)
                watchdogThread.Join();
            else
                watchdogThread.Join(timeoutMilliseconds);
        }

        /// <summary>
        /// Starts thread leader
        /// </summary>
        public void Start()
        {
            if (isAlive)
                return;
            isAlive = true;
            isPaused = false;
            if (watchdogThread is null)
            {
                watchdogThread = new Thread(new ThreadStart(Watchdog));
                watchdogThread.Start();
            }
        }

        /// <summary>
        /// Disables thread leader
        /// </summary>
        public void Stop()
        {
            if (!isAlive)
                return;
            isAlive = false;
        }

        /// <summary>
        /// Pauses thread leader 
        /// </summary>
        public void Pause()
        {
            if (!isAlive)
                return;
            isPaused = true;
        }

        /// <summary>
        /// Resumes thread leader after pause 
        /// </summary>
        public void Resume()
        {
            if (!isAlive)
                return;
            isPaused = false;
        }

        /// <summary>
        /// Gets count of running concurrent threads
        /// </summary>
        public int BusyThreads
        {
            get
            { return MaxThreads - freeIndexes.Count; }
        }

        /// <summary>
        /// Aborts all running threads and disabling thread leader
        /// </summary>
        public void Kill()
        {
            isAlive = false;
            watchdogThread.Abort();
            while (threadQueue.Count > 0)
                threadQueue.TryDequeue(out Thread _);
            for (int i = 0; i < pool.Length; i++)
                if (pool[i] is {})
                    pool[i].Abort();
        }

        void Watchdog()
        {
            while (isAlive || !threadQueue.IsEmpty || BusyThreads != 0)
            {
                for (int i = 0; i < pool.Length; i++)
                {
                    Thread t = pool[i];
                    if (t is null)
                        continue;
                    if (!t.IsAlive)
                    {
                        pool[i] = null;
                        freeIndexes.Push(i);
                    }
                }
                while (threadQueue.Count > 0 && freeIndexes.Count > 0 && !isPaused)
                {
                    int index;
                    while (!freeIndexes.TryPop(out index))
                        Thread.Yield();
                    Thread thread;
                    while (!threadQueue.TryDequeue(out thread))
                        Thread.Yield();
                    pool[index] = thread;
                    thread.Start();
                }
                Thread.Sleep(WatchdogTickTime);
            }
            watchdogThread = null;
        }


        /// <summary>
        /// Process tasks in queue
        /// </summary>
        public void ProcessQueue()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            while (!tasks.IsEmpty && isAlive && !isPaused)
            {
                if (tasks.TryDequeue(out ThreadLeaderJob job))
                {
                    try
                    {
                        job.Result = job.RunDelegate.DynamicInvoke();
                    }
                    catch (Exception ex)
                    {
                        job.Error = ex;
                    }
                    if (job.Sema is {})
                        job.Sema.Release();
                }
                if (watch.ElapsedMilliseconds > MaxQueueProcessTime)
                    break;
            }
            watch.Stop();
        }

        public void Dispose()
        {
            Kill();
        }
    }
}
