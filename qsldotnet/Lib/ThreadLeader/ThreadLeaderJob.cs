﻿using System;
using System.Threading;

namespace qsldotnet.lib
{
    internal class ThreadLeaderJob
    {
        internal Delegate RunDelegate;
        internal SemaphoreSlim Sema;
        internal object Result;
        internal Exception Error;
    }
}
