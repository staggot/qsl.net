﻿using System;

namespace qsldotnet
{
    public partial class QContext
    {
        #region Arithmetic
        public static object operator +(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.add) && a[QConstants.add] is QFunction)
                return (a[QConstants.add] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.add + " is not implemented");
        }
        public static object operator +(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.add) && b[QConstants.add] is QFunction)
                return (b[QConstants.add] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.add + " is not implemented");
        }
        public static object operator +(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.add) && a[QConstants.add] is QFunction)
                return (a[QConstants.add] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.add) && b[QConstants.add] is QFunction)
                return (b[QConstants.add] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.add + " is not implemented");
        }

        public static object operator -(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.sub) && a[QConstants.sub] is QFunction)
                return (a[QConstants.sub] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.sub + " is not implemented");
        }
        public static object operator -(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.sub) && b[QConstants.sub] is QFunction)
                return (b[QConstants.sub] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.sub + " is not implemented");
        }
        public static object operator -(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.sub) && a[QConstants.sub] is QFunction)
                return (a[QConstants.sub] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.sub) && b[QConstants.sub] is QFunction)
                return (b[QConstants.sub] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.sub + " is not implemented");
        }

        public static object operator *(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.mul) && a[QConstants.mul] is QFunction)
                return (a[QConstants.mul] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.mul + " is not implemented");
        }
        public static object operator *(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.mul) && b[QConstants.mul] is QFunction)
                return (b[QConstants.mul] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.mul + " is not implemented");
        }
        public static object operator *(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.mul) && a[QConstants.mul] is QFunction)
                return (a[QConstants.mul] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.mul) && b[QConstants.mul] is QFunction)
                return (b[QConstants.mul] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.mul + " is not implemented");
        }

        public static object operator /(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.div) && a[QConstants.div] is QFunction)
                return (a[QConstants.div] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.div + " is not implemented");
        }
        public static object operator /(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.div) && b[QConstants.div] is QFunction)
                return (b[QConstants.div] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.div + " is not implemented");
        }
        public static object operator /(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.div) && a[QConstants.div] is QFunction)
                return (a[QConstants.div] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.div) && b[QConstants.div] is QFunction)
                return (b[QConstants.div] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.div + " is not implemented");
        }

        public static object operator %(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.mod) && a[QConstants.mod] is QFunction)
                return (a[QConstants.mod] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.mod + " is not implemented");
        }
        public static object operator %(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.mod) && b[QConstants.mod] is QFunction)
                return (b[QConstants.mod] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.mod + " is not implemented");
        }
        public static object operator %(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.mod) && a[QConstants.mod] is QFunction)
                return (a[QConstants.mod] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.mod) && b[QConstants.mod] is QFunction)
                return (b[QConstants.mod] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.mod + " is not implemented");
        }

        public static QContext operator ++(QContext a)
        {
            if (a.ContainsKey(QConstants.inc) && a[QConstants.inc] is QFunction)
                return (QContext)(a[QConstants.inc] as QFunction)(new FunctionArguments(new object[] { a }));
            throw new NotImplementedException("operator " + QConstants.mod + " is not implemented");
        }

        public static QContext operator --(QContext a)
        {
            if (a.ContainsKey(QConstants.dec) && a[QConstants.dec] is QFunction)
                return (QContext)(a[QConstants.dec] as QFunction)(new FunctionArguments(new object[] { a }));
            throw new NotImplementedException("operator " + QConstants.mod + " is not implemented");
        }

        public static object operator -(QContext a)
        {
            if (a.ContainsKey(QConstants.neg) && a[QConstants.neg] is QFunction)
                return (QContext)(a[QConstants.neg] as QFunction)(new FunctionArguments(new object[] { a }));
            throw new NotImplementedException("operator " + QConstants.mod + " is not implemented");
        }

        public static object operator +(QContext a)
        {
            if (a.ContainsKey(QConstants.pos) && a[QConstants.pos] is QFunction)
                return (QContext)(a[QConstants.pos] as QFunction)(new FunctionArguments(new object[] { a }));
            throw new NotImplementedException("operator " + QConstants.mod + " is not implemented");
        }

        #endregion

        #region Binary
        public static object operator &(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.bitand) && a[QConstants.bitand] is QFunction)
                return (a[QConstants.bitand] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitand + " is not implemented");
        }
        public static object operator &(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.bitand) && b[QConstants.bitand] is QFunction)
                return (b[QConstants.bitand] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitand + " is not implemented");
        }
        public static object operator &(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.bitand) && a[QConstants.bitand] is QFunction)
                return (a[QConstants.bitand] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.bitand) && b[QConstants.bitand] is QFunction)
                return (b[QConstants.bitand] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitand + " is not implemented");
        }

        public static object operator |(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.bitor) && a[QConstants.bitor] is QFunction)
                return (a[QConstants.bitor] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitor + " is not implemented");
        }
        public static object operator |(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.bitor) && b[QConstants.bitor] is QFunction)
                return (b[QConstants.bitor] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitor + " is not implemented");
        }
        public static object operator |(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.bitor) && a[QConstants.bitor] is QFunction)

                 return (a[QConstants.bitor] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.bitor) && b[QConstants.bitor] is QFunction)
                return (b[QConstants.bitor] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitor + " is not implemented");
        }

        public static object operator ^(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.bitxor) && a[QConstants.bitxor] is QFunction)
                return (a[QConstants.bitxor] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitxor + " is not implemented");
        }
        public static object operator ^(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.bitxor) && b[QConstants.bitxor] is QFunction)
                return (b[QConstants.bitxor] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitxor + " is not implemented");
        }
        public static object operator ^(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.bitxor) && a[QConstants.bitxor] is QFunction)
                return (a[QConstants.bitxor] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.bitxor) && b[QConstants.bitxor] is QFunction)
                return (b[QConstants.bitxor] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bitxor + " is not implemented");
        }

        public static object operator <<(QContext a, int b)
        {
            if (a.ContainsKey(QConstants.lshift) && a[QConstants.lshift] is QFunction)
                return (a[QConstants.lshift] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.lshift + " is not implemented");
        }

        public static object operator >>(QContext a, int b)
        {
            if (a.ContainsKey(QConstants.rshift) && a[QConstants.rshift] is QFunction)
                return (a[QConstants.rshift] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.rshift + " is not implemented");
        }

        public static object operator ~(QContext a)
        {
            if (a.ContainsKey(QConstants.bitnot) && a[QConstants.bitnot] is QFunction)
                return (a[QConstants.bitnot] as QFunction)(new FunctionArguments(new object[] { a }));
            throw new NotImplementedException("operator " + QConstants.bitnot + " is not implemented");
        }
        #endregion

        #region Boolean
        public static object operator !(QContext a)
        {
            if (a.ContainsKey(QConstants.boolnot) && a[QConstants.boolnot] is QFunction)
                return (a[QConstants.boolnot] as QFunction)(new FunctionArguments(a));
            throw new NotImplementedException("operator " + QConstants.booland + " is not implemented");
        }

        #endregion

        #region Compare
        public static object operator >(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.bigger) && a[QConstants.bigger] is QFunction)
                return (a[QConstants.bigger] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bigger + " is not implemented");
        }
        public static object operator >(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.bigger) && b[QConstants.bigger] is QFunction)
                return (b[QConstants.bigger] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bigger + " is not implemented");
        }
        public static object operator >(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.bigger) && a[QConstants.bigger] is QFunction)
                return (a[QConstants.bigger] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.bigger) && b[QConstants.bigger] is QFunction)
                return (b[QConstants.bigger] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.bigger + " is not implemented");
        }

        public static object operator <(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.lesser) && a[QConstants.lesser] is QFunction)
                return (a[QConstants.lesser] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.lesser + " is not implemented");
        }
        public static object operator <(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.lesser) && b[QConstants.lesser] is QFunction)
                return (b[QConstants.lesser] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.lesser + " is not implemented");
        }
        public static object operator <(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.lesser) && a[QConstants.lesser] is QFunction)
                return (a[QConstants.lesser] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.lesser) && b[QConstants.lesser] is QFunction)
                return (b[QConstants.lesser] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.lesser + " is not implemented");
        }

        public static object operator >=(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.biggereq) && a[QConstants.biggereq] is QFunction)
                return (a[QConstants.biggereq] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.biggereq + " is not implemented");
        }

         public static object operator >=(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.biggereq) && b[QConstants.biggereq] is QFunction)
                return (b[QConstants.biggereq] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.biggereq + " is not implemented");
        }
        public static object operator >=(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.biggereq) && a[QConstants.biggereq] is QFunction)
                return (a[QConstants.biggereq] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.biggereq) && b[QConstants.biggereq] is QFunction)
                return (b[QConstants.biggereq] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.biggereq + " is not implemented");
        }

        public static object operator <=(QContext a, object b)
        {
            if (a.ContainsKey(QConstants.lessereq) && a[QConstants.lessereq] is QFunction)
                return (a[QConstants.lessereq] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.lessereq + " is not implemented");
        }
        public static object operator <=(object a, QContext b)
        {
            if (b.ContainsKey(QConstants.lessereq) && b[QConstants.lessereq] is QFunction)
                return (b[QConstants.lessereq] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.lessereq + " is not implemented");
        }
        public static object operator <=(QContext a, QContext b)
        {
            if (a.ContainsKey(QConstants.lessereq) && a[QConstants.lessereq] is QFunction)
                return (a[QConstants.lessereq] as QFunction)(new FunctionArguments(a, b));
            if (b.ContainsKey(QConstants.lessereq) && b[QConstants.lessereq] is QFunction)
                return (b[QConstants.lessereq] as QFunction)(new FunctionArguments(a, b));
            throw new NotImplementedException("operator " + QConstants.lessereq + " is not implemented");
        }
        #endregion
    }
}