﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Reflection;
using System.Threading;

namespace qsldotnet
{
    public class QModule : QContext
    {
        internal QEngine Engine;
        internal FileInfo ModuleFile;
        internal DirectoryInfo ModuleDirectory;

        private readonly List<DynamicMethod> compiledMethods = new List<DynamicMethod>();
        private readonly Mutex dynamicMethodMutex = new Mutex();

        internal QModule(QEngine engine, FileInfo moduleFile)
        {
            this.Engine = engine;
            this.ModuleFile = moduleFile;
            this.ModuleDirectory = moduleFile.Directory;
            this.Module = this;
            this["module"] = new QFunction(ModuleMethod);
            this["assembly"] = new QFunction(AssemblyMethod);
        }

        internal object _Get(string name)
        {
            if (ContainsKey(name))
                return this[name];
            return Engine[name];
        }

        internal int AddCompiledMethod(DynamicMethod method)
        {
            dynamicMethodMutex.WaitOne();
            int result = compiledMethods.Count;
            compiledMethods.Add(method);
            dynamicMethodMutex.ReleaseMutex();
            return result;
        }

        internal DynamicMethod GetCompiledMethod(int id)
        {
            return compiledMethods[id];
        }

        internal object AssemblyMethod(FunctionArguments args)
        {
            if (args.Length == 1)
            {
                return Engine.LoadAssembly(ModuleDirectory, args[0].ToString());
            }
            else if (args.Length > 1)
            {
                Assembly[] result = new Assembly[args.Length];
                for (int i = 0; i < args.Length; i++)
                    result[i] = Engine.LoadAssembly(ModuleDirectory, args[i].ToString());
                return result;
            }
            return Unassigned.Value;
        }


        internal object ModuleMethod(FunctionArguments args)
        {
            if (args.Length == 1)
            {
                return Engine.LoadModule(ModuleDirectory, args[0].ToString());
            }
            else if (args.Length > 1)
            {
                QContext[] result = new QContext[args.Length];
                for (int i = 0; i < args.Length; i++)
                    result[i] = Engine.LoadModule(ModuleDirectory, args[i].ToString());
                return result;
            }
            return Unassigned.Value;
        }
    }
}
