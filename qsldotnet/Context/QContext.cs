using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using qsldotnet.core;

namespace qsldotnet
{
    public partial class QContext : IgnoreCaseMap<object>
    {
        public QModule Module { get; protected set; }

        public QContext ParentContext { get; protected set; }

        public string ClassId = null;

        public QContext GlobalContext
        {
            get
            {
                return Module;
            }
        }

        public override bool IsIgnoreCase
        {
            get { return Module.Engine.IgnoreCase; }
        }

        internal static MethodInfo CreateContextMethod = typeof(QContext).GetMethod(nameof(CreateContext), new Type[0]);
        /// <summary>
        /// Creates context inherited from current
        /// </summary>
        /// <returns>New context</returns>
        public QContext CreateContext()
        {
            return new QContext(this);
        }

        /// <summary>
        /// gets or sets variables in current context
        /// </summary>
        /// <param name="key">Variable name</param>
        /// <returns>Variable value</returns>
        public override object this[string key]
        {
            get
            {
                if (ContainsKey(key))
                    return base[key];
                return Unassigned.Value;
            }
            set
            {
                if (value is Unassigned)
                    Remove(key);
                else
                    base[key] = value;
            }
        }

        internal object GetVar(string name)
        {
            QContext currentContext = this;
            while (!(currentContext is null))
            {
                if (currentContext.ContainsKey(name))
                    return currentContext[name];
                currentContext = currentContext.ParentContext;
            }
            return Module._Get(name);
        }

        internal void SetVar(string name, object value)
        {
            QContext currentContext = this;
            while (!(currentContext is null))
            {
                if (currentContext.ContainsKey(name))
                {
                    currentContext[name] = value;
                    return;
                }
                currentContext = currentContext.ParentContext;
            }
            this[name] = value;
        }

        internal void SetVarLocal(string name, object value)
        {
            this[name] = value;
        }

        protected QContext()
        {
            Init(null);
        }

        internal QContext(QModule module)
        {
            this.Module = module;
            Init(module);
        }

        internal QContext(QContext parentContext)
        {
            this.Module = parentContext.Module;
            Init(parentContext);
        }

        internal void Init(QContext context)
        {
            ParentContext = context;
        }

        internal ScriptExecutor RunStringExecutor(string script, IDictionary<string, object> values)
        {
            if (values is {})
                foreach (var kv in values)
                    this[kv.Key] = kv.Value;
            ScriptExecutor runner = new ScriptExecutor(this, script);
            runner.Run();
            return runner;
        }

        internal ScriptExecutor RunStreamExecutor(StreamReader stream, IDictionary<string, object> values)
        {
            if (values is {})
                foreach (var kv in values)
                    this[kv.Key] = kv.Value;
            ScriptExecutor runner = new ScriptExecutor(this, stream);
            runner.Run();
            return runner;
        }

        internal string[] VarNames()
        {
            string[] result = new string[Count];
            int i = 0;
            foreach (string key in base.Keys)
            {
                result[i] = key;
                i++;
            }
            return result;
        }

        internal ScriptExecutor RunFileExecutor(string path, IDictionary<string, object> values)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                return RunStreamExecutor(new StreamReader(fs), values);
            }
        }

        internal void RunStream(StreamReader stream, Dictionary<string, object> values = null)
        {
            RunStreamExecutor(stream, values);
        }

        internal void RunFile(string path, Dictionary<string, object> values = null)

         {
            RunFileExecutor(path, values);
        }

        internal void RunString(string code, IDictionary<string, object> values = null)
        {
            RunStringExecutor(code, values);
        }

        internal object EvalString(string code, IDictionary<string, object> values = null)
        {
            ScriptExecutor executor = RunStringExecutor(code, values);
            if (executor.Stack.Count > 0)
                return executor.Stack.PopValue();
            return Unassigned.Value;
        }

        internal object EvalFile(string path, IDictionary<string, object> values = null)
        {
            ScriptExecutor executor = RunFileExecutor(path, values);
            if (executor.Stack.Count > 0)
                return executor.Stack.PopValue();
            return Unassigned.Value;
        }

        internal object EvalStream(StreamReader stream, IDictionary<string, object> values = null)
        {
            ScriptExecutor executor = RunStreamExecutor(stream, values);
            if (executor.Stack.Count > 0)
                return executor.Stack.PopValue();
            return Unassigned.Value;
        }

        /// <summary>
        /// Evaluates expression
        /// </summary>
        /// <param name="code">Source code</param>
        /// <param name="values">Initial variables {name, value}</param>
        /// <returns>Executional context</returns>
        internal object Eval(string code, IDictionary<string, object> values = null)
        {
            return CreateContext().EvalString(code, values);
        }

        /// <summary>
        /// Evaluates expression
        /// </summary>
        /// <param name="stream">Source code stream</param>
        /// <param name="values">Initial variables {name, value}</param>
        /// <returns>Executional context</returns>
        internal object Eval(Stream stream, IDictionary<string, object> values = null)
        {
            return CreateContext().EvalStream(new StreamReader(stream), values);
        }

        /// <summary>
        /// Run source code in current context
        /// </summary>
        /// <param name="code">Source code</param>
        /// <param name="values">Initial variables {name, value}</param>
        /// <returns>Executional context</returns>
        internal QContext Run(string code, IDictionary<string, object> values = null)
        {
            return CreateContext().RunStringExecutor(code, values).Context;
        }

        /// <summary>
        /// Run source code in current context
        /// </summary>
        /// <param name="stream">Source code stream</param>
        /// <param name="values">Initial variables {name, value}</param>
        /// <returns>Executional context</returns>
        internal QContext Run(Stream stream, IDictionary<string, object> values = null)
        {
            return CreateContext().RunStreamExecutor(new StreamReader(stream), values).Context;
        }

        public override string ToString()
        {
            return Module.Engine.DumpString(this);
        }
    }
}
