﻿using System.Diagnostics;
using System.Threading;
using System.IO;

namespace qsldotnet
{
    static class BuildinTemplates
    {
        internal static void Init(QEngine engine)
        {
            engine.AddAllowedType(typeof(QsTemplate));
            engine.AddAllowedType(typeof(QsTemplateBank));

            QModule templatesModule = new QModule(engine, engine.GlobalModule.ModuleFile);
            engine["templates"] = templatesModule;
            templatesModule["load"] = new QFunction((args) => MkTemplate(engine, args));
        }

        internal static object MkTemplate(QEngine engine, FunctionArguments args)
        {
            QArgs argv = args.ExctractArguments(
                engine,
                new string[] { "path", "pattern" },
                new object[] { null, null },
                null
            );
            string path = null;
            string pattern = null;
            if (args.Length < 1 || args.Length > 3)
                throw new ScriptException(engine, "Invalid Parameters, Required (template|file:path)");
            if (argv[0] is {})
                path = argv[0].ToString();
            if (argv[1] is {})
                pattern = argv[1].ToString();
            if (File.Exists(path))
            {
                return new QsTemplate(engine, new FileInfo(path));
            }
            if (Directory.Exists(path))
            {
                if (pattern is {})
                    return new QsTemplateBank(engine, new DirectoryInfo(path), pattern);
                return new QsTemplateBank(engine, new DirectoryInfo(path));
            }
            return new QsTemplate(engine, argv[2].ToString());
        }
    }
}