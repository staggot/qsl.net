﻿using System;
using System.IO;

namespace qsldotnet
{
    internal static class BuildinJson
    { 
        internal static void Init(QEngine engine)
        {
            QContext jsonContext = engine.CreateContext();
            engine["json"] = jsonContext;
            jsonContext["dumps"] = new QFunction((args) => Dumps(engine, args));
            jsonContext["loads"] = new QFunction((args) => Loads(engine, args));
            jsonContext["dump"] = new QFunction((args) => Dump(engine, args));
            jsonContext["load"] = new QFunction((args) => Load(engine, args));
        }

        internal static object Dumps(QEngine engine, FunctionArguments args)
        {
            if (args.Length == 1)
                return engine.DumpString(args[0]);
            throw new ScriptException(engine, "Invalid arguments, required (obj)");
        }

        internal static object Dump(QEngine engine, FunctionArguments args)
        {
            QArgs arg = args.ExctractArguments(engine, new string[] { "stream", "data" }, new object[] { null, null });

            object stream = arg[0];
            object data = arg[1];

            if(stream is StreamWriter)
            {
                engine.Jsonizer.Dump(stream as StreamWriter, data);
                return Unassigned.Value;
            }
            if(stream is Stream)
            {
                StreamWriter sw = new StreamWriter(stream as Stream);
                engine.Jsonizer.Dump(sw, data);
                sw.Flush();
                return Unassigned.Value;
            }
            throw new ScriptException(engine, "Invalid arguments, required (stream, obj)");
        }

        internal static object Loads(QEngine engine, FunctionArguments args)
        {
            if (args.Length == 1)
            {
                object str = args[0];
                if (str is string)
                    return engine.Jsonizer.LoadString(str as string);
            }
            throw new ScriptException(engine, "Invalid arguments, required (string)");
        }

        internal static object Load(QEngine engine, FunctionArguments args)
        {
            if (args.Length == 1)
            {
                object stream = args[0];
                if (stream is StreamReader)
                    return engine.Jsonizer.Load(stream as StreamReader);
                if(stream is Stream)
                {
                    StreamReader sr = new StreamReader(stream as Stream);
                    object result = engine.Jsonizer.Load(sr);
                    sr.DiscardBufferedData();
                    return result;
                }
            }
            throw new ScriptException(engine, "Invalid arguments, required (stream)");
        }

    }
}
