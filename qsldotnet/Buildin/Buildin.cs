﻿using System;
using System.Text.RegularExpressions;

namespace qsldotnet
{
    internal static class Buildin
    {
        internal static void Init(QEngine engine)
        {
            engine.AllowType(typeof(byte));
            engine.AllowType(typeof(sbyte));
            engine.AllowType(typeof(char));
            engine.AllowType(typeof(ushort));
            engine.AllowType(typeof(short));
            engine.AllowType(typeof(uint));
            engine.AllowType(typeof(int));
            engine.AllowType(typeof(ulong));
            engine.AllowType(typeof(long));
            engine.AllowType(typeof(float));
            engine.AllowType(typeof(double));
            engine.AllowType(typeof(decimal));
            engine.AllowType(typeof(object));
            engine.AllowType(typeof(string));
            engine.AllowType(typeof(Type));

            //Misc
            engine.AllowType(typeof(Regex));
            engine.AllowType(typeof(Math));
            engine.AllowType(typeof(System.Globalization.CultureInfo));
            engine.AllowType(typeof(DateTime));
            engine.AllowType(typeof(TimeSpan));
            engine.AllowType(typeof(DateTimeKind));

            engine["eval"] = new QFunction((args) => EvalFunction(engine, args));
            engine["env"] = Environment.GetEnvironmentVariables(EnvironmentVariableTarget.Process);
            engine["print"] = new QFunction((args) => Print(engine, args));
            engine["error"] = new QFunction((args) => Print(engine, args));
            engine["range"] = new QFunction((args) => Range(engine, args));
            engine["stdout"] = Console.Out;
            engine["stderr"] = Console.Error;
            engine["stdin"] = Console.In;
            engine["set_jsonizer"] = new QFunction((args) => SetJsonizer(engine, args));
            engine["typeof"] = new QFunction((args) => TypeOf(engine, args));
            engine["exit"] = new QFunction((args) => Exit(engine, args));
        }

        internal static object Exit(QEngine engine, FunctionArguments args)
        {
            if (args.Length > 0)
            {
                Environment.Exit((int)args[0]);
            }
            else
            {
                Environment.Exit(0);
            }
            return Unassigned.Value;
        }

        internal static object EvalFunction(QEngine engine, FunctionArguments args)
        {
            return engine.Eval(args[0].ToString());
        }

        internal static object TypeOf(QEngine engine, FunctionArguments args)
        {
            if (args.Length == 1 && args[0] is Type)
            {
                return args[0];
            }
            throw new ScriptException(engine, "Invalid Parameters, Required (Type)");
        }

        internal static object Range(QEngine engine, FunctionArguments args)
        {
            long start = 0;
            long step = 1;
            long end;
            switch (args.Length)
            {
                case 1:
                    end = Convert.ToInt64(args[0]);
                    break;
                case 2:
                    start = Convert.ToInt64(args[0]);
                    end = Convert.ToInt64(args[1]);
                    if (end < start) step = -1;
                    break;
                case 3:
                    start = Convert.ToInt64(args[0]);
                    end = Convert.ToInt64(args[1]);
                    step = Convert.ToInt64(args[2]);
                    break;
                default:
                    throw new Exception("Invalid arguments");
            }

            return new QsRange(start, end, step);
        }

        internal static object SetJsonizer(QEngine engine, FunctionArguments args)
        {
            if (args.Length == 2 && args[0] is Type)
            {
                if ((args[1] is null || args[1] is Unassigned))
                {
                    engine.Jsonizer.SetJsonizer(args[0] as Type, null);
                    return Unassigned.Value;
                }
                if (args[1] is QFunction)
                {
                    QFunction pringerFunction = args[1] as QFunction;
                    engine.Jsonizer.SetJsonizer(args[0] as Type, (arg) =>
                    {
                        FunctionArguments fargs = new FunctionArguments(new object[] { arg });
                        return (string)pringerFunction(fargs);
                    }
                        );
                    return Unassigned.Value;
                }

            }
            throw new ScriptException(engine, "Invalid arguments, required (Type, Function)");

        }

        internal static object Error(QEngine engine, FunctionArguments values)
        {
            if (values.Length == 1)
            {
                if (values[0] is null)
                    Console.Error.Write("null");
                else
                    Console.Error.Write(values[0]);
                return Unassigned.Value;
            }
            else if (values.Length > 1)
            {
                string format = values[0].ToString();
                object[] vals = new string[values.Length - 1];
                for (int i = 0; i < values.Length - 1; i++)
                    vals[i] = (values[i + 1]).ToString();
                Console.Error.Write(string.Format(format, vals));
                return Unassigned.Value;
            }
            throw new ScriptException(engine, "Invalid Parameters, Required (value) or (format,value0[,value1,...])");
        }

        internal static object Print(QEngine engine, FunctionArguments args)
        {
            QArgs _args = args.ExctractArguments(engine, new string[] { "format" }, new object[] { "{0}" }, null);
            if (_args.Length == 0)
            {
                Console.WriteLine();
                return Unassigned.Value;
            }
            if (_args.Length == 1)
            {
                Console.WriteLine(_args[0]);
                return Unassigned.Value;
            }
            object[] lines = new object[_args.Length - 1];
            for (int i = 1; i < _args.Length; i++)
                lines[i - 1] = _args[i].ToString();
            Console.WriteLine(string.Format(_args[0].ToString(), lines));
            return Unassigned.Value;
        }
    }
}
