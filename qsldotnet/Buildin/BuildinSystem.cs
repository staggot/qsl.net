﻿using System;
using System.Text;
using System.Diagnostics;

namespace qsldotnet
{
    static class BuildinSystem
    {
        internal static void Init(QEngine engine)
        {
            engine["system"] = new QFunction((args) => System(engine, args));
            engine["subprocess"] = new QFunction((args) => Subprocess(engine, args));
        }

        internal static object Subprocess(QEngine engine, FunctionArguments args)
        {
            if (args.Length > 0)
            {
                string name = args[0].ToString();
                StringBuilder arguments = new StringBuilder();
                for (int i = 1; i < args.Length; i++)
                {

                    if (args[i] is string)
                    {
                        if (i > 1)
                            arguments.Append(" ");
                        arguments.Append(engine.DumpString(args[i]));
                    }
                    else
                    {
                        throw new ScriptException(engine, "Invalid arguments, required (command. arg0, arg1, ...)");
                    }
                }
                Process subprocess = new Process();
                subprocess.StartInfo = new ProcessStartInfo(name, arguments.ToString());

                return subprocess;
            }
            throw new ScriptException(engine, "Invalid arguments, required (command. arg0, arg1, ...)");
        }

        internal static object System(QEngine engine, FunctionArguments args)
        {
            string name = args[0].ToString();
            StringBuilder arguments = new StringBuilder();

            for (int i = 1; i < args.Length; i++)
            {
                if (args[i] is string)
                {
                    if (i > 1)
                        arguments.Append(" ");
                    arguments.Append(engine.DumpString(args[i]));
                }
                else
                {
                    throw new ScriptException(engine, "Invalid arguments, required (command, arg0, arg1, ...)");
                }
            }
            QContext result = engine.GlobalModule.CreateContext();
            result["exit"] = -1;

            using (Process p = new Process())
            {
                p.StartInfo.FileName = name;
                p.StartInfo.Arguments = arguments.ToString();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.Start();
                result["stderr"] = p.StandardError.ReadToEnd();
                result["stdout"] = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                result["exit"] = p.ExitCode;
            }
            return result;
        }
    }
}
