﻿using System;
using System.IO;
using qsldotnet.lib;

namespace qsldotnet
{
    internal static class BuildinLogging
    {

        internal static void Init(QEngine engine)
        {
            QModule loggerModule = engine.CreateModule();
            loggerModule["logger"] = new QFunction((args) => new Logger());
            loggerModule["NONE"] = LogLevel.NONE;
            loggerModule["DEBUG"] = LogLevel.DEBUG;
            loggerModule["INFO"] = LogLevel.INFO;
            loggerModule["WARNING"] = LogLevel.WARNING;
            loggerModule["ERROR"] = LogLevel.ERROR;
            loggerModule["CRITICAL"] = LogLevel.CRITICAL;
            loggerModule["FATAL"] = LogLevel.FATAL;
            loggerModule["ALL"] = LogLevel.ALL;
            engine["logging"] = loggerModule;

        }
    }
}
