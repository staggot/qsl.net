﻿using System;
using System.Collections;

namespace qsldotnet
{

    public class QsRangeEnumerator : IEnumerator
    {
        QsRange range;
        int current_index = -1;

        public object Current => range[current_index];

        internal QsRangeEnumerator(QsRange range)
        {
            this.range = range;
            Reset();
        }

        public bool MoveNext()
        {
            current_index++;
            if (current_index >= range.Length)
                return false;
            return true;
        }

        public void Reset()
        {
            current_index = -1;
        }
    }

    public class QsRange : IEnumerable, ICollection
    {
        internal long Start, End, Step;
        internal long Length;

        internal QsRange(long start, long end, long step)
        {
            this.Start = start;
            this.End = end;
            this.Step = step;
            if(step > 0)
            {
                Length = (end - start) / step;
            }
            else if (step < 0)
            {
                Length = (end - start) / step;
            }
            else
            {
                Length = 0;
            }
         }

        public object this[long index]
        {
            get
            {
                if (index >= Length || Length < 0)
                    throw new IndexOutOfRangeException();
                return Start + index * Step;
            }
            set => throw new NotImplementedException("range is immutable");
        }

        public bool IsFixedSize
        {
            get { return true; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public int Count
        {
            get { return (int)Length; }
        }

        public bool IsSynchronized
        {
            get { return true; }
        }

        public object SyncRoot
        {
            get { return this; }
        }

        public int Add(object value)
        {
            throw new NotImplementedException("range is immutable");
        }

        public void Clear()
        {
            throw new NotImplementedException("range is immutable");
        }

        public bool Contains(object value)
        {
            throw new NotImplementedException();  //TODO: Implement?
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException(); //TODO: Implement?
        }

        public IEnumerator GetEnumerator()
        {
            return new QsRangeEnumerator(this);
        }

        public int IndexOf(object value)
        {
            throw new NotImplementedException("range is immutable");
        }

        public void Insert(int index, object value)
        {
            throw new NotImplementedException("range is immutable");
        }

        public void Remove(object value)
        {
            throw new NotImplementedException("range is immutable");
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException("range is immutable");
        }
    }
}
