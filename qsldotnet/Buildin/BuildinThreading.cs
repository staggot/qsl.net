﻿using System.Diagnostics;
using System.Threading;
using qsldotnet.lib;

namespace qsldotnet
{
    static class BuildinThreading
    {
        internal static void Init(QEngine engine)
        {
            engine.AddAllowedType(typeof(Mutex));
            engine.AddAllowedType(typeof(Semaphore));
            engine.AddAllowedType(typeof(Thread));
            engine.AddAllowedType(typeof(ThreadStart));
            engine.AddAllowedType(typeof(ParameterizedThreadStart));
            engine.AddAllowedType(typeof(System.Threading.ThreadState));
            engine.AddAllowedType(typeof(CompressedStack));
            engine.AddAllowedType(typeof(ThreadPriority));
            engine.AddAllowedType(typeof(ThreadPriorityLevel));
            engine.AddAllowedType(typeof(ThreadLeader));
        }
    }
}