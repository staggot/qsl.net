﻿using System;
using System.IO;
using System.Globalization;
using System.Text;
using System.Collections.Generic;

namespace qsldotnet
{
    internal static class BuildinIO
    {
        internal static void Init(QEngine engine)
        {
            QModule ioModule = engine.CreateModule();
            engine["io"] = ioModule;
            engine.AllowType(typeof(FileStream));
            engine.AllowType(typeof(Stream));
            engine.AllowType(typeof(BufferedStream));
            engine.AllowType(typeof(StreamReader));
            engine.AllowType(typeof(StreamWriter));
            engine.AllowType(typeof(FileAccess));
            engine.AllowType(typeof(FileShare));
            engine.AllowType(typeof(FileMode));
            engine.AllowType(typeof(Encoding));
            ioModule["open"] = new QFunction((args) => Open(engine, args));
        }

        internal static object Open(QEngine engine, FunctionArguments args)
        {
            QArgs arg = args.ExctractArguments(
                engine,
                new string[] { "path", "mode", "encoding", "bufferSize" },
                new object[] { null, "r", "utf8", 4096 },
                null);
            object path = arg["path"];
            object mode = arg["mode"];
            object encoding = arg["encoding"];
            object bufferSize = arg["bufferSize"];

            switch ((string)mode)
            {
                case "r":
                    return new StreamReader((string)path, Encoding.GetEncoding((string)encoding), true, (int)bufferSize);
                case "w":
                    return new StreamWriter((string)path, false, Encoding.GetEncoding((string)encoding), (int)bufferSize);
                case "rb":
                case "br":
                    return new FileStream((string)path, FileMode.Open, FileAccess.Read, FileShare.Read, (int)bufferSize);
                case "wb":
                case "bw":
                    return new FileStream((string)path, FileMode.Create, FileAccess.Write, FileShare.None, (int)bufferSize);
                case "rb+":
                case "br+":
                    return new FileStream((string)path, FileMode.Open, FileAccess.ReadWrite, FileShare.None, (int)bufferSize);
                case "wb+":
                case "bw+":
                    return new FileStream((string)path, FileMode.Create, FileAccess.ReadWrite, FileShare.None, (int)bufferSize);
                case "a":
                    return new StreamWriter((string)path, true, Encoding.GetEncoding((string)encoding), (int)bufferSize);
                case "ab":
                case "ba":
                    return new FileStream((string)path, FileMode.Append, FileAccess.Write, FileShare.None, (int)bufferSize);
                case "ab+":
                case "ba+":
                    return new FileStream((string)path, FileMode.Append, FileAccess.ReadWrite, FileShare.None, (int)bufferSize);
                default:
                    throw new ScriptException(engine, "Invalid mode argument");

            }
        }
    }
}
