﻿using System;
using System.Net.Sockets;
using System.Net;


namespace qsldotnet
{
    internal static class BuildinNet
    {
        internal static void Init(QEngine engine)
        {
            //TcpClient c = new TcpClient(AddressFamily.AppleTalk,)
            engine.AllowType(typeof(NetworkStream));
            engine.AllowType(typeof(Socket));
            engine.AllowType(typeof(IPAddress));
            engine.AllowType(typeof(Dns));
            engine.AllowType(typeof(TcpListener));
            engine.AllowType(typeof(TcpClient));
            engine.AllowType(typeof(Socket));
            engine.AllowType(typeof(SocketError));
            engine.AllowType(typeof(AddressFamily));

            QModule netModule = engine.CreateModule();
            engine["net"] = netModule;
            netModule["tcpclient"] = new QFunction((args) => ClientTCP(engine, args));
            netModule["tcplistener"] = new QFunction((args) => ListenerTCP(engine, args));
        }



        internal static object ClientTCP(QEngine engine, FunctionArguments args)
        {
            QArgs arg = args.ExctractArguments(
                engine,
                new string[] { "hostname", "port" },
                new object[] { null, 0 },
                null);
            string hostname = (string)arg["hostname"];
            int port = (int)arg["port"];
            return new TcpClient(hostname, port);
        }

        internal static object ListenerTCP(QEngine engine, FunctionArguments args)
        {
            QArgs arg = args.ExctractArguments(
                engine,
                new string[] { "ip", "port" },
                new object[] { null, 0 },
                null);
            int port = (int)arg["port"];
            IPAddress addr = IPAddress.Any;
            if (arg["ip"] is {})
                addr = IPAddress.Parse((string)arg["ip"]);
            return new TcpListener(addr, port);
        }
    }
}
