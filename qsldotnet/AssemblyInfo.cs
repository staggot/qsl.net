﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("qsl.net.45")]
[assembly: AssemblyDescription("Q Script Interpreter/Compiler")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("StagNet")]
[assembly: AssemblyProduct("qsl.net")]
[assembly: AssemblyCopyright("Copyright © Valeriy Gordiychenko (staggot) 2019")]
[assembly: AssemblyTrademark("StagNet")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("849696ed-99ab-4c47-a7e1-5bc5622c3424")]
[assembly: AssemblyVersion("0.9.2")]
