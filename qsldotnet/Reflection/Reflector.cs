﻿using System;
using System.Reflection;

namespace qsldotnet.core
{
    internal static class Reflector
    {
        internal const BindingFlags FindBindingsFlags = (BindingFlags)(0xffffff & (~(int)BindingFlags.DeclaredOnly));
        internal const BindingFlags FindStaticBindingFlags = (BindingFlags)((int)FindBindingsFlags & (~(int)BindingFlags.Instance));
        internal const BindingFlags FindInstanceBindingFlags = (BindingFlags)((int)FindBindingsFlags & (~(int)BindingFlags.Static));

        internal static bool IsNumberType(this Type t)
        {
            return t.IsIntegerType() || t.IsFloatType();
        }

        internal static bool IsFloatType(this Type t)
        {
            switch (Type.GetTypeCode(t))
            {
                case TypeCode.Decimal:
                case TypeCode.Single:
                case TypeCode.Double:
                    return true;
                default:
                    return false;
            }
        }

        internal static bool IsIntegerType(this Type t)
        {
            switch (Type.GetTypeCode(t))
            {
                case TypeCode.Char:
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                    return true;
                default:
                    return false;
            }
        }

        internal static bool IsNumber(this object o)
        {
            if (o is null)
                return false;
            return o.GetType().IsNumberType();
        }

        internal static void DynamicInvokeDelegate(ExecutionStack stack, Delegate deleg, FunctionArguments parameters)
        {

            MethodInfo method = deleg.Method;

            object target = deleg.Target;
            DynamicInvokeMethod(stack, method, target, parameters);
        }

        internal static MethodInfo FindMethod(Type type, object obj, string name, FunctionArguments parameters, bool ignoreCase)
        {
            MethodInfo[] methods = type.GetMethods(FindBindingsFlags);
            MethodInfo matchingMethod = null;
            MethodInfo matchingMethodIgnoreCase = null;
            int bestScore = -1;
            int bestScoreIgnoreCase = -1;
            string lower = name.ToLower();
            for (int i = 0; i < methods.Length; i++)
            {
                MethodInfo method = methods[i];
                bool staticMatch = (method.IsStatic && obj is null) || (!method.IsStatic && obj is {});
                if (!staticMatch)
                    continue;
                if (method.Name == name)
                {
                    int score = parameters.MatchMethod(method, ignoreCase);
                    if (score > bestScore)
                    {
                        bestScore = score;
                        matchingMethod = method;
                    }
                }
                else if (method.Name.ToLower() == lower && ignoreCase)
                {
                    int score = parameters.MatchMethod(method, ignoreCase);
                    if (score > bestScoreIgnoreCase)
                    {
                        bestScoreIgnoreCase = score;
                        matchingMethodIgnoreCase = method;
                    }
                }
            }
            if (matchingMethod is null)
                return matchingMethodIgnoreCase;
            return matchingMethod;
        }


        internal static void DynamicInvokeMethod(ExecutionStack stack, MethodBase method, object target, FunctionArguments parameters)
        {
            object[] values = parameters.GetArgumentsFor(method, stack.Engine.IgnoreCase);

            object result;
            if (method is ConstructorInfo)
            {
                result = (method as ConstructorInfo).Invoke(values);
                stack.PushValue(result);
            }
            else if (method is MethodInfo)
            {
                result = method.Invoke(target, values);
                if ((method as MethodInfo).ReturnType != typeof(void))
                    stack.PushValue(result);
            }
            else
                throw new ExecutionException(stack.Context, "Can invoke  only ConstructorInfo or MethodInfo");

        }

        internal static MemberInfo FindInstanceMember(Type type, string name, bool ignoreCase)
        {
            MemberInfo backup = null;
            string lower = name.ToLower();
            foreach (MemberInfo member in type.GetMembers(FindInstanceBindingFlags))
            {
                if (member.Name == name)
                    return member;
                if (ignoreCase && lower == member.Name.ToLower())
                    backup = member;
            }
            return backup;
        }

        internal static MemberInfo FindStaticMember(Type type, string name, bool ignoreCase)
        {
            MemberInfo backup = null;
            string lower = name.ToLower();
            foreach (MemberInfo member in type.GetMembers(FindStaticBindingFlags))
            {
                if (member.Name == name)
                    return member;
                if (ignoreCase && lower == member.Name.ToLower())
                    backup = member;
            }
            return backup;
        }

        internal static ConstructorInfo FindConstructor(Type type, FunctionArguments parameters, bool ignoreCase)
        {
            ConstructorInfo[] methods = type.GetConstructors(FindBindingsFlags);

            ConstructorInfo matchingMethod = null;
            int bestScore = -1;

            for (int i = 0; i < methods.Length; i++)
            {
                ConstructorInfo method = methods[i];
                int score = parameters.MatchMethod(method, ignoreCase);
                if (score > bestScore)
                {
                    bestScore = score;
                    matchingMethod = method;
                }
            }
            return matchingMethod;
        }

        internal static object ParamConvert(object value, Type paramType)
        {
            if (value is null)
                return null;
            if (paramType.IsInstanceOfType(value))
                return value;
            if (paramType.IsSubclassOf(typeof(Delegate)) && value is QFunction)
                return QCompiler.EmitDelegate(paramType, value as QFunction);
            return Convert.ChangeType(value, paramType);
        }
    }
}
