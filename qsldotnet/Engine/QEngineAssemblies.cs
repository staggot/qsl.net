﻿using System;
using System.IO;
using System.Reflection;

namespace qsldotnet
{
    public partial class QEngine
    {
        void InitAssemblies()
        {
            Assembly[] RunningAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            for (int i = 0; i < RunningAssemblies.Length; i++)
            {
                LoadAssembly(RunningAssemblies[i]);
            }
        }

        internal Assembly LoadAssembly(DirectoryInfo directory, string assemblyFile)
        {
            FileInfo afi = LocateAssembly(directory, assemblyFile);
            if (afi is {})
            {
                string fullname = afi.FullName;
                if (!loadedAssemblies.ContainsKey(fullname))
                {
                    loadedAssemblies[fullname] = Assembly.LoadFile(fullname);
                    if (loadedAssemblies[fullname] is {})
                        LoadAssembly(loadedAssemblies[fullname]);
                }
                return loadedAssemblies[fullname];
            }
            throw new ScriptEngineException(this, string.Format("Assembly not found: {0}", assemblyFile));
        }

        internal FileInfo LocateAssembly(DirectoryInfo directory, string name)
        {
            return LocateLib(directory, AssembliesDirectory, null, name, ".dll", ".exe");
        }

        void LoadAssembly(Assembly assembly)
        {
            if (Assemblies.Contains(assembly))
                return;
            Assemblies.Add(assembly);
            Type[] assTypes = assembly.GetTypes();
            for (int t = 0; t < assTypes.Length; t++)
            {
                Type type = assTypes[t];
                if (!type.IsPublic)
                    continue;
                if (!type.IsVisible)
                    continue;

                string fullName = type.FullName;
                if (IgnoreCase)
                    fullName = fullName.ToLower();
                types[fullName] = type;
            }
        }
    }
}