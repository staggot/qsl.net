using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.IO;
using qsldotnet.core;

[assembly: InternalsVisibleTo("qsl.net.tests")]

namespace qsldotnet
{
    public partial class QEngine
    {
        private readonly HashSet<Type> allowedTypes = new HashSet<Type>()
        {
            typeof(FunctionArguments),
            typeof(ArrayArguments),
            typeof(QFunction),
            typeof(QConstructor),
            typeof(QArgumentLocker),
            typeof(QArgs),
            typeof(Unassigned)
        };
        private readonly HashSet<Assembly> Assemblies = new HashSet<Assembly>();
        private readonly Dictionary<string, Assembly> loadedAssemblies = new Dictionary<string, Assembly>();
        private readonly Dictionary<string, QModule> loadedModules = new Dictionary<string, QModule>();
        private readonly Dictionary<string, object> constants = new Dictionary<string, object>();
        private readonly Dictionary<string, Type> types = new Dictionary<string, Type>();

        internal static readonly string ModulesDirectory = "modules";
        internal static readonly string AssembliesDirectory = "lib";
        internal static readonly string ScriptExtention = ".qs";
        internal static readonly string JsonExtention = ".json";
        internal static readonly string DefaultModuleFile = "main";

        /// <summary>
        /// Default module
        /// </summary>
        public QModule GlobalModule { get; private set; }

        /// <summary>
        /// Gets security flags
        /// </summary>
        public QSecurityFlags Flags { get; private set; }
        public QJson Jsonizer { get; private set; }
        internal bool IgnoreCase { get; private set; } = true;
        internal bool StrictTypes { get; private set; } = false;

        internal Dictionary<string, string> nameMap = new Dictionary<string, string>();

        internal DirectoryInfo CurrentModuleDirectory { get; set; } =  new DirectoryInfo(Directory.GetCurrentDirectory() + "/" + ModulesDirectory);
        /// <summary>
        /// Creates Script Engine
        /// </summary>
        /// <param name="flags">Engine Security Flags</param>
        /// <param name="allowedTypes">Types Allowed To Operate With</param>
        public QEngine(QSecurityFlags flags, Type[] allowedTypes) : this((flags | QSecurityFlags.StrictTypes))
        {
            for (int i = 0; i < allowedTypes.Length; i++)
                AllowType(allowedTypes[i]);
        }

        /// <summary>
        /// Creates Script Engine
        /// </summary>
        /// <param name="flags">Engine Security Flags</param>
        public QEngine(QSecurityFlags flags)
        {
            IgnoreCase = (flags & QSecurityFlags.CaseSensitive) == 0;
            StrictTypes = (flags & QSecurityFlags.StrictTypes) != 0;
            Flags = flags;
            Jsonizer = new QJson();
            GlobalModule = new QModule(this, new FileInfo(Environment.GetCommandLineArgs()[0]));
            InitAssemblies();
            Buildin.Init(this);
            BuildinJson.Init(this);
            BuildinTemplates.Init(this);
            if ((flags & QSecurityFlags.ForbidSystem) == 0)
                BuildinSystem.Init(this);
            if ((flags & QSecurityFlags.ForbidTheading) == 0)
                BuildinThreading.Init(this);
            if ((flags & QSecurityFlags.ForbidNet) == 0)
                BuildinNet.Init(this);
            if ((flags & QSecurityFlags.ForbidIO) == 0)
                BuildinIO.Init(this);

        }

        internal static MethodInfo CreateContextMethod = typeof(QEngine).GetMethod(nameof(CreateContext), new Type[0]);
        /// <summary>
        /// Creates new context inherited from global module
        /// </summary>
        /// <returns>New context</returns>
        public QContext CreateContext()
        {
            return GlobalModule.CreateContext();
        }

        /// <summary>
        /// Creates new module
        /// </summary>
        /// <returns>New module</returns>
        public QModule CreateModule()
        {
            return new QModule(this, GlobalModule.ModuleFile);
        }

        /// <summary>
        /// Creates Script Engine
        /// </summary>
        public QEngine() : this(QSecurityFlags.None)
        {
            
        }

        internal Type FindType(string name)
        {
            if (IgnoreCase)
                name = name.ToLower();
            if (types.ContainsKey(name))
                return types[name];
            return null;
        }

        internal void SetType(Type type)
        {
            string name = type.FullName;
            if (IgnoreCase)
                name = name.ToLower();
            types[name] = type;
        }

        /// <summary>
        /// Global Engine Constants
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public object this[string name]
        {
            get
            {
                if (constants.ContainsKey(name))
                    return constants[name];
                if (IgnoreCase)
                {
                    string icName = name.ToLower();
                    if (nameMap.ContainsKey(icName))
                        return constants[nameMap[icName]];
                }
                return Unassigned.Value;
            }
            set
            {
                if (value is Unassigned)
                {
                    Remove(name);
                    return;
                }
                if (IgnoreCase)
                {
                    string icName = name.ToLower();
                    if (nameMap.ContainsKey(icName))
                        name = nameMap[icName];
                    else
                        nameMap[icName] = name;
                    constants[name] = value;
                }
                else
                {
                    constants[name] = value;
                }
            }
        }

        internal bool Remove(string key)
        {
            if (IgnoreCase)
            {
                string icName = key.ToLower();
                return nameMap.Remove(icName) || constants.Remove(nameMap[icName]);
            }
            return constants.Remove(key);
        }

        /// <summary>
        /// Evaluates Expression
        /// </summary>
        /// <param name="code">Source code</param>
        /// <param name="values">Initial variables {name, value}</param>
        /// <returns>Evaluation result</returns>
        public object Eval(string code, IDictionary<string,object> values = null)
        {
            return GlobalModule.Eval(code, values);
        }

        /// <summary>
        /// Evaluates Expression
        /// </summary>
        /// <param name="stream">Source code stream</param>
        /// <param name="values">Initial variables {name, value}</param>
        /// <returns>Evaluation result</returns>
        public object Eval(Stream stream, IDictionary<string,object> values = null)
        {
            return GlobalModule.Eval(stream, values);
        }

        /// <summary>
        /// Run Source Code
        /// </summary>
        /// <param name="code">Source code</param>
        /// <param name="values">Initial variables {name, value}</param>
        /// <returns>Executional context</returns>
        public QContext Run(string code, IDictionary<string,object> values = null)
        {
            return GlobalModule.Run(code, values);
        }

        /// <summary>
        /// Run Source Code
        /// </summary>
        /// <param name="stream">Source code stream</param>
        /// <param name="values">Initial variables {name, value}</param>
        /// <returns>Executional context</returns>
        public QContext Run(Stream stream, Dictionary<string,object> values = null)
        {
            return GlobalModule.Run(stream, values);
        }

        /// <summary>
        /// Compiles source code into singe function that can be called
        /// </summary>
        /// <param name="source">Source code</param>
        /// <returns>Compiled function</returns>
        public QFunction Compile(string source)
        {
            return QCompiler.CompileSource(this.GlobalModule, new TokenStream(source));
        }

        /// <summary>
        /// Compiles source code into singe function that can be called
        /// </summary>
        /// <param name="stream">Source code stream</param>
        /// <returns>Compiled function</returns>
        public QFunction Compile(StreamReader stream)
        {
            return QCompiler.CompileSource(this.GlobalModule, new TokenStream(stream));
        }

        /// <summary>
        /// Json serialization
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>String representation of object</returns>
        public string DumpString(object obj)
        {
            return Jsonizer.DumpString(obj);
        }

        /// <summary>
        /// Json serialization
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>String representation of object</returns>
        public void Dump(StreamWriter stream ,object obj)
        {
            Jsonizer.Dump(stream,obj);
        }
    }
}
