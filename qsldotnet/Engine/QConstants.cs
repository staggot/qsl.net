﻿using System;
namespace qsldotnet
{
    internal static class QConstants
    {
        internal const string
            add = "__add", // a + b
            pos = "__pos", // + a
            sub = "__sub", // a - b
            neg = "__neg", // - a
            mul = "__mul", // a * b
            div = "__div", // a / b
            mod = "__mod", // a % b
            inc = "__inc", // a++
            dec = "__dec", // a--

            booland = "__bool_and", // a && b
            boolor = "__bool_or",   // a || b
            boolnot = "__bool_not", // ! a

            bitand = "__bit_and",     // a & b
            bitor = "__bit_or",       // a | b
            bitnot = "__bit_not",     // ~ a
            bitxor = "__bit_xor",     // a ^ b
            lshift = "__left_shift",  // a << b
            rshift = "__right_shift", // a >> b

            bigger = "__bigger",      // a > b
            lesser = "__lesser",      // a < b
            biggereq = "__bigger_eq", // a >= b
            lessereq = "__lesser_eq"  // a <= b
            ;


    }
}
