using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using System.IO;

namespace qsldotnet
{
    public partial class QEngine
    {

        internal QModule LoadModule(DirectoryInfo directory, string name)
        {
            FileInfo moduleFile = LocateModuleFile(directory, name);
            if (moduleFile is {})
            {
                string key = moduleFile.FullName;
                if (!loadedModules.ContainsKey(key))
                {
                    QModule module = new QModule(this, moduleFile);
                    loadedModules[key] = module;
                    module.RunFileExecutor(moduleFile.FullName, null);
                }
                return loadedModules[key];
            }
            throw new ScriptEngineException(this, string.Format("Module Not Found: {0}", name));
        }

        internal FileInfo LocateModuleFile(DirectoryInfo directory, string name)
        {
            return LocateLib(directory, ModulesDirectory, DefaultModuleFile, name, ScriptExtention);
        }

        internal FileInfo LocateLib(DirectoryInfo rootDirectory, string defaultFolder, string defaultFileName, string name, params string[] extentions)
        {
            string testDirectory = rootDirectory.FullName;
            FileInfo result;

            if ((result = new FileInfo(name)).Exists)
                return result;


            if ((result = LocateLibExt(rootDirectory, defaultFolder, defaultFileName, name, null)) is {})
                return result;
            foreach (string extention in extentions)
            {
                if ((result = LocateLibExt(rootDirectory, defaultFolder, defaultFileName, name, extention)) is {})
                    return result;
            }
            rootDirectory = GlobalModule.ModuleDirectory;
            if ((result = LocateLibExt(rootDirectory, defaultFolder, defaultFileName, name, null)) is {})
                return result;
            foreach (string extention in extentions)
            {
                if ((result = LocateLibExt(rootDirectory, defaultFolder, defaultFileName, name, extention)) is {})
                    return result;
            }
            return null;
        }

        FileInfo LocateLibExt(DirectoryInfo rootDirectory, string defaultFolder, string defaultFileName, string name, string extention)
        {
            string testDirectory = rootDirectory.FullName;
            string testPath;
            if (File.Exists(testPath = testDirectory + "/" + name + (extention is null ? "" : extention)))
                return new FileInfo(testPath);
            if (defaultFolder is {} && File.Exists(testPath = testDirectory + "/" + defaultFolder + "/" + name + (extention is null ? "" : extention)))
                return new FileInfo(testPath);
            if (defaultFileName is {})
            {
                if (File.Exists(testPath = testDirectory + "/" + name + "/" + defaultFileName + (extention is null ? "" : extention)))
                    return new FileInfo(testPath);
                if (defaultFolder is {} && File.Exists(testPath = testDirectory + "/" + defaultFolder + "/" + name + "/" + defaultFileName + (extention is null ? "" : extention)))
                    return new FileInfo(testPath);
            }
            return null;
        }
    }
}
