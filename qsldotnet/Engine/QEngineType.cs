﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using qsldotnet.core;
namespace qsldotnet
{
    public partial class QEngine
    {
        /// <summary>
        /// Allows type to be used in scripts
        /// </summary>
        /// <param name="type">Type To Allow</param>
        public void AllowType(Type type)
        {
            AddAllowedType(type);
        }

        internal void AddAllowedType(Type type)
        {
            string name = type.Name;
            if (IgnoreCase)
                name = name.ToLower();
            types[name] = type;
            allowedTypes.Add(type);
        }

        internal void TestAllowedType(Type type)
        {
            if (!StrictTypes)
                return;
            if (type.IsGenericType)
                type = type.GetGenericTypeDefinition();
            if (type.IsArray)
                type = type.GetElementType();
            if (allowedTypes.Contains(type))
                return;
            throw new ForbiddenTypeException(this, type);
        }
    }
}
