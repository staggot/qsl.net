﻿using System;

namespace qsldotnet.core
{
    internal enum ScriptOperatorType : byte
    {
        RightOperator, LeftOperator, UnaryOperator, PostUnaryOperator
    }

    internal class ScriptOperator
    {
        internal string Name { get; private set; }
        Action<ExecutionStack> ExecuteDelegate;
        internal ScriptOperator(string name, int priority, ScriptOperatorType type, Action<ExecutionStack> exec)
        {
            this.Name = name;
            this.Priority = priority;
            this.Type = type;
            this.ExecuteDelegate = exec;
        }

        internal void Execute(ExecutionStack stack)
        {
            ExecuteDelegate(stack);
        }

        internal int Priority;
        internal ScriptOperatorType Type;
    }
}
