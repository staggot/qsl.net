﻿using System;
using System.Collections.Generic;

namespace qsldotnet.core
{
    public partial class ScriptExecutor
    {
        static Dictionary<string, ScriptOperator> operators = new Dictionary<string, ScriptOperator>();

        static void AddOperator(ScriptOperator oper)
        {
            operators[oper.Name.ToLower()] = oper;
        }

        static ScriptExecutor()
        {
            InitOperators();
        }

        static void InitOperators()
        {
            AddOperator(new ScriptOperator("+=", 0, ScriptOperatorType.LeftOperator, PlusEq));
            AddOperator(new ScriptOperator("-=", 0, ScriptOperatorType.LeftOperator, MinEq));
            AddOperator(new ScriptOperator("*=", 0, ScriptOperatorType.LeftOperator, MulEq));
            AddOperator(new ScriptOperator("/=", 0, ScriptOperatorType.LeftOperator, DivEq));
            AddOperator(new ScriptOperator("%=", 0, ScriptOperatorType.LeftOperator, ModEq));
            AddOperator(new ScriptOperator("|=", 0, ScriptOperatorType.LeftOperator, BitOrEq));
            AddOperator(new ScriptOperator("&=", 0, ScriptOperatorType.LeftOperator, BitAndEq));
            AddOperator(new ScriptOperator("^=", 0, ScriptOperatorType.LeftOperator, BitXorEq));

            AddOperator(new ScriptOperator("&&", 1, ScriptOperatorType.RightOperator, BoolAnd));
            AddOperator(new ScriptOperator("||", 1, ScriptOperatorType.RightOperator, BoolOr));

            AddOperator(new ScriptOperator("==", 2, ScriptOperatorType.RightOperator, Eq));
            AddOperator(new ScriptOperator("!=", 2, ScriptOperatorType.RightOperator, NotEq));
            AddOperator(new ScriptOperator(">", 2, ScriptOperatorType.RightOperator, Bigger));
            AddOperator(new ScriptOperator(">=", 2, ScriptOperatorType.RightOperator, BiggerEq));
            AddOperator(new ScriptOperator("<", 2, ScriptOperatorType.RightOperator, Lesser));
            AddOperator(new ScriptOperator("<=", 2, ScriptOperatorType.RightOperator, LesserEq));
            AddOperator(new ScriptOperator("==", 2, ScriptOperatorType.RightOperator, Eq));

            AddOperator(new ScriptOperator("+", 3, ScriptOperatorType.RightOperator, Plus));
            AddOperator(new ScriptOperator("-", 3, ScriptOperatorType.RightOperator, Minus));

            AddOperator(new ScriptOperator("*", 4, ScriptOperatorType.RightOperator, Mul));
            AddOperator(new ScriptOperator("/", 4, ScriptOperatorType.RightOperator, Div));
            AddOperator(new ScriptOperator("%", 4, ScriptOperatorType.RightOperator, Mod));

            AddOperator(new ScriptOperator("**", 5, ScriptOperatorType.RightOperator, Pow));

            AddOperator(new ScriptOperator("&", 6, ScriptOperatorType.RightOperator, BinAnd));
            AddOperator(new ScriptOperator("|", 6, ScriptOperatorType.RightOperator, BinOr));
            AddOperator(new ScriptOperator("^", 6, ScriptOperatorType.RightOperator, BinXor));

            AddOperator(new ScriptOperator("!", 7, ScriptOperatorType.UnaryOperator, BoolNot));
            AddOperator(new ScriptOperator("~", 7, ScriptOperatorType.UnaryOperator, BinNot));

            AddOperator(new ScriptOperator("-?", 7, ScriptOperatorType.UnaryOperator, Neg));
            AddOperator(new ScriptOperator("+?", 7, ScriptOperatorType.UnaryOperator, Pos));
            AddOperator(new ScriptOperator("?++", 7, ScriptOperatorType.PostUnaryOperator, PostInc));
            AddOperator(new ScriptOperator("++?", 7, ScriptOperatorType.UnaryOperator, PreInc));
            AddOperator(new ScriptOperator("?--", 7, ScriptOperatorType.PostUnaryOperator, PostDec));
            AddOperator(new ScriptOperator("--?", 7, ScriptOperatorType.UnaryOperator, PreDec));

            AddOperator(new ScriptOperator("<<", 7, ScriptOperatorType.LeftOperator, SHLeft));
            AddOperator(new ScriptOperator(">>", 7, ScriptOperatorType.LeftOperator, SHRight));

            AddOperator(new ScriptOperator("is", 7, ScriptOperatorType.RightOperator, Is));
            AddOperator(new ScriptOperator("as", 7, ScriptOperatorType.RightOperator, As));
        }

        internal static ScriptOperator FindOperator(string name)
        {
            name = name.ToLower();
            if (operators.ContainsKey(name))
                return operators[name];
            return null;
        }

        internal static void PlusEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            ExecutionStackWord aWord = stack.Pop();
            dynamic a = aWord.Get(stack);
            aWord.Set(stack, (object)(a + b));
        }

        internal static void MinEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            ExecutionStackWord aWord = stack.Pop();
            dynamic a = aWord.Get(stack);
            aWord.Set(stack, (object)(a - b));
        }

        internal static void Neg(ExecutionStack stack)
        {
            dynamic a = stack.PopValue();
            stack.PushValue((object)(-a));
        }

        internal static void Pos(ExecutionStack stack)
        {
            dynamic a = stack.PopValue();
            stack.PushValue((object)(+a));
        }

        internal static void MulEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            ExecutionStackWord aWord = stack.Pop();
            dynamic a = aWord.Get(stack);
            aWord.Set(stack, (object)(a * b));
        }

        internal static void DivEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            ExecutionStackWord aWord = stack.Pop();
            dynamic a = aWord.Get(stack);
            aWord.Set(stack, (object)(a / b));
        }

        internal static void ModEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            ExecutionStackWord aWord = stack.Pop();
            dynamic a = aWord.Get(stack);
            aWord.Set(stack, (object)(a % b));
        }
        internal static void BitAndEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            ExecutionStackWord aWord = stack.Pop();
            dynamic a = aWord.Get(stack);
            aWord.Set(stack, (object)(a & b));
        }
        internal static void BitOrEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            ExecutionStackWord aWord = stack.Pop();
            dynamic a = aWord.Get(stack);
            aWord.Set(stack, (object)(a | b));
        }

        internal static void BitXorEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            ExecutionStackWord aWord = stack.Pop();
            dynamic a = aWord.Get(stack);
            aWord.Set(stack, (object)(a ^ b));
        }

        internal static void PostInc(ExecutionStack stack)
        {
            ExecutionStackWord a = stack.Pop();
            dynamic aVal = a.Get(stack);
            stack.PushValue(aVal);
            aVal = aVal + (dynamic)1;
            a.Set(stack, (object)(aVal));
        }

        internal static void PostDec(ExecutionStack stack)
        {
            ExecutionStackWord a = stack.Pop();
            dynamic aVal = a.Get(stack);
            stack.PushValue(aVal);
            aVal = aVal - (dynamic)1;
            a.Set(stack, (object)(aVal));
        }

        internal static void PreInc(ExecutionStack stack)
        {
            ExecutionStackWord a = stack.Pop();
            dynamic aVal = a.Get(stack);
            aVal = aVal + (dynamic)1;
            a.Set(stack, aVal);
            stack.PushValue((object)(aVal));
        }

        internal static void PreDec(ExecutionStack stack)
        {
            ExecutionStackWord a = stack.Pop();
            dynamic aVal = a.Get(stack);
            aVal = aVal - (dynamic)1;
            a.Set(stack, aVal);
            stack.PushValue((object)(aVal));
        }

        internal static void As(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue(Convert.ChangeType(a, b));
        }

        internal static void Is(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            if (a is QContext)
            {
                if (b is QConstructor)
                    stack.PushValue((a as QContext).ClassId == ((QConstructor)b).Method.Name);
                else if (b is QContext)
                    stack.PushValue(
                        (a as QContext).ClassId is {} &&
                        (a as QContext).ClassId == (b as QContext).ClassId
                        );
                else
                    stack.PushValue(false);
            }
            else
                stack.PushValue((b as Type).IsInstanceOfType(a));
        }

        internal static void Plus(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a + b));
        }
        internal static void Mul(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a * b));
        }
        internal static void Pow(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)Math.Pow(a, b));
        }
        internal static void Div(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a / b));
        }
        internal static void Mod(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a % b));
        }
        internal static void Minus(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a - b));
        }
        internal static void Bigger(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a > b));
        }
        internal static void BiggerEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a >= b));
        }
        internal static void Lesser(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a < b));
        }
        internal static void LesserEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a <= b));
        }
        internal static void Eq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a == b));
        }
        internal static void NotEq(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a != b));
        }
        internal static void BinAnd(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a & b));
        }
        internal static void BinOr(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a | b));
        }
        internal static void BinNot(ExecutionStack stack)
        {
            dynamic a = stack.PopValue();
            stack.PushValue((object)(~a));
        }
        internal static void BoolAnd(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a && b));
        }
        internal static void BoolOr(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a || b));
        }
        internal static void BoolNot(ExecutionStack stack)
        {
            dynamic a = stack.PopValue();
            stack.PushValue((object)(!a));
        }
        internal static void BinXor(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a | b));
        }

        internal static void SHRight(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a >> b));
        }

        internal static void SHLeft(ExecutionStack stack)
        {
            dynamic b = stack.PopValue();
            dynamic a = stack.PopValue();
            stack.PushValue((object)(a << b));
        }
    }
}
