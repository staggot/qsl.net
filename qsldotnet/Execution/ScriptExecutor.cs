using System;
using System.Collections.Generic;
using System.IO;
using System.Collections;

namespace qsldotnet.core
{
    public partial class ScriptExecutor
    {
        List<ExecutionBranch> branchStack = new List<ExecutionBranch>();
        QContext context;
        ExecutionStack stack;
        private CodeBuffer codeBuffer;
        private long PC = 0;
        bool running = true;

        internal QContext Context
        {
            get { return context; }
        }

        internal ExecutionStack Stack
        {
            get { return stack; }
        }

        internal ScriptExecutor(QContext context, StreamReader stream) : this(context, new RpnStream(stream))
        {

        }

        internal ScriptExecutor(QContext context, string text) : this(context, new RpnStream(text))
        {

        }

        internal ScriptExecutor(QContext context, RpnStream tokenStream)
        {
            this.context = context;
            stack = new ExecutionStack(context);
            codeBuffer = new CodeBuffer(tokenStream);
        }

        internal ExecutionBranch PeekBranch()
        {
            return branchStack[branchStack.Count - 1];
        }

        internal void PopBranch()
        {
            branchStack.RemoveAt(branchStack.Count - 1);
        }

        private void PassBlock()
        {
            int deep = 0;
            while (true)
            {

                RpnToken bToken = codeBuffer[PC];
                PC++;
                if (bToken.Text == "{")
                    deep++;
                else if (bToken.Type == RpnTokenType.EndBlock && deep == 0)
                {   
                    break;
                }
                else if (bToken.Type == RpnTokenType.EndBlock)
                    deep--;
            }
        }

        internal void ExecuteToken(RpnToken token)
        {
            switch (token.Type)
            {
                case RpnTokenType.Symbol:
                    ExecuteSymbol(stack, token.Text);
                    break;
                case RpnTokenType.NewSymbol:
                    ExecuteNewSymbol(stack, token.Text);
                    break;
                case RpnTokenType.Assign:
                    ExecuteAssign(stack);
                    break;
                case RpnTokenType.Colon:
                    ExecuteBlockAssign(stack);
                    break;
                case RpnTokenType.StartRoundCortage:
                    ExecuteStartArguments(stack);
                    break;
                case RpnTokenType.EndRoundCortage:
                    ExecuteEndCortageFunctionCall(stack);
                    break;
                case RpnTokenType.StartBlock:
                    ExecuteStartBlock(stack);
                    break;
                case RpnTokenType.StartSquareCortage:
                    ExecuteStartCortage(stack);
                    break;
                case RpnTokenType.EndSquareCortage:
                    ExecuteEndArrayCortage(stack);
                    break;
                case RpnTokenType.EndSquareCortageConstructor:
                    ExecuteEndConstructorArrayCortage(stack);
                    break;
                case RpnTokenType.EndRoundCortageConstructor:
                    ExecuteEndCortageConstructorCall(stack);
                    break;
                case RpnTokenType.SquareBracesOpen:
                    ExecuteStartList(stack);
                    break;
                case RpnTokenType.ListComma:
                    ExecuteListComma(stack);
                    break;
                case RpnTokenType.SquareBracesClose:
                    ExecuteEndList(stack);
                    break;
                case RpnTokenType.ArrayArgumentsComma:
                    ExecuteArrayArgumentsComma(stack);
                    break;
                case RpnTokenType.FunctionArgumentsComma:
                    ExecuteFunctionArgumentsComma(stack);
                    break;

                 case RpnTokenType.Value:
                    ExecuteValue(stack, token.Value);
                    break;
                case RpnTokenType.This:
                    ExecuteThis(stack, 0);
                    break;
                case RpnTokenType.Throw:
                    ExecuteThrow(stack);
                    break;
                case RpnTokenType.Global:
                    ExecuteGlobal(stack);
                    break;
                case RpnTokenType.Null:
                    ExecuteValue(stack, null);
                    break;
                case RpnTokenType.Unassigned:
                    ExecuteValue(stack, Unassigned.Value);
                    break;
                case RpnTokenType.Return:
                    throw new ExecutionException(context, "Only Function Can Return");
                case RpnTokenType.Member:
                    ExecuteMember(stack, token.Text);
                    break;
                case RpnTokenType.Operator:
                    ExecuteOperator(stack, token.Text);
                    break;
                case RpnTokenType.Function:
                    ExecuteCompiledFunction(stack, QCompiler.CompileFunction(stack.Module, (-1), codeBuffer, ref PC));
                    break;
                case RpnTokenType.Constructor:
                    ExecuteCompiledConstructor(stack, QCompiler.CompileConstructor(stack.Module, codeBuffer, ref PC));
                    break;
                case RpnTokenType.StartTryBlock:
                case RpnTokenType.StartCatchBlock:
                case RpnTokenType.StartFinallyBlock:
                    throw new ExecutionException(context, "Try-Catch-Finally Block Avaiable Only In Functions");
                case RpnTokenType.EndBlock:
                    if (branchStack.Count > 0)
                    {
                        ExecutionBranch branch = branchStack[branchStack.Count - 1];
                        switch (branch.Type)
                        {

                            case BranchEnum.If:

                            case BranchEnum.Else:
                            case BranchEnum.ElseIf:
                                PopBranch();
                                break;
                            case BranchEnum.While:
                                PC = branch.Start;
                                break;
                            case BranchEnum.Foreach:
                                PC = branch.Start;
                                break;
                        }

                    }
                    break;
                case RpnTokenType.ForeachIn:
                    ExecuteGetEnumerator(stack);
                    IEnumerator enumerator = stack.PopValue() as IEnumerator;
                    ExecutionStackWord iterator = stack.Pop();
                    branchStack.Add(new ExecutionBranch() { Start = PC, Type = BranchEnum.Foreach, Enumerator = enumerator, Iterator = iterator });
                    break;
                case RpnTokenType.StartForeachBlock:
                    ExecutionBranch foreachBranch = PeekBranch();
                    ExecuteNextForeach(stack, foreachBranch.Enumerator, foreachBranch.Iterator);
                    if (!PopTrue(stack))
                    {
                        PassBlock();
                        PopBranch();
                    }
                    break;
                case RpnTokenType.While:
                    branchStack.Add(new ExecutionBranch() { Start = PC, Type = BranchEnum.While });
                    break;
                case RpnTokenType.StartWhileBlock:
                    if (!PopTrue(stack))
                    {
                        PassBlock();
                        PopBranch();
                    }
                    break;

                case RpnTokenType.Break:
                    int passCount = 0;
                    while (branchStack.Count > 0)
                    {
                        ExecutionBranch data = branchStack[branchStack.Count - 1];

                        if (data.Type == BranchEnum.While || data.Type == BranchEnum.Foreach)
                        {
                            break;
                        }
                        passCount++;
                        PopBranch();
                    }
                    while (true)
                    {
                        RpnToken pToken = codeBuffer[PC++];
                        if (passCount == 0 && pToken.Type == RpnTokenType.EndBlock)
                            break;
                        else if (pToken.Type == RpnTokenType.EndBlock)
                            passCount--;
                    }
                    break;
                case RpnTokenType.Continue:
                    while (branchStack.Count > 0)
                    {
                        ExecutionBranch data = branchStack[branchStack.Count - 1];
                        if (data.Type == BranchEnum.While || data.Type == BranchEnum.Foreach)
                        {
                            PC = data.Start;
                            break;
                        }
                        PopBranch();
                    }
                    break;

                case RpnTokenType.If:
                    branchStack.Add(new ExecutionBranch() { Start = PC, Type = BranchEnum.If });
                    break;

                case RpnTokenType.Else:
                    RpnToken nextToken = codeBuffer[PC++];
                    if (nextToken.Type == RpnTokenType.StartElseBlock)
                    {
                        PassBlock();
                    }
                    else
                        throw new Exception();
                    break;

                case RpnTokenType.ElseIf:
                    while (true)
                    {
                        RpnToken nextElseIfToken = codeBuffer[PC++];
                        if (nextElseIfToken.Type == RpnTokenType.StartElseIfBlock)
                        {
                            PassBlock();
                            break;
                        }
                    }
                    break;
                case RpnTokenType.StartElseBlock:
                    branchStack.Add(new ExecutionBranch() { Start = PC, Type = BranchEnum.Else });
                    break;
                case RpnTokenType.StartElseIfBlock:
                    if (!PopTrue(stack))
                    {
                        PassBlock();
                        PopBranch();
                        RpnToken nextIfToken = codeBuffer[PC];
                        if (nextIfToken.Type == RpnTokenType.Else)
                        {
                            PC++;
                            branchStack.Add(new ExecutionBranch() { Start = PC, Type = BranchEnum.Else });
                        }
                        else if (nextIfToken.Type == RpnTokenType.ElseIf)
                        {
                            PC++;
                            branchStack.Add(new ExecutionBranch() { Start = PC, Type = BranchEnum.ElseIf });
                        }
                    }
                    break;
                case RpnTokenType.StartIfBlock:

                    if (!PopTrue(stack))
                    {
                        PassBlock();
                        PopBranch();
                        RpnToken nextIfToken = codeBuffer[PC];
                        if (nextIfToken.Type == RpnTokenType.Else)
                        {
                            PC++;
                            branchStack.Add(new ExecutionBranch() { Start = PC, Type = BranchEnum.Else });
                        }
                        else if (nextIfToken.Type == RpnTokenType.ElseIf)
                        {
                            PC++;
                            branchStack.Add(new ExecutionBranch() { Start = PC, Type = BranchEnum.ElseIf });
                        }
                    }
                    break;
                case RpnTokenType.Semicolon:
                    ExecuteSemicolon(stack);
                    break;
                case RpnTokenType.End:
                    running = false;
                    break;
            }
        }

        internal void Run()
        {
            PC = 0;
            RpnToken token;
            running = true;

            while (running)
            {
                token = codeBuffer[PC];
                PC++;
                ExecuteToken(token);
                if (branchStack.Count > 0)
                    codeBuffer.LastNeededPC = branchStack[0].Start;
                else
                    codeBuffer.LastNeededPC = PC;
            }
        }
    }
}
