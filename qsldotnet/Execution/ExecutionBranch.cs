﻿using System;
using System.Collections;

namespace qsldotnet.core
{
    internal enum BranchEnum : byte
    {
        Function,
        If,
        Else,
        ElseIf,
        While,
        Foreach,
        Try,
        Catch,
        Finally,
        Using
    }

    internal struct ExecutionBranch
    {
        internal long Start;
        internal BranchEnum Type;
        internal ExecutionStackWord Iterator;
        internal IEnumerator Enumerator;
    }
}
