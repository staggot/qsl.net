using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections;

namespace qsldotnet.core
{
    public partial class ScriptExecutor
    {
        internal static MethodInfo CreateStackMethod = typeof(ScriptExecutor).GetMethod(nameof(CreateStack), new Type[] { typeof(QContext) });
        public static ExecutionStack CreateStack(QContext context)
        {
            return new ExecutionStack(context);
        }

        internal static MethodInfo ExecuteCompiledConstructorMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteCompiledConstructor),
                new Type[] { typeof(ExecutionStack), typeof(int) });

        public static void ExecuteCompiledConstructor(ExecutionStack stack, int id)
        {
            stack.PushValue(stack.Module.GetCompiledMethod(id).CreateDelegate(typeof(QConstructor), stack.Context));
        }

        internal static MethodInfo PassArgumentsMethod =
            typeof(ScriptExecutor).GetMethod(nameof(PassArguments), new Type[] { typeof(QContext), typeof(string[]), typeof(object[]) });
        public static void PassArguments(QContext context, string[] names, object[] values)
        {
            context["_args"] = values;
            for (int i = 0; i < names.Length && i < values.Length; i++)
            {
                context[names[i]] = values[i];
            }
        }

        internal static MethodInfo SetVarLocalMethod =
            typeof(ScriptExecutor).GetMethod(nameof(SetVarLocal), new Type[] { typeof(QContext), typeof(string), typeof(object) });
        public static void SetVarLocal(QContext context, string name, object value)
        {
            context.SetVarLocal(name, value);
        }

        internal static MethodInfo ExecuteCompiledFunctionMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteCompiledFunction),
                                             new Type[] { typeof(ExecutionStack), typeof(int) });

        public static void ExecuteCompiledFunction(ExecutionStack stack, int id)
        {
            stack.PushValue(stack.Module.GetCompiledMethod(id).CreateDelegate(typeof(QFunction), stack.Context));
        }

        internal static MethodInfo AppyInsideMethod =
            typeof(ScriptExecutor).GetMethod(
                nameof(ApplyInsideCall),
                new Type[] { typeof(FunctionArguments), typeof(string[]), typeof(object[]), typeof(QContext) });

        public static void ApplyInsideCall(FunctionArguments args, string[] paramNames, object[] defaultValues, QContext context)
        {
            if (args is null)
                context["_args"] = new QArgs();
            else
                context["_args"] = args.ExctractArguments(context.Module.Engine, paramNames, defaultValues, context);
        }

        internal static MethodInfo ExecuteStartBlockMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteStartBlock),
                                               new Type[] { typeof(ExecutionStack) });

        public static void ExecuteStartBlock(ExecutionStack stack)
        {
            stack.PushValue(stack.Engine.CreateContext());
        }

        internal static MethodInfo ExecuteStartListMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteStartList),
                                               new Type[] { typeof(ExecutionStack) });

        public static void ExecuteStartList(ExecutionStack stack)
        {
            stack.PushValue(new List<object>());
        }

        internal static MethodInfo ExecuteListCommaMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteListComma),
                                               new Type[] { typeof(ExecutionStack) });

        public static void ExecuteListComma(ExecutionStack stack)
        {
            object value = stack.PopValue();
            List<object> list = stack.PopValue() as List<object>;
            list.Add(value);
            stack.PushValue(list);
        }

        internal static MethodInfo ExecuteEndListMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteEndList),
                                             new Type[] { typeof(ExecutionStack) });

        public static void ExecuteEndList(ExecutionStack stack)
        {
            object value = stack.PopValue();
            List<object> list;

            if (value is List<object>)
            {
                stack.PushValue((value as List<object>).ToArray());
            }
            else
            {
                list = stack.PopValue() as List<object>;
                list.Add(value);
                stack.PushValue(list.ToArray());
            }
        }

        internal static MethodInfo ExecuteBlockAssignMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteBlockAssign),
                                            new Type[] { typeof(ExecutionStack) });

        public static void ExecuteBlockAssign(ExecutionStack stack)
        {
            object value = stack.PopValue();
            ExecutionStackWord word = stack.Pop();

            string key = null;
            if (word.WordType == StackWordType.Symbol)
                key = word.Parameter as string;
            else if (word.WordType == StackWordType.Value && word.Value is string)
                key = word.Value as string;
            else
                throw new Exception();


            object context = stack.PopValue();
            if (context is QContext)
            {
                (context as QContext)[key] = value;
                stack.PushValue(context);
            }
            else if (context is FunctionArguments)
            {
                (context as FunctionArguments).Set(key, value);
                stack.PushValue(context);
                stack.PushValue(QArgumentLocker.Value);
            }
        }

        internal static MethodInfo ExecuteAssignMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteAssign),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteAssign(ExecutionStack stack)
        {
            object value = stack.PopValue();
            ExecutionStackWord word = stack.Pop();
            word.Set(stack, value);
            stack.Push(word);
        }

        internal static MethodInfo ExecuteThrowMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteThrow),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteThrow(ExecutionStack stack)
        {
            object value = stack.PopValue();
            throw value as Exception;
        }

        internal static MethodInfo ExecuteSemicolonMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteSemicolon),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteSemicolon(ExecutionStack stack)
        {
            if (stack.Count > 1)
                throw new Exception();
            while (stack.Count > 0)
                stack.Pop();
        }

        internal static MethodInfo ExecuteValueMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteValue),
                new Type[] { typeof(ExecutionStack), typeof(object) });
        public static void ExecuteValue(ExecutionStack stack, object value)
        {
            stack.PushValue(value);
        }


        internal static MethodInfo ExecuteStartArgumentsMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteStartArguments),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteStartArguments(ExecutionStack stack)
        {
            stack.PushValue(new FunctionArguments());
        }

        internal static MethodInfo ExecuteStartCortageMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteStartCortage),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteStartCortage(ExecutionStack stack)
        {
            stack.PushValue(new ArrayArguments(stack.Engine));
        }


        internal static MethodInfo ExecuteArrayArgumentsCommaMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteArrayArgumentsComma),
        new Type[] { typeof(ExecutionStack) });
        public static void ExecuteArrayArgumentsComma(ExecutionStack stack)
        {
            object value = stack.PopValue();
            ArrayArguments args = stack.PopValue() as ArrayArguments;
            if (args is {})
            {
                args.AddValue(value);
            }
            else
            {
                throw new ExecutionException(stack.Context, "Array Arguments Not Found");
            }

        }

        internal static MethodInfo ExecuteFunctonArgumentsCommaMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteFunctionArgumentsComma),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteFunctionArgumentsComma(ExecutionStack stack)
        {
            object value = stack.PopValue();
            if (value is QArgumentLocker)
                return;
            FunctionArguments cortage = stack.PopValue() as FunctionArguments;

            if (cortage is {})
            {
                cortage.AddValue(value);
                stack.PushValue(cortage);
            }
            else
            {
                throw new ExecutionException(stack.Context, "Function Arguments Not Found");
            }

        }

        internal static MethodInfo ExecuteGetEnumeratorMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteGetEnumerator),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteGetEnumerator(ExecutionStack stack)
        {
            object objValue = stack.PopValue() as IEnumerable;
            IEnumerable value = objValue as IEnumerable;
            if (value is {})
            {
                stack.PushValue(value.GetEnumerator());
            }
            else
            {
                throw new ExecutionException(
                    stack.Context,
                    string.Format("{0} Not Enumerable", objValue is null ? "null" : objValue.GetType().ToString())
                    );
            }
        }

        internal static MethodInfo ExecuteNextForeachMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteNextForeach),
                new Type[] { typeof(ExecutionStack), typeof(IEnumerator), typeof(ExecutionStackWord) });
        public static void ExecuteNextForeach(ExecutionStack stack, IEnumerator enumerator, ExecutionStackWord iterator)
        {
            bool moved = enumerator.MoveNext();
            if (moved)
                iterator.Set(stack, enumerator.Current);
            stack.PushValue(moved);
        }


        internal static bool PopTrue(ExecutionStack stack)
        {
            return stack.PopTrue();
        }


        internal static MethodInfo ExecuteEndCortageConstructorCallMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteEndCortageConstructorCall),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteEndCortageConstructorCall(ExecutionStack stack)
        {
            object value = stack.PopValue();
            FunctionArguments cortage;

            if (value is FunctionArguments)
            {
                cortage = (value as FunctionArguments);
            }
            else
            {
                cortage = (stack.PopValue() as FunctionArguments);
                if (!(value is QArgumentLocker))
                    cortage.AddValue(value);

            }

            ExecutionStackWord func = stack.Pop();

            func.Constructor(stack, cortage);
        }

        internal static MethodInfo ExecuteEndCortageFunctionCallMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteEndCortageFunctionCall),
                new Type[] { typeof(ExecutionStack) });
        public static void ExecuteEndCortageFunctionCall(ExecutionStack stack)
        {
            object value = stack.PopValue();
            FunctionArguments cortage;
            if (value is FunctionArguments)
            {
                cortage = (value as FunctionArguments);
            }
            else
            {
                cortage = (stack.PopValue() as FunctionArguments);
                if (!(value is QArgumentLocker))
                    cortage.AddValue(value);

            }
            ExecutionStackWord func = stack.Pop();

            func.Execute(stack, cortage);
        }

        internal static MethodInfo ExecuteGlobalMethod = ExecuteEndArrayCortageMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteGlobal), new Type[] { typeof(ExecutionStack) });
        public static void ExecuteGlobal(ExecutionStack stack)
        {
            stack.PushValue(stack.Engine.GlobalModule);
        }

        internal static MethodInfo ExecuteThisMethod = ExecuteEndArrayCortageMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteThis), new Type[] { typeof(ExecutionStack), typeof(int) });
        public static void ExecuteThis(ExecutionStack stack, int deep)
        {
            QContext result = stack.Context;
            while (deep > 0)
            {
                result = result.ParentContext;
                deep--;
            }
            stack.PushValue(result);
        }

        internal static MethodInfo ExecuteEndArrayCortageMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteEndArrayCortage), new Type[] { typeof(ExecutionStack) });
        public static void ExecuteEndArrayCortage(ExecutionStack stack)
        {
            object val = stack.PopValue();
            ArrayArguments args;
            if (val is ArrayArguments)
            {
                args = (val as ArrayArguments);
            }
            else
            {

                args = (stack.PopValue() as ArrayArguments);
                args.AddValue(val);

            }
            stack.Push(new ExecutionStackWord(
                StackWordType.Array,
                stack.PopValue(),
                args.Array()
            ));
        }

        internal static MethodInfo ExecuteEndContructorArrayCortageMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteEndConstructorArrayCortage), new Type[] { typeof(ExecutionStack) });
        public static void ExecuteEndConstructorArrayCortage(ExecutionStack stack)
        {
            object val = stack.PopValue();
            ArrayArguments args;
            if (val is ArrayArguments)
            {
                args = val as ArrayArguments;
            }
            else
            {

                args = stack.PopValue() as ArrayArguments;
                args.AddValue(val);

            }

            Type arrayType = (Type)stack.PopValue();
            object[] lengthsObj = args.Array();
            long[] lengths = new long[lengthsObj.Length];

            for (int i = 0; i < lengths.Length; i++)
            {
                lengths[i] = Convert.ToInt64(lengthsObj[i]);
            }

            stack.PushValue(Array.CreateInstance(arrayType, lengths));
        }

        internal static MethodInfo ExecuteNewSymbolMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteNewSymbol),
                new Type[] { typeof(ExecutionStack), typeof(string) });
        public static void ExecuteNewSymbol(ExecutionStack stack, string name)
        {
            stack.Push(new ExecutionStackWord(
                StackWordType.NewSymbol,
                null,
                name
            ));
        }

        internal static MethodInfo ExecuteSymbolMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteSymbol),
                new Type[] { typeof(ExecutionStack), typeof(string) });
        public static void ExecuteSymbol(ExecutionStack stack, string name)
        {

            Type t = stack.Engine.FindType(name);
            if (t is {})
            {
                stack.Push(new ExecutionStackWord(
                    StackWordType.Type,
                    t
                ));
            }
            else
            {
                stack.Push(new ExecutionStackWord(
                    StackWordType.Symbol,
                    null,
                    name
                ));
            }
        }

        internal static MethodInfo ExecuteOperatorMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteOperator),
                new Type[] { typeof(ExecutionStack), typeof(string) });
        public static void ExecuteOperator(ExecutionStack stack, string name)
        {
            FindOperator(name).Execute(stack);
        }

        internal static MethodInfo ExecuteMemberMethod =
            typeof(ScriptExecutor).GetMethod(nameof(ExecuteMember),
                new Type[] { typeof(ExecutionStack), typeof(string) });
        public static void ExecuteMember(ExecutionStack stack, string name)
        {
            ExecutionStackWord word = stack.Pop();

            if (word.WordType == StackWordType.Type)
            {
                Type objType = word.Value as Type;
                stack.Engine.TestAllowedType(objType);
                stack.Push(new ExecutionStackWord(
                    StackWordType.StaticMember,
                    objType,
                    name
                ));
            }
            else
            {
                object obj = word.Get(stack);
                if (obj is null)
                    throw new ExecutionException(stack.Context, string.Format("Null Target On Member: \"{0}\"", name));
                if (obj is QContext)

                 {

                     stack.Push(new ExecutionStackWord(
                        StackWordType.ContextMember,
                        obj,
                        name
                    ));
                }
                else
                {
                    Type objType = obj.GetType();
                    stack.Engine.TestAllowedType(objType);
                    stack.Push(new ExecutionStackWord(
                        StackWordType.InstanceMember,
                        obj,
                        name
                    ));
                }
            }
        }

        internal static MethodInfo FunctionArgumentsAppyDictMethod =
            typeof(ScriptExecutor).GetMethod(
                nameof(FunctionArgumentsAppyDict),
                        new Type[] { typeof(FunctionArguments), typeof(QContext) });

        public static void FunctionArgumentsAppyDict(FunctionArguments args, QContext context)
        {
            if (args is null)
                context["_args"] = new QArgs();
            else
                args.ApplyDict(context);
        }
    }
}
