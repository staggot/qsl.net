using System;

namespace qsldotnet
{
    public class ScriptEngineException : Exception
    {
        QEngine engine;
        internal ScriptEngineException(QEngine engine, string message) : base(message)
        {
            this.engine = engine;

        }
    }

    public class ForbiddenTypeException : ScriptEngineException
    {
        internal ForbiddenTypeException(QEngine engine, Type type) : base(engine, string.Format("Unallowed type: [{0}]", type.FullName))
        {
        }
    }

    public class ParserException : ScriptEngineException
    {
        StreamToken token;

        internal ParserException(StreamToken token, string message) : base(null, string.Format("Parser Error: {0} On Token {1}", message, token))
        {
            this.token = token;
        }
    }

    public class CompilerException : ScriptEngineException
    {
        internal CompilerException(QEngine engine, string message) : base(engine, string.Format("Compiler Error: {0}", message))
        {
        }
    }

    public class ExecutionException : ScriptEngineException
    {
        internal ExecutionException(QContext context, string message) : base(context.Module.Engine, string.Format("Execution Error: {0},\nContext: {1}", message, context))
        {
        }
    }

    public class ScriptException : ScriptEngineException
    {
        internal ScriptException (QEngine engine, string message) : base (engine, string.Format ("Execution Error: {0}", message))
        {
        }

    }
}
